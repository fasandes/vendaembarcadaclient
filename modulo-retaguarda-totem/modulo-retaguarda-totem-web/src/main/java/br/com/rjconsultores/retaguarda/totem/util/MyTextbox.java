/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Textbox;

/**
 *
 * @author Administrador
 */
@SuppressWarnings("serial")
public class MyTextbox extends Textbox {

	public MyTextbox() {
		this.setStyle("text-transform:uppercase;");
	}

	@Override
	public String getValue() throws WrongValueException {
		return super.getValue().toUpperCase().trim();
	}

	@Override
	public void setValue(String value) throws WrongValueException {
		if (value == null) {
			super.setValue(value);
		} else {
			super.setValue(value.toUpperCase().trim());
		}
	}

	@Override
	public String getText() throws WrongValueException {
		return super.getText().toUpperCase().trim();
	}

	@Override
	public void setText(String value) throws WrongValueException {
		if (value == null) {
			super.setText(value);
		} else {
			super.setText(value.toUpperCase().trim());
		}
	}
}
