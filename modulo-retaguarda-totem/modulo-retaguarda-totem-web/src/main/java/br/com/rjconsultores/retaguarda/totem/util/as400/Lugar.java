/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util.as400;

/**
 * 
 * @author Rafius
 */
public class Lugar {

	private String cod;
	private String des;
	private String estado;
	private String pais;

	public String getCod() {
		return cod;
	}

	public void setCod(final String cod) {
		this.cod = cod;
	}

	public String getDes() {
		return des;
	}

	public void setDes(final String des) {
		this.des = des;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(final String pais) {
		this.pais = pais;
	}

}
