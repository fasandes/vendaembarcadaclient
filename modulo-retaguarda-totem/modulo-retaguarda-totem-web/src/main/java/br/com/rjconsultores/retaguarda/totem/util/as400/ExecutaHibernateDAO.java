package br.com.rjconsultores.retaguarda.totem.util.as400;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.hibernate.GenericHibernateDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Classe;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.Estado;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;
import br.com.rjconsultores.retaguarda.totem.entidades.Preco;
import br.com.rjconsultores.retaguarda.totem.entidades.Servico;
import br.com.rjconsultores.retaguarda.totem.entidades.Trecho;
import br.com.rjconsultores.retaguarda.totem.service.ClasseService;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.service.EstadoService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.service.LocalidadeService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.service.PrecoService;
import br.com.rjconsultores.retaguarda.totem.service.ServicoService;
import br.com.rjconsultores.retaguarda.totem.service.TrechoService;

/**
 * 
 * @author Rafael
 */
@Named("executa")
public class ExecutaHibernateDAO extends GenericHibernateDAO<Object, String> implements Executa {

	@Inject
	private LocalidadeService localidadeService;
	@Inject
	private EmpresaService empresaService;
	@Inject
	private OperadorService operadorService;
	@Inject
	private ServicoService servicoService;
	@Inject
	private TrechoService trechoService;
	@Inject
	private EstadoService estadoService;
	@Inject
	private PrecoService precoService;
	@Inject
	private ClasseService classeService;
	@Inject
	private LinhaService linhaService;

	private final Logger log = Logger.getLogger(ExecutaHibernateDAO.class);

	@Transactional
	public int sincronizaLocalidade(Connection c) {
		try {
			c.setAutoCommit(true);

			final PreparedStatement ps = c.prepareStatement("SELECT LOCOD, LODES,LOESTA FROM LUGAR");

			final ResultSet rs = ps.executeQuery();
			rs.setFetchSize(10000);

			Localidade localidade;

			Session session = getSession();
			Transaction tx = session.beginTransaction();
			int count = -1;
			while (rs.next()) {
				localidade = new Localidade();
				localidade.setLocalidadeId(rs.getInt("LOCOD"));
				localidade.setCidade(rs.getString("LODES"));
				localidade.setEstado(estadoService.buscarPorId(rs.getInt("LOESTA")));

				session.saveOrUpdate(localidade);

				if (++count % 20 == 0) {
					session.flush();
				}

			}

			tx.commit();
			session.close();

			return count;

		} catch (Exception e) {
			log.error("Error", e);

			return -1;

		}
	}

	@Transactional
	public int sincronizaClasse(Connection c) {
		int count = 0;
		try {
			c.setAutoCommit(true);
			final PreparedStatement ps = c.prepareStatement(
					"SELECT CCL, CDES FROM CLASE WHERE CCL IN(SELECT DISTINCT(UPPER(CCL)) FROM CLASE)");
			final ResultSet rs = ps.executeQuery();
			rs.setFetchSize(10000);

			Classe classe;
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			while (rs.next()) {
				classe = new Classe();
				classe.setCodigoClasse(rs.getString("CCL"));
				classe.setDescricao(rs.getString("CDES"));
				session.saveOrUpdate(classe);
				if (++count % 20 == 0) {
					session.flush();
				}

			}

			tx.commit();
			session.close();
			return count;

		} catch (Exception e) {
			log.error("Error", e);
			return count;

		}
	}

	@Transactional
	public int sincronizaEstado(Connection c) {
		int count = -1;
		try {
			c.setAutoCommit(true);
			final PreparedStatement ps = c.prepareStatement("SELECT ESCOD, ESDES, ESICMS, ESSIG FROM ESTADO");
			final ResultSet rs = ps.executeQuery();
			rs.setFetchSize(10000);

			Estado estado;
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			while (rs.next()) {
				estado = new Estado();
				estado.setCodigo(rs.getInt("estado"));
				estado.setNome(rs.getString("ESDES"));
				estado.setAliquota(rs.getBigDecimal("ESICMS"));
				estado.setSigla(rs.getString("ESSIG"));

				estadoService.salvar(estado);

				if (++count % 20 == 0) {
					session.flush();
					session.clear();
				}

			}

			tx.commit();
			session.close();
			return count;

		} catch (Exception e) {
			log.error("Error", e);
			return count;

		}
	}

	@Transactional
	public int sincronizaEmpresa(Connection c) {
		int count = -1;
		try {
			c.setAutoCommit(true);
			final PreparedStatement ps = c.prepareStatement(
					"SELECT E.EMCOD, E.EMNOM, E.EMREA, E.EMCNPJ, E.EMIEST, E.EMIMUN, E2.EMEND, E2.EMSAC FROM EMPRE E INNER JOIN EMPRE2 E2 ON (E.EMCOD = E2.EMCOD)");
			final ResultSet rs = ps.executeQuery();
			rs.setFetchSize(10000);

			Empresa empresa;
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			while (rs.next()) {
				empresa = new Empresa();
				empresa.setCodigoEmpresa(rs.getString("EMCOD"));
				empresa.setNome(rs.getString("EMNOM"));
				empresa.setCnpj(rs.getString("EMCNPJ"));
				empresa.setEndereco(rs.getString(rs.getString("EMEND")));
				empresa.setInscricaoEstadual(rs.getString("EMIEST"));
				empresa.setInscricaoMunicipal(rs.getString("EMIMUN"));
				session.saveOrUpdate(empresa);
				if (++count % 20 == 0) {
					session.flush();
					session.clear();
				}

			}

			tx.commit();
			session.close();
			return count;

		} catch (Exception e) {
			log.error("Error", e);
			return count;

		}
	}

	@SuppressWarnings("finally")
	@Transactional
	public int sincronizaPreco(Connection c, String tabelaPreco) {

		int count = -1;
		Session session = getSession();
		Transaction transaction = session.beginTransaction();
		try {
			c.setAutoCommit(true);
			final PreparedStatement psAlias = c
					.prepareStatement("CREATE ALIAS P_" + tabelaPreco + " FOR PRECIO(" + tabelaPreco + ")");
			try {
				psAlias.executeUpdate();
			} catch (Exception e) {
				log.error("alias j� existe, utilizando: P_" + tabelaPreco);

			} finally {
				psAlias.close();
			}
			final PreparedStatement ps = c.prepareStatement(
					"SELECT PRLIN, PRDES,PRHAS,PREMP,PRCLA,PRPRE,PRSEG,PRTAX,PRPEA,PROTR FROM P_" + tabelaPreco
							+ " where PRDES in (Select LOCOD from LUGAR) and PRHAS in (Select LOCOD from LUGAR) and PRCLA IN(SELECT DISTINCT(UPPER(CCL)) FROM CLASE) and PREMP in "
							+ "(select EMCOD from EMPRE) AND PRLIN IN (SELECT LMLIN FROM LINMA WHERE LMEMP IN (SELECT EMCOD FROM EMPRE))");
			final ResultSet rs = ps.executeQuery();
			rs.setFetchSize(10000);

			Preco preco;
			while (rs.next()) {
				preco = new Preco();

				preco.setTrecho(trechoService.buscarTrecho(rs.getInt("PRDES"), rs.getInt("PRHAS"), rs.getInt("PRLIN")));
				preco.setValorOutros(rs.getBigDecimal("PROTR"));
				preco.setValorPedagio(rs.getBigDecimal("PRSEG"));
				preco.setValorTaxa(rs.getBigDecimal("PRTAX"));
				preco.setValorTarifa(rs.getBigDecimal("PRPRE"));

				precoService.salvar(preco);
				count++;

			}

		} catch (Exception e) {
			log.error("Error", e);

		} finally {
			transaction.commit();
			session.close();
			return count;

		}
	}

	@Transactional
	public int sincronizaLinha(Connection c) throws RuntimeException {
		int count = -1;
		try {
			c.setAutoCommit(false);

			final StringBuilder sb = new StringBuilder("select LMLIN,LMDES,LMEMP,LMPRE from LINMA ORDER BY LMLIN");
			final PreparedStatement pStm = c.prepareStatement(sb.toString());
			final ResultSet rs = pStm.executeQuery();
			rs.setFetchSize(10000);
			Session session = getSession();
			Transaction tx = session.beginTransaction();
			Linha linha;
			while (rs.next()) {
				linha = new Linha();
				linha.setCodigoLinha(rs.getInt("LMLIN"));
				linha.setDescricao(rs.getString("LMDES"));
				Empresa empresa = empresaService.buscarPorCodigo(rs.getString("LMEMP"));

				if (empresa == null) {
					continue;
				}
				linha.setEmpresa(empresa);
				linha.setPrefixo(rs.getString("LMPRE"));
				session.saveOrUpdate(linha);

				if (++count % 20 == 0) {
					session.flush();
					// session.clear();
				}
			}

			tx.commit();
			session.close();
			return ++count;

		} catch (Exception e) {
			log.error("Error", e);
			return count;

		}

	}

	@Transactional
	public int sincronizaTipoPagamento(Connection c) {
		int count = -1;

		Session session = getSession();
		Transaction tx = session.beginTransaction();

		final List<String> lsInserts = new ArrayList<String>();
		try {
			c.setAutoCommit(false);

			StringBuilder sb = new StringBuilder();
			sb.append("SELECT TACOD, TADES, TADTO FROM TIPPAS");

			final PreparedStatement ps = c.prepareStatement(sb.toString(), ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			final ResultSet rs = ps.executeQuery();

			rs.setFetchSize(10000);

			sb = new StringBuilder();
			while (rs.next()) {

				sb.append(
						"insert into tipo_pagamento (codigo_tipo_pagamento, descricao, fator, exige_documento, ativo) values (");
				sb.append("'" + rs.getString("TACOD") + "', ");
				sb.append("'" + rs.getString("TADES") + "',");
				sb.append(" " + new BigDecimal(rs.getDouble("TADTO")).setScale(2, BigDecimal.ROUND_HALF_UP) + " ,");
				sb.append("null,");
				sb.append("true) ");
				sb.append(" on duplicate key update ");
				sb.append("codigo_tipo_pagamento = values(codigo_tipo_pagamento), ");
				sb.append("descricao = values(descricao); ");

				if (!rs.getString("TACOD").equals(" ")) {
					lsInserts.add(sb.toString());
				}
				sb = new StringBuilder();
				count++;

			}

			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {
					// System.out.println(sb.toString());
					PreparedStatement ps = connection.prepareStatement(defaultInsertTipoPagamento());
					try {
						ps.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					for (int i = 0; i < lsInserts.size(); i++) {
						try {
							ps.execute(lsInserts.get(i));
						} catch (SQLException ex) {
							// System.out.println(ex.getMessage());
						}
					}

					tx.commit();
				}
			});

		} catch (Exception e) {
			tx.rollback();
			log.error("Error", e);
			return count;
		} finally {
			session.close();
		}

		return ++count;
	}

	@Transactional
	public int sincronizaFormaPagamento(Connection c) {
		try {
			defaultInsertFormaPagamento();
		} catch (Exception e) {
			e.getMessage();
		}

		return 1;

	}

	@Transactional
	public int sincronizaServico(Connection c) throws RuntimeException {
		int count = -1;
		try {
			
			c.setAutoCommit(true);
			final String consulta = "select * from BONDT where TCL01 in (SELECT LOCOD FROM LUGAR) AND TRUTA in (SELECT LMLIN FROM LINMA WHERE LMEMP IN (SELECT	 EMCOD FROM EMPRE)) AND TEMP IN (SELECT EMCOD FROM EMPRE)";
			final PreparedStatement pStm = c.prepareStatement(consulta);
			final ResultSet rs = pStm.executeQuery();
			Servico servico = null;
			while (rs.next()) {
				servico = new Servico();
				final String campoDestino = this.encontraUltimoCampo(rs);

				servico.setClasse(classeService.buscarPorId(rs.getString("TCLAS")));
				servico.setCodigo(rs.getInt("TSRV"));
				servico.setOrigem(localidadeService.buscarPorId(rs.getInt("TCL01")));
				servico.setDestino(localidadeService.buscarPorId(rs.getInt(campoDestino)));
				servico.setEmpresa(empresaService.buscarPorCodigo(rs.getString("TEMP")));
				servico.setHorarioSaida(rs.getString("THO01"));
				servico.setLinha(linhaService.buscarPorId(rs.getInt("TRUTA")));
				
				servicoService.salvar(servico);

				count++;

			}

			return ++count;

		} catch (Exception e) {
			log.error("Error", e);
			return count;

		}

	}

	@Transactional
	public int sincronizaTrecho(Connection c) {
		int count = -1;
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			c.setAutoCommit(true);
			final String consulta = "SELECT * FROM BONDT WHERE TRUTA IN (SELECT LMLIN FROM LINMA WHERE LMEMP IN (SELECT EMCOD FROM EMPRE)) AND TEMP IN (SELECT EMCOD FROM EMPRE) AND TCLAS IN(SELECT DISTINCT(UPPER(CCL)) FROM CLASE)";
			final PreparedStatement pStm = c.prepareStatement(consulta);
			final ResultSet rs = pStm.executeQuery();
			rs.setFetchSize(10000);
			String campoOrigem;

			while (rs.next()) {
				for (int i = 1; i <= 60; i++) {
					campoOrigem = campoBOLTO(i);
					int destinoIndice = i + 1;
					Trecho trecho;
					if (Integer.parseInt(rs.getString(campoOrigem)) > 0) {
						String campoDestino = campoBOLTO(destinoIndice);

						while (Integer.parseInt(rs.getString(campoDestino)) > 0) {
							trecho = new Trecho();
							trecho.setOrigem(localidadeService.buscarPorId(rs.getInt(campoOrigem)));
							trecho.setDestino(localidadeService.buscarPorId(rs.getInt(campoDestino)));
							trecho.setServico(servicoService.buscarPorId(rs.getInt("TSRV")));
							destinoIndice++;
							campoDestino = campoBOLTO(destinoIndice);
						}

					}

				}

				count++;
			}

		} catch (Exception e) {
			log.error("Error", e);
			return count;
		} finally {
			tx.commit();
			session.close();
		}

		return count;
	}

	@Override
	@Transactional
	public int sincronizaBilheteiros(Connection c, String agencia) {
		int count = -1;
		Session session = getSession();
		final Transaction tx = session.beginTransaction();
		final List<String> lsInserts = new ArrayList<String>();
		try {
			c.setAutoCommit(true);

			StringBuilder sb = new StringBuilder();

			final PreparedStatement ps = c.prepareStatement("SELECT CJNOM,CJERO FROM CAJERO WHERE CJAGE = ?",
					ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

			ps.setString(1, agencia);
			final ResultSet rs = ps.executeQuery();

			rs.setFetchSize(10000);

			while (rs.next()) {
				if (!operadorService.buscarOperador(rs.getString("CJNOM"), rs.getString("CJERO"), agencia)) {

					sb.append(
							"INSERT INTO operador (OPERADOR_NAME, OPERADOR_ATIVO, OPERADOR_TIPO,OPERADOR_EMAIL,OPERADOR_AGENCIA) VALUES (");
					sb.append("'" + rs.getString("CJNOM") + "',");
					sb.append("false ,");
					sb.append("2, ");
					sb.append("'" + rs.getString("CJERO") + "',");
					sb.append("'" + agencia + "')");
					sb.append(
							" ON DUPLICATE KEY UPDATE OPERADOR_ID = (OPERADOR_ID), OPERADOR_NAME = (OPERADOR_NAME), OPERADOR_EMAIL = (OPERADOR_EMAIL), OPERADOR_AGENCIA = (OPERADOR_AGENCIA); ");
					lsInserts.add(sb.toString());
					sb = new StringBuilder();
					count++;
				}

			}

			session.doWork(new Work() {

				@Override
				public void execute(Connection connection) throws SQLException {
					PreparedStatement ps = connection.prepareStatement(lsInserts.get(0));
					try {
						ps.executeUpdate();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

					for (int i = 1; i < lsInserts.size(); i++) {
						try {
							ps.execute(lsInserts.get(i));
						} catch (SQLException ex) {
							System.out.println(ex.getMessage());
						}
					}

					tx.commit();
				}
			});

		} catch (Exception e) {
			tx.rollback();
			log.error("Error", e);
			return count;
		} finally {
			session.close();
		}

		return ++count;
	}

	@Transactional
	public void atualizaBolto(Map<String, String> parametros, Connection connection) throws SQLException {

		PreparedStatement ps = connection.prepareStatement(
				"UPDATE BOLTO B SET B.BOBOES = ?1 WHERE B.BOFEC  = ?2 AND B.BONPP = ?3 AND B.BOCJ = ?4");
		ps.setString(1, parametros.get("ccf"));
		ps.setString(2, parametros.get("bofec"));
		ps.setString(3, parametros.get("ccf"));
		ps.setString(4, parametros.get("bocj"));

		ps.executeUpdate();

		final String tabela = parametros.get("bocj");
		ps = connection.prepareStatement(
				"UPDATE " + tabela + " B SET B.BOBOES = ?1 WHERE B.BOFEC  = ?2 AND B.BONPP = ?3 AND B.BOCJ = ?4");
		ps.setString(1, parametros.get("ccf"));
		ps.setString(2, parametros.get("bofec"));
		ps.setString(3, parametros.get("ccf"));
		ps.setString(4, parametros.get("bocj"));
		ps.executeUpdate();

	}

	private String campoBOLTO(int indice) {
		if (indice <= 9) {
			return "TCL0" + indice;
		} else {
			return "TCL" + indice;
		}
	}

	private String defaultInsertTipoPagamento() {

		StringBuilder sb = new StringBuilder();

		sb.append(
				"INSERT INTO TIPO_PAGAMENTO (CODIGO_TIPO_PAGAMENTO, DESCRICAO, FATOR, EXIGE_DOCUMENTO, ATIVO) VALUES (");
		sb.append("99, ");
		sb.append("'NORMAL',");
		sb.append("0, ");
		sb.append("NULL, ");
		sb.append("true) ");
		sb.append(" ON DUPLICATE KEY UPDATE ");
		sb.append("CODIGO_TIPO_PAGAMENTO = VALUES(CODIGO_TIPO_PAGAMENTO), ");
		sb.append("DESCRICAO = VALUES(DESCRICAO); ");

		return sb.toString();
	}

	private String defaultInsertFormaPagamento() {

		Session session = getSession();
		Transaction tx = session.beginTransaction();

		final StringBuilder sb = new StringBuilder();

		sb.append("insert into forma_pagamento (codigo_forma_pagamento, descricao, fator, ativo ) values (");
		sb.append("'DI', ");
		sb.append("'DINHEIRO',");
		sb.append("1, ");
		sb.append("true) ");
		sb.append(" on duplicate key update ");
		sb.append("codigo_forma_pagamento = values(codigo_forma_pagamento), ");
		sb.append("descricao = values(descricao); ");

		session.doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sb.toString());

				try {
					ps.executeUpdate();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}

				if (!tx.wasCommitted()) {
					tx.commit();
				}
			}
		});
		return sb.toString();
	}

	private String encontraUltimoCampo(ResultSet rs) throws SQLException {
		int i = 10;
		for (i = 2; i < 60; i++) {
			if (rs.getInt(this.campoBOLTO(i)) == 0) {
				return this.campoBOLTO(--i);

			}
		}

		return "";

	}

}
