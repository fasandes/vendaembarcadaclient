/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jboss.logging.Logger;

/**
 * 
 * @author Rafael
 */
public class ValidacaoUtil {

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static Logger log = Logger.getLogger(ValidacaoUtil.class);

	public static boolean validaEmail(final String email) {
		log.debug("Metodo de validacao de email");
		final Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		final Matcher m = p.matcher(email);
		if (m.find()) {
			log.debug("O email " + email + " e valido");
			return true;
		} else {
			log.debug("O E-mail " + email + " é inválido");
			return false;
		}
	}

	private static int calcularDigito(final String str, final int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	public static boolean isValidCPF(String cpf) {
		cpf = cpf.replaceAll("[^a-z0-9]", "");
		if ((cpf == null) || (cpf.length() != 11)) {
			return false;
		}

		final Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		final Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	public static boolean isValidCNPJ(String cnpj) {
		cnpj = cnpj.replaceAll("[^a-z0-9]", "");
		if ((cnpj == null) || (cnpj.length() != 14)) {
			return false;
		}

		final Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		final Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}

	public static void main(final String[] args) {
		System.out.printf("Email:%s \n", validaEmail("rafius@gamil"));
		System.out.printf("CPF Valido:%s \n", isValidCPF("068.387.806-90"));
		System.out.printf("CNPJ Valido:%s \n", isValidCNPJ("51524436000110"));
	}
}
