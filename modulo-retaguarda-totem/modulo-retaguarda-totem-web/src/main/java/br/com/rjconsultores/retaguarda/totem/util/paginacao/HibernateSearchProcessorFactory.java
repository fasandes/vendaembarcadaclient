package br.com.rjconsultores.retaguarda.totem.util.paginacao;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.googlecode.genericdao.search.jpa.JPAAnnotationMetadataUtil;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;

public class HibernateSearchProcessorFactory {

	@PersistenceContext
	private EntityManager entityManager;

	@Produces
	public JPASearchProcessor criar() {
		return new JPASearchProcessor(new JPAAnnotationMetadataUtil());
	}

}
