package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Classe;


public class RenderComboClasse implements ComboitemRenderer<Classe>{

	@Override
	public void render(Comboitem item, Classe classe, int index) throws Exception {
		
		item.setValue(classe);
		item.setLabel(classe.getDescricao());
		
	}

}
