/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Combobox;

/**
 *
 * @author gleimar
 */
public class MyCombobox extends Combobox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public MyCombobox() {
		super();

		this.setAutodrop(true);
		this.setAutocomplete(true);
		this.setReadonly(true);
		this.setCtrlKeys("#del");

		this.addEventListener("onCtrlKey", new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				MyCombobox.this.setSelectedItem(null);
				MyCombobox.this.close();
			}
		});
	}
}
