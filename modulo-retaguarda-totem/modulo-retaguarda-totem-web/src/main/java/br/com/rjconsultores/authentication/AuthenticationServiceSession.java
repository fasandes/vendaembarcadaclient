package br.com.rjconsultores.authentication;

import java.io.Serializable;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;

public class AuthenticationServiceSession extends AuthenticationService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario getUserCredential() {
		Session sess = Sessions.getCurrent();
		Usuario usuario = (Usuario) sess.getAttribute("usuario");
		if (usuario == null) {
			usuario = new Usuario();// new a anonymous user and set to session
			sess.setAttribute("usuario", usuario);
		}
		return usuario;
	}

}
