package br.com.rjconsultores.retaguarda.totem.util;

import java.util.Map;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Window;

public class PantallaUtileria {

	public static int OVERLAPPED = 1;
	public static int MODAL = 2;
	private static Logger log = Logger.getLogger(MyGenericForwardComposer.class);

	public static void openWindow(String component, String title, Map args, Desktop desktop) {
		PantallaUtileria.openWindow(component, title, args, OVERLAPPED, desktop);
	}

	public static void openWindowPosi(String component, String title, Map args, String pos, Desktop desktop) {
		PantallaUtileria.openWindowPosi(component, title, args, OVERLAPPED, pos, desktop);
	}

	public static void openWindowPosi(String component, String title, Map args, int type, String pos, Desktop desktop) {
		Window win1 = null;

		try {
			win1 = (Window) desktop.getExecution().createComponents(component, null, args);

			Component c = Path.getComponent("/win/" + win1.getId());
			if (c != null) {
				c.detach();
			}

			win1.setTitle(title);
			win1.setClosable(true);
			win1.setMaximizable(false);
			if (OVERLAPPED == type) {
				win1.doOverlapped();
			} else {
				win1.doHighlighted();
			}

			win1.setPosition(pos);
			win1.setPage(desktop.getPage("mainWin"));
			win1.setParent(desktop.getPage("mainWin").getFellow("win"));
		} catch (Exception e) {
			log.error(e);
			if (win1 != null) {
				win1.detach();
			}

			throw UiException.Aide.wrap(e);
		}
	}

	public static void openWindow(String component, String title, Map args, int type, Desktop desktop) {
		Window win1 = null;

		try {
			win1 = (Window) desktop.getExecution().createComponents(component, null, args);

			Component c = Path.getComponent("/win/" + win1.getId());
			if (c != null) {
				c.detach();
			}

			win1.setTitle(title);
			win1.setClosable(true);
			win1.setMaximizable(false);
			if (OVERLAPPED == type) {
				win1.doOverlapped();
			} else {
				win1.doHighlighted();
			}

			win1.setPosition("center");
			win1.setPage(desktop.getPage("mainWin"));
			win1.setParent(desktop.getPage("mainWin").getFellow("win"));
		} catch (Exception e) {
			log.error(e);
			if (win1 != null) {
				win1.detach();
			}

			throw UiException.Aide.wrap(e);
		}
	}

	public static void openWindow(String component, String title, Map args, int type, Boolean closabled,
			Desktop desktop) {
		Window win1 = null;

		try {
			win1 = (Window) desktop.getExecution().createComponents(component, null, args);

			Component c = Path.getComponent("/win/" + win1.getId());
			if (c != null) {
				c.detach();
			}

			win1.setTitle(title);
			win1.setClosable(closabled);
			win1.setMaximizable(false);
			if (OVERLAPPED == type) {
				win1.doOverlapped();
			} else {
				win1.doHighlighted();
			}

			win1.setPosition("center");
			win1.setPage(desktop.getPage("mainWin"));
			win1.setParent(desktop.getPage("mainWin").getFellow("win"));
		} catch (Exception e) {
			log.error(e);
			if (win1 != null) {
				win1.detach();
			}

			throw UiException.Aide.wrap(e);
		}
	}

	public static void openWindow(String component, String title, Map args, int type) {
		Window win1 = null;

		try {
			win1 = (Window) Executions.createComponents(component, null, null);

			Component c = Path.getComponent("/win/" + win1.getId());
			if (c != null) {
				c.detach();
			}

			win1.setTitle(title);
			win1.setClosable(true);
			win1.setMaximizable(false);
			if (OVERLAPPED == type) {
				win1.doOverlapped();
			} else {
				win1.doHighlighted();
			}

			win1.setPosition("center");

		} catch (Exception e) {
			log.error(e);
			if (win1 != null) {
				win1.detach();
			}

			throw UiException.Aide.wrap(e);
		}
	}
}
