package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Classe;

public class RenderClasse implements ListitemRenderer<Classe> {

	@Override
	public void render(Listitem item, Classe classe, int index) throws Exception {
		Listcell lc = new Listcell(classe.getCodigoClasse());
		lc.setParent(item);

		lc = new Listcell(classe.getDescricao());
		lc.setParent(item);

		lc.setValue(classe);

	}

}
