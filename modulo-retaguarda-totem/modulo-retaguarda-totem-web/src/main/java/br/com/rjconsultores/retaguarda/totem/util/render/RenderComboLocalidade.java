package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;


public class RenderComboLocalidade implements ComboitemRenderer<Localidade>{

	@Override
	public void render(Comboitem item, Localidade data, int index) throws Exception {
		
		item.setValue(data);
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(data.getLocalidadeId());
		sb.append(" - ");
		sb.append(data.getCidade());
		
		item.setLabel(sb.toString());
		
	}

}
