package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

public class RenderLocalidade implements ListitemRenderer<Localidade> {

	@Override
	public void render(Listitem item, Localidade localidade, int index) throws Exception {

		Listcell lc = new Listcell(localidade.getLocalidadeId().toString());
		lc.setParent(item);
	
		lc = new Listcell(localidade.getCidade());
		lc.setParent(item);

//		lc = new Listcell(localidade.getLatitude() == null ? "" : localidade.getLatitude().setScale(9, RoundingMode.HALF_DOWN).toString());
//		lc.setParent(item);
//
//		lc = new Listcell(localidade.getLongitude() == null ? "" : localidade.getLongitude().setScale(9, RoundingMode.HALF_DOWN).toString());
//		lc.setParent(item);

		item.setValue(localidade);

	}

}
