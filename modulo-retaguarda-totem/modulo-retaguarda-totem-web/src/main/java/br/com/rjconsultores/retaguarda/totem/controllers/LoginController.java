package br.com.rjconsultores.retaguarda.totem.controllers;

import java.nio.channels.Selector;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import br.com.rjconsultores.authentication.AuthenticationServiceSession;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;
import br.com.rjconsultores.retaguarda.totem.service.UsuarioService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;

public class LoginController extends MyGenericSelectorComposer<Selector> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Wire
	private Textbox txtUsuario;

	@Wire
	private Textbox txtSenha;

	@WireVariable("usuarioService")
	private UsuarioService usuarioService;

	private AuthenticationServiceSession authServiceSession;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		authServiceSession = new AuthenticationServiceSession();
	}

	@Listen("onClick=#btnAcessar")
	public void onClick$btnAcessar(Event ev) {
		String login = txtUsuario.getValue();
		String senha = txtSenha.getValue();

		Usuario usu = usuarioService.buscarPorUsername(login);

		if (usu != null) {

			if (!usu.getSenha().equalsIgnoreCase(senha) && !authServiceSession.login(usu)) {

				Messagebox.show("Login ou senha inválidos!", "Retaguarda", Messagebox.OK, Messagebox.ERROR);
				return;
			}
		}

		Executions.sendRedirect("/index.zul");

	}

}
