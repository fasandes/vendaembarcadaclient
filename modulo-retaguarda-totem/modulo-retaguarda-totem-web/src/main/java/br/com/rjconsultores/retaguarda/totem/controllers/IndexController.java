package br.com.rjconsultores.retaguarda.totem.controllers;

import java.util.HashMap;
import java.util.Map;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Toolbar;

import br.com.rjconsultores.authentication.AuthenticationServiceSession;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericForwardComposer;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class IndexController extends MyGenericForwardComposer {

	private static final Logger log = Logger.getLogger(IndexController.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Wire
	private Toolbar toolBar;
	@Wire
	private Toolbar toolBarUser;
	@Wire
	private Label lblUsuario;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		AuthenticationServiceSession authService = new AuthenticationServiceSession();
		Usuario usuario = authService.getUserCredential();
		lblUsuario.setValue(usuario.getUsuario());

	}

	@Listen("onClick = #mniParametrosSRVP")
	public void onClick$mniParametrosSRVP(Event ev) {
		openWindow("/gui/parametros/servidor/srvp/cadastrarEditarServidor.zul", "Configurações Servidor", arg, MODAL,
				Boolean.TRUE);

	}

	@Listen("onClick = #mniParametrosTotalBus")
	public void onClick$mniParametrosTotalBus(Event ev) {
		openWindow("/gui/parametros/servidor/totalbus/cadastrarEditarServidorTotalBus.zul",
				"Configurações Servidor Total Bus", arg, MODAL, Boolean.TRUE);

	}

	@Listen("onClick = #mniParametrosEmbarcada")
	public void onClick$mniParametrosEmbarcada(Event ev) {
		openWindow("/gui/parametros/embarcada/editarParametrosEmbarcada.zul", "", arg, MODAL, Boolean.FALSE);

	}

	@Listen("onClick = #mniNovoUsuario")
	public void onClick$mniNovoUsuario(Event ev) {
		Map<String, Object> args = new HashMap<String, Object>();
		Boolean novo = true;
		args.put("novo", novo);
		openWindow("/gui/usuario/pesquisarUsuario.zul", "Cadastro/Edição Usuário", args, MODAL, Boolean.TRUE);

	}

	@Listen("onClick = #mniArquivoBematech")
	public void onClick$mniArquivoBematech(Event ev) {
		openWindow("/gui/exportacao/bematech/selecao.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniExportaArquivoBGM")
	public void onClick$mniExportaArquivoBGM(Event ev) {
		openWindow("/gui/exportacao/bematech/selecao.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniArquivoDaruma")
	public void onClick$mniArquivoDaruma(Event ev) {
		openWindow("/gui/exportacao/daruma/selecaoDaruma.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniFormaPagamento")
	public void onClick$mniFormaPagamento(Event ev) {
		openWindow("/gui/manutencao/formaPagamento/pesquisarFormaPagamento.zul", "Forma de Pagamento", arg, MODAL,
				Boolean.TRUE);
	}

	@Listen("onClick = #mniTipoPagamento")
	public void onClick$mniTipoPagamento(Event ev) {
		openWindow("/gui/manutencao/tipoPagamento/pesquisarTipoPagamento.zul", "Tipo de Pagamento", arg, MODAL,
				Boolean.TRUE);
	}

	@Listen("onClick = #mniOperador")
	public void onClick$mniOperador(Event ev) {
		openWindow("/gui/manutencao/operador/pesquisarOperador.zul", "Pesquisar Operador", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onclick = #mniVinculaOperadorLinha")
	public void onClick$mniVinculaOperadorLinha(Event ev) {
		openWindow("/gui/manutencao/operador/selecaoOperador.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onclick = #mniVincularTiposPagamento")
	public void onClick$mniVincularTiposPagamento(Event ev) {
		openWindow("/gui/manutencao/formaPagamento/vincularTiposPagamento.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniEdicaoEmpresa")
	public void onClick$mniEdicaoEmpresa(Event ev) {
		Map<String, Object> args = new HashMap<String, Object>();
		Boolean novo = true;
		args.put("novo", novo);
		openWindow("/gui/manutencao/empresa/pesquisarEmpresa.zul", "Cadastro/Edição Empresa", args, MODAL,
				Boolean.TRUE);

	}

	@Listen("onClick = #mniImportArquivosBGM")
	public void onClick$mniImportArquivosBGM(Event ev) {
		openWindow("/gui/importacao/bematech/arquivos.zul", "Importação BGM", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniImportArquivosDaruma")
	public void onClick$mniImportArquivosDaruma(Event ev) {
		// openWindow("/gui/importacao/daruma/selecaoMotoristaEmpresa.zul",
		// "Seleção Motorista - Empresa", arg, MODAL, Boolean.TRUE);
		openWindow("/gui/importacao/daruma/arquivosDaruma.zul", "Importação Daruma", arg, MODAL, Boolean.TRUE);
		// openWindow("/gui/importacao/daruma/arquivosDaruma.zul", "Importação
		// Daruma", arg, MODAL,
		// Boolean.TRUE);
	}

	@Listen("onClick = #mniEditarLocalidades")
	public void onClick$mniEditarLocalidades(Event ev) {
		openWindow("/gui/manutencao/localidades/pesquisarLocalidades.zul", "Pesquisar Localidades", arg, MODAL,
				Boolean.TRUE);
	}

	@Listen("onClick = #mniCoordenadas")
	public void onClick$mniCoordenadas(Event ev) {
		openWindow("/gui/manutencao/localidades/filtroEditarCoordenadas.zul", null, arg, MODAL, Boolean.TRUE);
		// openWindow("/gui/manutencao/localidades/editarCoordenadasLocalidades.zul",
		// null, arg, MODAL,
		// Boolean.TRUE);
	}

	@Listen("onClick = #mniVendas")
	public void onClick$mniVendas(Event ev) {
		openWindow("/gui/relatorios/vendas/filtroRelatorioVendas.zul", "Relatório Vendas", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniVendasViagem")
	public void onClick$mniVendasViagem(Event ev) {
		openWindow("/gui/relatorios/vendas/filtroRelatorioVendasViagem.zul", "Relatório Vendas por Viagem", arg, MODAL,
				Boolean.TRUE);
	}

	@Listen("onClick = #mniGerarVendaTotalBus")
	public void onClick$mniGerarVendaTotalBus(Event ev) {
		openWindow("/gui/manutencao/vendaTotalBus/geraVendaTotalBus.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #mniCadastroDispositivo")
	public void onClick$mniCadastroDispositivo(Event ev) {
		openWindow("/gui/manutencao/dispositivo/pesquisarDispositivo.zul", "Seleção", arg, MODAL, Boolean.TRUE);
	}

	@Listen("onClick = #toolBar")
	public void onClick$toolBar(Event ev) {
		AuthenticationServiceSession auth = new AuthenticationServiceSession();
		auth.logout();
	}

	@Listen("onClick = #toolBarUser")
	public void onClick$toolBarUser(Event ev) {
		Map<String, Object> args = new HashMap<String, Object>();
		Boolean novo = false;
		args.put("novo", novo);
		openWindow("/gui/usuario/cadastrarEditarUsuario.zul", "Configurações Servidor Total Bus", args, MODAL,
				Boolean.TRUE);
	}

	@Listen("onClick = #mniSuperLinha")
	public void onClick$mniSuperLinha(Event ev) {

		openWindow("/gui/manutencao/superLinha/pesquisarSuperLinha.zul", "Super Linha", null, MODAL, Boolean.TRUE);

	}

	@Listen("onClick = #mniEnvioSrvp")
	public void onClick$mniEnvioSrvp(Event ev) {

		openWindow("/gui/exportacao/srvp/filtroVendasExportadasSrvp.zul", "Filtro de Vendas", null, MODAL,
				Boolean.TRUE);

	}

}
