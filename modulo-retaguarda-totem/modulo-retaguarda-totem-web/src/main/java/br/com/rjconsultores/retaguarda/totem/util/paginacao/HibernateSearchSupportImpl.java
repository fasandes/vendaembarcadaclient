/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util.paginacao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Filter;
import org.hibernate.NonUniqueResultException;

import com.googlecode.genericdao.search.ExampleOptions;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;

/**
 * @author bbruhns
 *
 */
@Transactional
public class HibernateSearchSupportImpl implements HibernateSearchSupport {

	@Inject
	private JPASearchProcessor jpaSearchProcessor;
	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#count(java.lang.
	 * Class, com.trg.search.ISearch)
	 */
	public int count(Class<?> searchClass, ISearch search) {
		return jpaSearchProcessor.count(entityManager, searchClass, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#count(com.trg.search
	 * .ISearch)
	 */
	public int count(ISearch search) {
		return jpaSearchProcessor.count(entityManager, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#generateQL(java.
	 * lang.Class, com.trg.search.ISearch, java.util.List)
	 */
	public String generateQL(Class<?> entityClass, ISearch search, List<Object> paramList) {
		return jpaSearchProcessor.generateQL(entityClass, search, paramList);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#generateRowCountQL
	 * (java.lang.Class, com.trg.search.ISearch, java.util.List)
	 */
	public String generateRowCountQL(Class<?> entityClass, ISearch search, List<Object> paramList) {
		return jpaSearchProcessor.generateRowCountQL(entityClass, search, paramList);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#getFilterFromExample
	 * (java.lang.Object)
	 */
	public Filter getFilterFromExample(Object example) {
		return (Filter) jpaSearchProcessor.getFilterFromExample(example);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#getFilterFromExample
	 * (java.lang.Object, com.trg.search.ExampleOptions)
	 */
	public Filter getFilterFromExample(Object example, ExampleOptions options) {
		return (Filter) jpaSearchProcessor.getFilterFromExample(example, options);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#search(java.lang
	 * .Class, com.trg.search.ISearch)
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> search(Class<T> searchClass, ISearch search) {
		return jpaSearchProcessor.search(entityManager, searchClass, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.forsthaus.backend.dao.impl.HibernateSearchSupport#search(com.trg.
	 * search .ISearch)
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> search(ISearch search) {
		return jpaSearchProcessor.search(entityManager, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#searchAndCount(java
	 * .lang.Class, com.trg.search.ISearch)
	 */
	@SuppressWarnings("unchecked")
	public <T> com.googlecode.genericdao.search.SearchResult<T> searchAndCount(Class<T> searchClass, ISearch search) {
		return jpaSearchProcessor.searchAndCount(entityManager, searchClass, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#searchAndCount(com
	 * .trg.search.ISearch)
	 */
	@SuppressWarnings("unchecked")
	public <T> SearchResult<T> searchAndCount(ISearch search) {
		return jpaSearchProcessor.searchAndCount(entityManager, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#searchUnique(java
	 * .lang.Class, com.trg.search.ISearch)
	 */
	@SuppressWarnings("unchecked")
	public <T> T searchUnique(Class<T> entityClass, ISearch search) throws NonUniqueResultException {
		return (T) jpaSearchProcessor.searchUnique(entityManager, entityClass, search);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.forsthaus.backend.dao.impl.HibernateSearchSupport#searchUnique(com
	 * .trg.search.ISearch)
	 */
	public Object searchUnique(ISearch search) throws NonUniqueResultException {
		return jpaSearchProcessor.searchUnique(entityManager, search);
	}
}
