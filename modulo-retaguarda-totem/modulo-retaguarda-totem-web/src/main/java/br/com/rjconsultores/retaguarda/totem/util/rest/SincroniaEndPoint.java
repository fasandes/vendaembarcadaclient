package br.com.rjconsultores.retaguarda.totem.util.rest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import com.google.gson.Gson;

import br.com.rjconsultores.retaguarda.totem.bean.CaixaFechadoBean;
import br.com.rjconsultores.retaguarda.totem.bean.DetalheHorarioBean;
import br.com.rjconsultores.retaguarda.totem.bean.DetalheHorarioOperadorBean;
import br.com.rjconsultores.retaguarda.totem.bean.HorarioBean;
import br.com.rjconsultores.retaguarda.totem.bean.OperacaoBean;
import br.com.rjconsultores.retaguarda.totem.bean.OperadorLinhaBean;
import br.com.rjconsultores.retaguarda.totem.bean.OperadorSuperLinhaPKBean;
import br.com.rjconsultores.retaguarda.totem.bean.SuperLinhaBean;
import br.com.rjconsultores.retaguarda.totem.bean.SuperLinhaSequenciaBean;
import br.com.rjconsultores.retaguarda.totem.bean.VendaFechadaBean;
import br.com.rjconsultores.retaguarda.totem.bean.ViagemFechadaBean;
import br.com.rjconsultores.retaguarda.totem.dto.OperadorSrvpDto;
import br.com.rjconsultores.retaguarda.totem.dto.ParametrosGeracaoVenda;
import br.com.rjconsultores.retaguarda.totem.dto.VendaFechadaDto;
import br.com.rjconsultores.retaguarda.totem.dto.VendaGeradaSrvpDto;
import br.com.rjconsultores.retaguarda.totem.entidades.AliasLocali;
import br.com.rjconsultores.retaguarda.totem.entidades.CaixaFechado;
import br.com.rjconsultores.retaguarda.totem.entidades.CaixaFechadoBeanModel;
import br.com.rjconsultores.retaguarda.totem.entidades.CaixaFechadoPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Classe;
import br.com.rjconsultores.retaguarda.totem.entidades.DetalheHorario;
import br.com.rjconsultores.retaguarda.totem.entidades.DetalheHorarioPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.FormaPagamento;
import br.com.rjconsultores.retaguarda.totem.entidades.Horario;
import br.com.rjconsultores.retaguarda.totem.entidades.HorarioPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;
import br.com.rjconsultores.retaguarda.totem.entidades.Operacao;
import br.com.rjconsultores.retaguarda.totem.entidades.OperacaoPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.entidades.OperadorSuperLinha;
import br.com.rjconsultores.retaguarda.totem.entidades.Operadorlinha;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;
import br.com.rjconsultores.retaguarda.totem.entidades.Pda;
import br.com.rjconsultores.retaguarda.totem.entidades.SuperLinha;
import br.com.rjconsultores.retaguarda.totem.entidades.SuperLinhaSequencia;
import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaEmbarcadaCliVO;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaExportadaSrvp;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaExportadaSrvpPK;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaFechada;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaFechadaBeanModel;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaFechadaPK;
import br.com.rjconsultores.retaguarda.totem.entidades.ViagemFechada;
import br.com.rjconsultores.retaguarda.totem.entidades.ViagemFechadaBeanModel;
import br.com.rjconsultores.retaguarda.totem.entidades.ViagemFechadaPK;
import br.com.rjconsultores.retaguarda.totem.entidades.VinculoTipoPagamento;
import br.com.rjconsultores.retaguarda.totem.entidades.enums.EnumStatusVendaEmbarcada;
import br.com.rjconsultores.retaguarda.totem.service.AliasLocaliService;
import br.com.rjconsultores.retaguarda.totem.service.CaixaFechadoBeanModelService;
import br.com.rjconsultores.retaguarda.totem.service.CaixaFechadoService;
import br.com.rjconsultores.retaguarda.totem.service.CaixaFechadoStringService;
import br.com.rjconsultores.retaguarda.totem.service.ClasseService;
import br.com.rjconsultores.retaguarda.totem.service.DetalheHorarioService;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.service.FormaPagamentoService;
import br.com.rjconsultores.retaguarda.totem.service.HorarioService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.service.LocalidadeService;
import br.com.rjconsultores.retaguarda.totem.service.OperacaoService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorLinhaService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorSuperLinhaService;
import br.com.rjconsultores.retaguarda.totem.service.ParametroService;
import br.com.rjconsultores.retaguarda.totem.service.PdaService;
import br.com.rjconsultores.retaguarda.totem.service.SuperLinhaSequenciaService;
import br.com.rjconsultores.retaguarda.totem.service.SuperLinhaService;
import br.com.rjconsultores.retaguarda.totem.service.TipoPagamentoService;
import br.com.rjconsultores.retaguarda.totem.service.VendaExportadaSrvpService;
import br.com.rjconsultores.retaguarda.totem.service.VendaFechadaBeanModelService;
import br.com.rjconsultores.retaguarda.totem.service.VendaFechadaService;
import br.com.rjconsultores.retaguarda.totem.service.VendaFechadaStringService;
import br.com.rjconsultores.retaguarda.totem.service.ViagemFechadaBeanModelService;
import br.com.rjconsultores.retaguarda.totem.service.ViagemFechadaService;
import br.com.rjconsultores.retaguarda.totem.service.ViagemFechadaStringService;
import br.com.rjconsultores.retaguarda.totem.service.VinculoTipoPagamentoService;

@RequestScoped
@Path("/sincroniaAndroid")
@Named("sincroniaEndPoint")
public class SincroniaEndPoint extends ServiceEndpoint {

	@Inject
	private LocalidadeService localidadeService;

	@Inject
	private OperadorService operadorService;

	@Inject
	private ClasseService classeService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private FormaPagamentoService formaPagamentoService;

	@Inject
	private TipoPagamentoService tipoPagamentoService;

	@Inject
	private HorarioService horarioService;

	@Inject
	private LinhaService linhaService;

	@Inject
	private OperadorLinhaService operadorLinhaService;

	@Inject
	private OperadorSuperLinhaService operadorSuperLinhaService;

	@Inject
	private DetalheHorarioService detalheHorarioService;

	@Inject
	private OperacaoService operacaoService;

	@Inject
	private PdaService pdaService;

	@Inject
	private ViagemFechadaService viagemFechadaService;

	@Inject
	private CaixaFechadoService caixaFechadoService;

	@Inject
	private VendaFechadaBeanModelService vendaFechadaBeanModelService;

	@Inject
	private ViagemFechadaBeanModelService viagemFechadaBeanModelService;

	@Inject
	private CaixaFechadoBeanModelService caixaFechadoBeanModelService;

	@Inject
	private VendaFechadaService vendaFechadaService;

	@Inject
	private SuperLinhaService superLinhaService;

	@Inject
	private ParametroService parametroService;

	@Inject
	private SuperLinhaSequenciaService superLinhaSequenciaService;

	@Inject
	private CaixaFechadoStringService caixaFechadoStringService;

	@Inject
	private ViagemFechadaStringService viagemFechadaStringService;

	@Inject
	private VendaFechadaStringService vendaFechadaStringService;

	@Inject
	private WSConsumerVendaEmbarcada wsConsumerVendaEmbarcada;

	@Inject
	private VendaExportadaSrvpService vendaExportadaSrvpService;

	@Inject
	private AliasLocaliService aliasLocaliService;

	@Inject
	private WsConsumerGeraVendaSrvp wsConsumerGeraVendaSrvp;

	private List<Localidade> lsLocalidades;
	private List<Operador> lsOperadores;
	private List<Classe> lsClasses;
	private List<Empresa> lsEmpresas;
	private List<FormaPagamento> lsFormaPagamento;
	private List<TipoPagamento> lsTipoPagamento;
	private List<Horario> lsHorarios;
	private List<HorarioBean> lsHorarioBean;
	private List<DetalheHorarioOperadorBean> lsDetalheHorarioOperadorBean;
	private List<Linha> lsLinhas;
	private List<Operacao> lsOperacao;
	private List<OperacaoBean> lsOperacaoBean;
	private List<Pda> lsPda;
	private List<Operadorlinha> lsOperadorlinha;
	private List<AliasLocali> lsAliasLocali;
	private final Logger log = Logger.getLogger(SincroniaEndPoint.class);
	private List<VinculoTipoPagamento> lsVinculoTipoPagamento;

	@Inject
	private VinculoTipoPagamentoService vinculoTipoPagamentoService;

	@GET
	@Path("/validaConexao")
	@Produces(MediaType.APPLICATION_JSON)
	public void validaConexao(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);

		final Response resp = Response.ok(Boolean.TRUE).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaClasses")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaClasses(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Classes");
		lsClasses = classeService.buscarTodos();

		final Response resp = Response.ok(lsClasses).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaEmpresas")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaEmpresas(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Empresas");
		lsEmpresas = empresaService.buscarTodos();

		final Response resp = Response.ok(lsEmpresas).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaFormaPagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaFormaPagamento(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> FormaPagamento");
		lsFormaPagamento = formaPagamentoService.buscarTodosAtivos();

		final Response resp = Response.ok(lsFormaPagamento).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTipoPagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaTipoPagamento(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> TipoPagamento");
		lsTipoPagamento = tipoPagamentoService.buscarTodosAtivos();

		final Response resp = Response.ok(lsTipoPagamento).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosOperadores")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaOperadores(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Operadores");
		lsOperadores = operadorService.buscarTodosAtivos();

		final Response resp = Response.ok(lsOperadores).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodasLocalidades")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaLocalidades(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Localidades");
		lsLocalidades = localidadeService.buscarTodas();

		final Response resp = Response.ok(lsLocalidades).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodasVinculoTipoPagamento")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaTodasVinculoTipoPagamento(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Vinculo Tipo Pagamento");
		lsVinculoTipoPagamento = vinculoTipoPagamentoService.buscarTodos();

		final Response resp = Response.ok(lsVinculoTipoPagamento).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodasAliasLocali")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaTodasAliasLocali(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Alias Locali");
		lsAliasLocali = aliasLocaliService.buscarTodas();

		final Response resp = Response.ok(lsAliasLocali).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodasLinhas")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaLinhas(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Linhas");
		lsLinhas = linhaService.buscarTodos();

		final Response resp = Response.ok(lsLinhas).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosOperadorlinha")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaOperadorLinha(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> OperadorLinha");
		List<OperadorLinhaBean> lsOperadorlinhaBean = OperadorLinhaToOperadorLinhaBean(
				operadorLinhaService.buscarTodos());

		final Response resp = Response.ok(lsOperadorlinhaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosOperadorlinha/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaOperadorLinha(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> OperadorLinha");
		List<OperadorLinhaBean> lsOperadorlinhaBean = OperadorLinhaToOperadorLinhaBean(
				operadorLinhaService.lsHorariosOperador(codigoOperador));

		final Response resp = Response.ok(lsOperadorlinhaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosOperadorSuperLinha")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaTodosOperadorSuperLinha(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> OperadorSuperLinha");
		List<OperadorSuperLinhaPKBean> lsOperadorlinhaBean = operadorSuperLinhaPKToOperadorSuperLinhaBean(
				operadorSuperLinhaService.buscarTodos());

		final Response resp = Response.ok(lsOperadorlinhaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosOperadorSuperLinha/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaTodosOperadorSuperLinha(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> OperadorSuperLinha");
		List<OperadorSuperLinhaPKBean> lsOperadorlinhaBean = operadorSuperLinhaPKToOperadorSuperLinhaBean(
				operadorSuperLinhaService.lsHorariosOperador(codigoOperador));

		final Response resp = Response.ok(lsOperadorlinhaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosSuperLinha")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaSuperLinha(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Super Linha");

		List<SuperLinhaSequenciaBean> lsSuperLinhaSequenciaBean = this
				.superLinhaSequenciaToSuperLinhaSequenciaBean(superLinhaSequenciaService.buscarTodos());

		final Response resp = Response.ok(lsSuperLinhaSequenciaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaTodosSuperLinha/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaSuperLinhaOperador(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Super Linha");

		List<SuperLinhaSequenciaBean> lsSuperLinhaSequenciaBean = this.superLinhaSequenciaToSuperLinhaSequenciaBean(
				superLinhaSequenciaService.buscarTodosPorOperador(codigoOperador));

		final Response resp = Response.ok(lsSuperLinhaSequenciaBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaHorariosOperador/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaHorariosOperador(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Horario Operador");
		lsOperadorlinha = retornaListaOperadorLinha(codigoOperador);

		lsHorarios = retornaListaHorariosOperador(lsOperadorlinha);

		lsHorarioBean = horarioToHorarioBean(lsHorarios);

		final Response resp = Response.ok(lsHorarioBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaDetalheHorarioOperador/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaDetalheHorarioOperador(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Detalhe_Horario Operador");
		lsDetalheHorarioOperadorBean = retornaListaDetalheHorarioOperador(codigoOperador);

		final Response resp = Response.ok(lsDetalheHorarioOperadorBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaLinhasOperador/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaLinhasOperador(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Linhas Operador");
		lsLinhas = retornaListaLinhasOperador(codigoOperador);

		final Response resp = Response.ok(lsLinhas).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaLocalidadesOperador/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaLocalidadesOperador(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Localidades Operador");
		List<Integer> lsIdsLocalidades = retornaListaIdsLocalidadePorOperador(codigoOperador);

		lsLocalidades = localidadeService.buscarPorIds(lsIdsLocalidades);

		final Response resp = Response.ok(lsLocalidades).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaOperacoes/{codigoOperador}")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaOperacoes(@Suspended AsyncResponse asyncResponse,
			@PathParam("codigoOperador") Integer codigoOperador) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Operaçoes");
		lsOperacaoBean = retornaListaOperacaoBean(codigoOperador);

		final Response resp = Response.ok(lsOperacaoBean).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	@GET
	@Path("/baixaPda")
	@Produces(MediaType.APPLICATION_JSON)
	public void baixaPda(@Suspended AsyncResponse asyncResponse) {
		this.trataTimeOut(asyncResponse);
		log.info("Sincronizando:::::::>>> Pda");
		lsPda = pdaService.buscarTodos();

		final Response resp = Response.ok(lsPda).type(MediaType.APPLICATION_JSON).build();

		asyncResponse.resume(resp);
	}

	// @POST
	// @Path("/descarregaViagens")
	// @Consumes(MediaType.APPLICATION_JSON)
	// public void descarregaViagens(@Suspended AsyncResponse asyncResponse,
	// List<ViagemFechadaBean> lsViagemFechadaBean) {
	// this.trataTimeOut(asyncResponse);
	// log.info("Inicio do envio das Viagens");
	// List<ViagemFechada> lsViagemFechada =
	// viagemFechadaBeanToViagemFechada(lsViagemFechadaBean);
	// for (ViagemFechada viagem : lsViagemFechada) {
	// // System.out.println(viagem.toString());
	// if (viagemFechadaService.buscarPorId(viagem.getViagemFechadaPK()) ==
	// null) {
	// viagemFechadaService.salvar(viagem);
	// }
	// }
	//
	// final Response resp =
	// Response.ok(lsViagemFechada).type(MediaType.APPLICATION_JSON +
	// ";charset=utf-8").build();
	//
	// if (resp.getStatus() != 200) {
	// try {
	// throw new Exception("Failed : HTTP error code : " + resp.getStatus());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// log.info("Viagens Recebidas");
	// asyncResponse.resume(resp);
	//
	// }

	@POST
	@Path("/descarregaViagens")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaViagens(@Suspended AsyncResponse asyncResponse,
			List<ViagemFechadaBeanModel> lsViagemFechadaBean) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio do envio das Viagens");
		for (ViagemFechadaBeanModel viagem : lsViagemFechadaBean) {
			// System.out.println(viagem.toString());
			if (viagemFechadaBeanModelService.buscarViagem(viagem.getIdDispositivo(), viagem.getCaixaId(),
					viagem.getHoraViagem()) == null) {
				viagemFechadaBeanModelService.salvar(viagem);
			}
		}

		final Response resp = Response.ok(lsViagemFechadaBean).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.info("Viagens Recebidas");
		asyncResponse.resume(resp);

	}

	// @POST
	// @Path("/descarregaCaixas")
	// @Consumes(MediaType.APPLICATION_JSON)
	// // public void descarregaVendaEmbarcada(@Suspended AsyncResponse
	// // asyncResponse, EmbarcadaServicoDTO embarcadaServicoDTO) {
	// public void descarregaCaixas(@Suspended AsyncResponse asyncResponse,
	// List<CaixaFechadoBean> lsCaixaFechadoBean) {
	// this.trataTimeOut(asyncResponse);
	// log.info("Inicio do envio dos Caixas");
	// List<CaixaFechado> lsCaixaFechado =
	// caixaFechadoBeanToCaixaFechado(lsCaixaFechadoBean);
	// for (CaixaFechado caixa : lsCaixaFechado) {
	// // System.out.println(caixa.toString());
	//
	// if (caixaFechadoService.buscarPorId(caixa.getCaixaFechadoPK()) == null) {
	// caixaFechadoService.salvar(caixa);
	// }
	// }
	//
	// final Response resp =
	// Response.ok(lsCaixaFechado).type(MediaType.APPLICATION_JSON +
	// ";charset=utf-8").build();
	//
	// if (resp.getStatus() != 200) {
	// try {
	// throw new Exception("Failed : HTTP error code : " + resp.getStatus());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// log.info("Caixas Recebidos");
	// asyncResponse.resume(resp);
	//
	// }

	@POST
	@Path("/descarregaCaixas")
	@Consumes(MediaType.APPLICATION_JSON)
	// public void descarregaVendaEmbarcada(@Suspended AsyncResponse
	// asyncResponse, EmbarcadaServicoDTO embarcadaServicoDTO) {
	public void descarregaCaixas(@Suspended AsyncResponse asyncResponse,
			List<CaixaFechadoBeanModel> lsCaixaFechadoBean) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio do envio dos Caixas");
		for (CaixaFechadoBeanModel caixa : lsCaixaFechadoBean) {
			// System.out.println(caixa.toString());
			if (caixaFechadoBeanModelService.buscarCaixa(caixa.getIdDispositivo(), caixa.getOperador().toString(),
					caixa.getHoraAbertura()) == null) {
				caixaFechadoBeanModelService.salvar(caixa);
			}
		}

		final Response resp = Response.ok(lsCaixaFechadoBean).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		log.info("Caixas Recebidos");
		asyncResponse.resume(resp);

	}

	// @POST
	// @Path("/descarregaVendas")
	// @Consumes(MediaType.APPLICATION_JSON)
	// public void descarregaVendas(@Suspended AsyncResponse asyncResponse,
	// List<VendaFechadaBean> lsVendaFechadaBean) {
	// this.trataTimeOut(asyncResponse);
	// log.info("Inicio do envio das Vendas");
	// List<VendaFechada> lsVendaFechada =
	// vendaFechadaBeanToVendaFechada(lsVendaFechadaBean);
	// for (VendaFechada venda : lsVendaFechada) {
	// if (vendaFechadaService.buscarPorId(venda.getVendaFechadaPK()) == null) {
	// vendaFechadaService.salvar(venda);
	// }
	// }
	//
	// final Response resp =
	// Response.ok(lsVendaFechada).type(MediaType.APPLICATION_JSON +
	// ";charset=utf-8").build();
	//
	// if (resp.getStatus() != 200) {
	// try {
	// throw new Exception("Failed : HTTP error code : " + resp.getStatus());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// log.info("Vendas Recebidas");
	// asyncResponse.resume(resp);
	//
	// }

	@POST
	@Path("/descarregaVendas")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaVendas(@Suspended AsyncResponse asyncResponse,
			List<VendaFechadaBeanModel> lsVendaFechadaBean) {
		this.trataTimeOut(asyncResponse);

		List<VendaFechadaBeanModel> lsVendaFechadaBeanModel = new ArrayList<VendaFechadaBeanModel>();
		VendaFechadaDto vendaFechadaListDto = null;
		ParametrosGeracaoVenda parametrosGeracaoVenda = preecheParametros();
		String[] retornoVenda;

		log.info("Inicio do envio das Vendas");
		for (VendaFechadaBeanModel venda : lsVendaFechadaBean) {
			if (vendaFechadaBeanModelService.buscarVenda(venda.getIdDispositivo(), venda.getNumeroBilhete(),
					venda.getOperador().toString(), venda.getDataAberturaVenda(), venda.getViagemId()) == null) {
				venda.setRetornoVenda(99);
				vendaFechadaBeanModelService.salvar(venda);
				lsVendaFechadaBeanModel.add(venda);
			}
		}

		final Response resp = Response.ok(lsVendaFechadaBean).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			log.info("Vendas Recebidas");
			asyncResponse.resume(resp);

			List<Pda> lsPda = pdaService.buscarTodos();

			if (parametroService.buscarPorId(Parametro.IMPORTACAO_AUTOMATICA_SRVP) != null
					&& parametroService.buscarPorId(Parametro.IMPORTACAO_AUTOMATICA_SRVP).getValor().equals("0")) {

				for (VendaFechadaBean vendaFechadaBean : vendaFechadaBeanModelToVendaFechadaBean(
						lsVendaFechadaBeanModel)) {
					vendaFechadaListDto = new VendaFechadaDto();
					vendaFechadaListDto.setVendaFechadaBean(vendaFechadaBean);
					vendaFechadaListDto.setParametrosGeracaoVenda(parametrosGeracaoVenda);
					vendaFechadaListDto.setEnderecoWsRetornoVenda(
							parametroService.buscarPorId(Parametro.ENDERECO_WS_RETORNO_VENDA_SRVP).getValor());
					VendaFechada vf = new VendaFechada();
					vf = vendaFechadaBeanToVendaFechada(vendaFechadaBean);
					vendaFechadaListDto.setCodigoTipoPagamento(vf.getCodigoTipoPagamento().getCodigoTipoPagamento());
					vendaFechadaListDto.setPercentualDescontoTipoPagamento(vf.getCodigoTipoPagamento().getFator());

					retornoVenda = wsConsumerGeraVendaSrvp.geraVendaSrvp(vendaFechadaListDto,
							parametroService.buscarPorId(Parametro.ENDERECO_WS_GERA_VENDA_SRVP));

					if (retornoVenda[0] != null && Integer.parseInt(retornoVenda[0]) == 0) {
						vendaFechadaBean.setCodigoSessao(0);// utilizando o
															// codigo da sessao
															// para enviar o
															// retorno da venda
						registraExportacao(vendaFechadaBean);
					}

				}
			}

		}
		log.info("Vendas Recebidas");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/descarregaStringCaixa")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaStringCaixa(@Suspended AsyncResponse asyncResponse, List<String> lsStringCaixaFechado) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio do envio  String dos Caixas");
		// for (String caixa : lsStringCaixaFechado) {
		// CaixaFechadoString caixaFechadoString = new CaixaFechadoString();
		// caixaFechadoString.setCaixaString(caixa);
		// caixaFechadoStringService.salvar(caixaFechadoString);
		// }

		final Response resp = Response.ok(lsStringCaixaFechado).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("String :::>>> Caixas Recebidos");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/descarregaStringViagem")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaStringViagem(@Suspended AsyncResponse asyncResponse, List<String> lsStringViagemFechada) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio do envio  String das Viagens");
		// for (String viagem : lsStringViagemFechada) {
		// ViagemFechadaString viagemFechadaString = new ViagemFechadaString();
		// viagemFechadaString.setViagemString(viagem);
		// viagemFechadaStringService.salvar(viagemFechadaString);
		// }

		final Response resp = Response.ok(lsStringViagemFechada).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("String :::>>> Viagens Recebidas");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/descarregaStringVendas")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaStringVendas(@Suspended AsyncResponse asyncResponse, List<String> lsStringVendaFechada) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio do envio  String das Vendas");
		// for (String venda : lsStringVendaFechada) {
		// VendaFechadaString vendaFechadaString = new VendaFechadaString();
		// vendaFechadaString.setVendaString(venda);
		// vendaFechadaStringService.salvar(vendaFechadaString);
		// }

		final Response resp = Response.ok(lsStringVendaFechada).type(MediaType.APPLICATION_JSON + ";charset=utf-8")
				.build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("String :::>>> Vendas Recebidas");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/retornoVendaSrvp")
	@Consumes(MediaType.APPLICATION_JSON)
	public void retornoVendaSrvp(@Suspended AsyncResponse asyncResponse, String vendaGeradaSrvpJson) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio recebimento das vendas geradas::>>>");
		Gson gson = new Gson();
		VendaGeradaSrvpDto vendaGeradaSrvpDto = gson.fromJson(vendaGeradaSrvpJson, VendaGeradaSrvpDto.class);

		VendaExportadaSrvp exportada = new VendaExportadaSrvp();

		VendaFechadaPK vendaFechadaPK = new VendaFechadaPK();
		VendaFechadaBeanModel vfBeanModel = vendaFechadaBeanModelService
				.buscarVendaPeloIdDispositivo(vendaGeradaSrvpDto.getVendaGeradaSrvp().getIdDispositivo());
		String idDispositivo = vfBeanModel.getIdDispositivo();
		vendaFechadaPK.setIdDispositivo(idDispositivo);
		vendaFechadaPK.setVendaId(vendaGeradaSrvpDto.getVendaGeradaSrvp().getIdVenda());

		exportada.setVendaFechadaBeanModel(vfBeanModel);
		exportada.setData(dateToCalendar(vendaGeradaSrvpDto.getVendaGeradaSrvp().getDataImportacao()));

		VendaExportadaSrvpPK pk = new VendaExportadaSrvpPK();

		pk.setVendaFechadaBeanModelVendaBeanId(vfBeanModel.getVendaBeanId());
		pk.setId(vfBeanModel.getVendaId());
		exportada.setVendaExportadaSrvpPK(pk);

		vendaExportadaSrvpService.savarOuAtualizar(exportada);

		final Response resp = Response.ok(exportada).type(MediaType.APPLICATION_JSON + ";charset=utf-8").build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("String :::>>> Retorno Vendas Concluído com Sucesso!");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/cadastroSenhaVendaEmbarcada")
	@Consumes(MediaType.APPLICATION_JSON)
	public void cadastroSenhaVendaEmbarcada(@Suspended AsyncResponse asyncResponse, OperadorSrvpDto operadorSrvpDto) {
		this.trataTimeOut(asyncResponse);
		log.info("Inicio da alteração da senha::>>>");
		operadorSrvpDto.toString();

		Operador operador = operadorService.buscarPorId(operadorSrvpDto.getMatricula());
		operador.setOperadorSenha(operadorSrvpDto.getNovaSenha().toString());
		operador.setIdDispositivo(operadorSrvpDto.getIdDispositivo());
		operador.setDataAlteracao(new Date());
		operadorService.atualizar(operador);

		final Response resp = Response.ok(operadorSrvpDto).type(MediaType.APPLICATION_JSON + ";charset=utf-8").build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("Cadastro Senha :::>>> Senha alterada com sucesso!");
		asyncResponse.resume(resp);

	}

	@POST
	@Path("/descarregaVendasSrvp")
	@Consumes(MediaType.APPLICATION_JSON)
	public void descarregaVendasSrvp(@Suspended AsyncResponse asyncResponse,
			List<VendaFechadaBean> lsVendaFechadaBean) {
		this.trataTimeOut(asyncResponse);

	}

	@POST
	@Path("/atualizaCoordenadas")
	@Consumes(MediaType.APPLICATION_JSON)
	public void atualizaCoordenadas(@Suspended AsyncResponse asyncResponse,
			List<DetalheHorarioBean> lsDetalheHorarioBean) {
		this.trataTimeOut(asyncResponse);
		List<DetalheHorario> lsDetalheHorario = DetalheHorarioBeanToDetalheHorario(lsDetalheHorarioBean);
		for (DetalheHorario detalheHorario : lsDetalheHorario) {
			// System.out.println(venda.toString());
			detalheHorarioService.atualizar(detalheHorario);
		}

		final Response resp = Response.ok(DetalheHorarioBeanToDetalheHorario(lsDetalheHorarioBean))
				.type(MediaType.APPLICATION_JSON + ";charset=utf-8").build();

		if (resp.getStatus() != 200) {
			try {
				throw new Exception("Failed : HTTP error code : " + resp.getStatus());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		asyncResponse.resume(resp);

	}

	public List<Operadorlinha> retornaListaOperadorLinha(Integer codigoOperador) {
		List<Operadorlinha> lsOperadorlinha = new ArrayList<Operadorlinha>();
		lsOperadorlinha = operadorLinhaService.lsHorariosOperador(codigoOperador);
		return lsOperadorlinha;
	}

	public List<Horario> retornaListaHorariosOperador(List<Operadorlinha> lsOperadorlinha) {

		List<Horario> listaHorario = new ArrayList<Horario>();
		for (int i = 0; i < lsOperadorlinha.size(); i++) {
			listaHorario.add(lsOperadorlinha.get(i).getHorario());
		}

		return listaHorario;
	}

	public List<Horario> retornaListaHorariosSuperLinha() {

		List<Horario> listaHorario = new ArrayList<Horario>();
		List<SuperLinhaSequencia> listSp = superLinhaSequenciaService.buscarTodos();
		for (SuperLinhaSequencia sp : listSp) {
			listaHorario.add(sp.getHorario());
		}

		return listaHorario;
	}

	public List<Integer> retornaListaIdsLocalidadePorOperador(Integer codigoOperador) {

		lsDetalheHorarioOperadorBean = retornaListaDetalheHorarioOperador(codigoOperador);
		List<Integer> lsIdsLocalidades = new ArrayList<Integer>();

		for (int i = 0; i < lsDetalheHorarioOperadorBean.size(); i++) {
			lsIdsLocalidades.add(lsDetalheHorarioOperadorBean.get(i).getLocalidade().getLocalidadeId());
		}
		return lsIdsLocalidades;
	}

	public List<DetalheHorarioOperadorBean> retornaListaDetalheHorarioOperador(Integer codigoOperador) {

		List<DetalheHorario> listaDH = new ArrayList<DetalheHorario>();
		List<DetalheHorarioOperadorBean> listaDHBean = new ArrayList<DetalheHorarioOperadorBean>();
		lsOperadorlinha = retornaListaOperadorLinha(codigoOperador);
		lsHorarios = retornaListaHorariosOperador(lsOperadorlinha);
		lsHorarios.addAll(retornaListaHorariosSuperLinha());
		listaDH = detalheHorarioService.buscarPorHorarios(lsHorarios);

		for (int i = 0; i < listaDH.size(); i++) {
			DetalheHorarioOperadorBean detalheHorarioBean = new DetalheHorarioOperadorBean();
			detalheHorarioBean.setHorario(listaDH.get(i).getHorario().getHorarioPK().getCodigoHorario());// voltar
			// tipo
			// para
			// Horario
			detalheHorarioBean.setLatitude(listaDH.get(i).getLatitude());
			detalheHorarioBean.setLocalidade(listaDH.get(i).getLocalidade());
			detalheHorarioBean.setLongitude(listaDH.get(i).getLongitude());
			detalheHorarioBean
					.setSequenciaDaLocalidade(listaDH.get(i).getDetalheHorarioPK().getSequenciaDaLocalidade());
			listaDHBean.add(detalheHorarioBean);
		}
		return listaDHBean;
	}

	public List<Linha> retornaListaLinhasOperador(Integer codigoOperador) {

		List<Linha> listaLinha = new ArrayList<>();
		lsOperadorlinha = retornaListaOperadorLinha(codigoOperador);

		for (int i = 0; i < lsOperadorlinha.size(); i++) {
			Linha linha = lsOperadorlinha.get(i).getHorario().getLinha();
			if (!listaLinha.contains(linha)) {
				listaLinha.add(linha);
			}
		}
		return listaLinha;
	}

	public List<Linha> retornaListaLinhasSuperLinha() {

		List<Linha> listaLinha = new ArrayList<>();

		List<SuperLinhaSequencia> listSp = superLinhaSequenciaService.buscarTodos();

		for (SuperLinhaSequencia sp : listSp) {
			listaLinha.add(sp.getHorario().getLinha());
		}

		return listaLinha;
	}

	public List<OperacaoBean> retornaListaOperacaoBean(Integer codigoOperador) {
		// OperacaoBean opBean = new OperacaoBean();
		List<OperacaoBean> lsOperacaoBean = new ArrayList<OperacaoBean>();

		lsLinhas = retornaListaLinhasOperador(codigoOperador);
		lsLinhas.addAll(retornaListaLinhasSuperLinha());
		lsOperacao = operacaoService.buscarPorLinhas(lsLinhas);

		for (int i = 0; i < lsOperacao.size(); i++) {
			OperacaoBean opBean = new OperacaoBean();
			opBean.setClasse(lsOperacao.get(i).getClasse());
			opBean.setCodigoPais(lsOperacao.get(i).getCodigoPais());
			opBean.setDestino(lsOperacao.get(i).getDestino());
			opBean.setDistanciaTrecho(lsOperacao.get(i).getDistanciaTrecho());
			opBean.setEmpresa(lsOperacao.get(i).getEmpresa());
			opBean.setLinha(lsOperacao.get(i).getLinha());
			opBean.setOrigem(lsOperacao.get(i).getOrigem());
			opBean.setValorOutros(lsOperacao.get(i).getValorOutros());
			opBean.setValorPedagio(lsOperacao.get(i).getValorPedagio());
			opBean.setValorSeguro(lsOperacao.get(i).getValorSeguro());
			opBean.setValorTarifa(lsOperacao.get(i).getValorTarifa());
			opBean.setValorTaxa(lsOperacao.get(i).getValorTaxa());
			opBean.setPermiteVenda(lsOperacao.get(i).getPermiteVenda());
			lsOperacaoBean.add(opBean);
		}

		return lsOperacaoBean;
	}

	public List<OperadorLinhaBean> OperadorLinhaToOperadorLinhaBean(List<Operadorlinha> lsOperadorlinha) {
		List<OperadorLinhaBean> lsOperadorlinhaBean = new ArrayList<>();

		for (Operadorlinha operadorLinha : lsOperadorlinha) {
			OperadorLinhaBean opBean = new OperadorLinhaBean();
			opBean.setOperador(operadorService.buscarPorId(operadorLinha.getOperadorlinhaPK().getOperadorOperadorId()));
			opBean.setCodigoEmpresa(
					empresaService.buscarPorCodigo(operadorLinha.getOperadorlinhaPK().getHorarioCodigoEmpresa()));
			opBean.setCodigoHorario(operadorLinha.getOperadorlinhaPK().getHorarioCodigoHorario());
			opBean.setCodigoLinha(linhaService.buscarPorId(operadorLinha.getOperadorlinhaPK().getHorarioCodigoLinha()));
			opBean.setDestinoHorario(
					localidadeService.buscarPorId(operadorLinha.getOperadorlinhaPK().getHorarioDestinoHorario()));
			opBean.setHoraHorario(operadorLinha.getOperadorlinhaPK().getHorarioHoraHorario());
			opBean.setOrigemHorario(
					localidadeService.buscarPorId(operadorLinha.getOperadorlinhaPK().getHorarioOrigemHorario()));
			opBean.setSentidoHorario(operadorLinha.getOperadorlinhaPK().getHorarioSentidoHorario());
			// opBean.setHorarioBean(horarioToHorarioBean(horarioService.buscarHorario(operadorLinha.getHorario().getHorarioPK().getCodigoHorario()
			// ,operadorLinha.getHorario().getLinha().getCodigoLinha(),
			// operadorLinha.getHorario().getHorarioPK().getOrigemHorario(),
			// operadorLinha.getHorario().getHorarioPK().getDestinoHorario(),
			// operadorLinha.getHorario().getHorarioPK().getSentidoHorario())));
			opBean.setDataCriacao(operadorLinha.getDataCriacao());
			lsOperadorlinhaBean.add(opBean);
		}

		return lsOperadorlinhaBean;

	}

	public List<OperadorSuperLinhaPKBean> operadorSuperLinhaPKToOperadorSuperLinhaBean(
			List<OperadorSuperLinha> lsOperadorlinha) {
		List<OperadorSuperLinhaPKBean> lsOperadorlinhaBean = new ArrayList<>();

		for (OperadorSuperLinha operadorLinha : lsOperadorlinha) {
			OperadorSuperLinhaPKBean bean = new OperadorSuperLinhaPKBean();

			bean.setOperador(
					operadorService.buscarPorId(operadorLinha.getOperadorSuperLinhaPK().getOperadorOperadorId()));
			// bean.setSuperLinhaBean(superLinhaToSuperLinhaBean(superLinhaService.buscarPorId(operadorLinha.getOperadorSuperLinhaPK().getSuperLinhaId())));
			bean.setSuperLinha(
					superLinhaService.buscarPorId(operadorLinha.getOperadorSuperLinhaPK().getSuperLinhaId()));
			lsOperadorlinhaBean.add(bean);
		}

		return lsOperadorlinhaBean;

	}

	public List<CaixaFechado> caixaFechadoBeanToCaixaFechado(List<CaixaFechadoBean> lsCaixaFechadoBean) {
		List<CaixaFechado> lsCaixaFechado = new ArrayList<>();

		for (CaixaFechadoBean caixaFechadoBean : lsCaixaFechadoBean) {
			CaixaFechado caixaFechado = new CaixaFechado();
			CaixaFechadoPK caixaFechadoPK = new CaixaFechadoPK();
			caixaFechadoPK.setCaixaId(caixaFechadoBean.getCaixaId());
			caixaFechadoPK.setIdDispositivo(caixaFechadoBean.getIdDispositivo());
			caixaFechado.setCaixaFechadoPK(caixaFechadoPK);
			caixaFechado.setCodigoSessao(caixaFechadoBean.getCodigoSessao());
			caixaFechado.setDataAbertura(caixaFechadoBean.getDataAbertura());
			caixaFechado.setHoraAbertura(caixaFechadoBean.getHoraAbertura());
			caixaFechado.setDataFechamento(caixaFechadoBean.getDataFechamento());
			caixaFechado.setHoraFechamento(caixaFechadoBean.getHoraFechamento());
			caixaFechado.setValorTotalVendas(caixaFechadoBean.getValorTotalVendas());
			caixaFechado.setValorTotalComissoes(caixaFechadoBean.getValorTotalComissoes());
			caixaFechado.setValorTotalAcertos(caixaFechadoBean.getValorTotalAcertos());
			caixaFechado.setValorTotalTrocos(caixaFechadoBean.getValorTotalTrocos());
			caixaFechado.setValorLiquidoCaixa(caixaFechadoBean.getValorLiquidoCaixa());
			caixaFechado.setEmpresaCodigoEmpresa(empresaService.buscarPorCodigo(caixaFechadoBean.getEmpresa()));
			caixaFechado.setOperadorOperadorId(operadorService.buscarPorId(caixaFechadoBean.getOperador()));
			caixaFechado.setNumeroVeiculo(caixaFechadoBean.getNumeroVeiculo());
			caixaFechado.setNumeroCatraca(caixaFechadoBean.getNumeroCatraca());
			lsCaixaFechado.add(caixaFechado);
		}
		return lsCaixaFechado;
	}

	public List<ViagemFechada> viagemFechadaBeanToViagemFechada(List<ViagemFechadaBean> lsViagemFechadaBean) {
		List<ViagemFechada> lsViagemFechada = new ArrayList<>();
		for (ViagemFechadaBean viagemFechadaBean : lsViagemFechadaBean) {
			ViagemFechada viagemFechada = new ViagemFechada();
			ViagemFechadaPK viagemFechadaPK = new ViagemFechadaPK();
			viagemFechadaPK.setIdViagem(viagemFechadaBean.getIdViagem());
			viagemFechadaPK.setIdDispositivo(viagemFechadaBean.getIdDispositivo());
			viagemFechada.setViagemFechadaPK(viagemFechadaPK);
			viagemFechada.setCodigoVeiculo(viagemFechadaBean.getCodigoVeiculo());
			viagemFechada.setDataViagem(viagemFechadaBean.getDataViagem());
			viagemFechada.setHoraViagem(viagemFechadaBean.getHoraViagem());
			viagemFechada.setSituacaoViagem(viagemFechadaBean.getSituacaoViagem());
			viagemFechada.setDataFechamentoViagem(viagemFechadaBean.getDataFechamentoViagem());
			viagemFechada.setHoraFechamentoViagem(viagemFechadaBean.getHoraFechamentoViagem());
			HorarioPK horarioPK = new HorarioPK();
			Horario horario = new Horario();
			horarioPK.setCodigoHorario(viagemFechadaBean.getCodigoHorario());
			horarioPK.setCodigoLinha(viagemFechadaBean.getCodigoLinha());
			horarioPK.setCodigoEmpresa(viagemFechadaBean.getCodigoEmpresa());
			horarioPK.setOrigemHorario(viagemFechadaBean.getOrigemHorario());
			horarioPK.setDestinoHorario(viagemFechadaBean.getDestinoHorario());
			horarioPK.setHoraHorario(viagemFechadaBean.getHoraHorario());
			horarioPK.setSentidoHorario(viagemFechadaBean.getSentidoHorario());
			horario.setLinha(linhaService.buscarPorId(viagemFechadaBean.getCodigoLinha()));
			horario.setEmpresa(empresaService.buscarPorCodigo(viagemFechadaBean.getCodigoEmpresa()));
			horario.setOrigemLocalidade(localidadeService.buscarPorId(viagemFechadaBean.getOrigemHorario()));
			horario.setDestinoLocalidade(localidadeService.buscarPorId(viagemFechadaBean.getDestinoHorario()));
			horario.setHorarioPK(horarioPK);
			viagemFechada.setHorario(horario);

			CaixaFechadoPK cpk = new CaixaFechadoPK();

			cpk.setCaixaId(viagemFechadaBean.getCaixaId());
			cpk.setIdDispositivo(viagemFechadaBean.getIdDispositivo());

			CaixaFechado caixaFechado = caixaFechadoService.buscarPorId(cpk);

			viagemFechada.setCaixaFechado(caixaFechado);

			// viagemFechada.setCaixaFechado(caixaFechadoService.buscarCaixa(viagemFechadaBean.getCaixaValorTotalVendas(),
			// viagemFechadaBean.getCaixaValorTotalComissoes(),
			// viagemFechadaBean.getCaixaValorTotalAcertos(),
			// viagemFechadaBean.getCaixaValorTotalTrocos(),
			// viagemFechadaBean.getCaixaValorLiquidoCaixa(),
			// viagemFechadaBean.getCaixaEmpresa(),
			// viagemFechadaBean.getCaixaOperador(),
			// viagemFechadaBean.getCaixaNumeroVeiculo(),
			// viagemFechadaBean.getCaixaNumeroCatraca()));
			// viagemFechada.setCaixaFechado(caixaFechadoService.buscarPorId(viagemFechadaBean.getCaixaId()));
			lsViagemFechada.add(viagemFechada);
		}
		return lsViagemFechada;
	}

	public List<VendaFechada> vendaFechadaBeanToVendaFechada(List<VendaFechadaBean> lsVendaFechadaBean) {
		List<VendaFechada> lsVendaFechada = new ArrayList<>();
		for (VendaFechadaBean vendaFechadaBean : lsVendaFechadaBean) {
			VendaFechada vendaFechada = new VendaFechada();
			VendaFechadaPK vendaFechadaPK = new VendaFechadaPK();
			vendaFechadaPK.setVendaId(vendaFechadaBean.getVendaId());
			vendaFechadaPK.setIdDispositivo(vendaFechadaBean.getIdDispositivo());
			// vendaFechadaPK.setPdaId(vendaFechadaBean.getPda());
			vendaFechada.setVendaFechadaPK(vendaFechadaPK);
			// vendaFechada.setViagemFechada(viagemFechadaService.buscarViagem(vendaFechadaBean.getViagemCodigoVeiculo(),
			// vendaFechadaBean.getViagemSituacaoViagem(),
			// vendaFechadaBean.getViagemCodigoHorario(),
			// vendaFechadaBean.getViagemDataViagem(),
			// vendaFechadaBean.getDataAberturaViagem(),
			// vendaFechadaBean.getViagemDataFechamentoViagem()));
			vendaFechada.setCodigoTipoPagamento(tipoPagamentoService.buscarPorId(vendaFechadaBean.getCodigoVenda()));
			vendaFechada.setNumeroBilhete(vendaFechadaBean.getNumeroBilhete());
			vendaFechada.setDataAberturaViagem(vendaFechadaBean.getDataAberturaViagem());
			vendaFechada.setDataAberturaVenda(vendaFechadaBean.getDataAberturaVenda());
			vendaFechada.setDataAberturaCaixa(vendaFechadaBean.getDataAberturaCaixa());
			vendaFechada.setHoraAberturaVenda(vendaFechadaBean.getHoraAberturaVenda());
			vendaFechada.setHoraAberturaCaixa(vendaFechadaBean.getHoraAberturaCaixa());
			vendaFechada.setCodigoSessao(vendaFechadaBean.getCodigoSessao());
			vendaFechada.setCodigoVenda(vendaFechadaBean.getCodigoVenda());
			vendaFechada.setValorTotalBilhete(vendaFechadaBean.getValorTotalBilhete() != null
					? vendaFechadaBean.getValorTotalBilhete() : BigDecimal.ZERO);
			vendaFechada.setTipoVenda(vendaFechadaBean.getTipoVenda());
			vendaFechada.setCodigoVenda(vendaFechadaBean.getCodigoVenda());
			vendaFechada.setCodigoVeiculo(vendaFechadaBean.getCodigoVeiculo());
			vendaFechada.setDataFechamento(vendaFechadaBean.getDataFechamento());
			vendaFechada.setHoraFechamento(vendaFechadaBean.getHoraFechamento());
			vendaFechada.setValorTaxaEmbarque(vendaFechadaBean.getValorTaxaEmbarque() != null
					? vendaFechadaBean.getValorTaxaEmbarque() : BigDecimal.ZERO);
			vendaFechada.setValorSeguro(
					vendaFechadaBean.getValorSeguro() != null ? vendaFechadaBean.getValorSeguro() : BigDecimal.ZERO);
			vendaFechada.setVendaCancelada(vendaFechadaBean.getVendaCancelada());
			vendaFechada.setValorPedagio(
					vendaFechadaBean.getValorPedagio() != null ? vendaFechadaBean.getValorPedagio() : BigDecimal.ZERO);
			vendaFechada.setValorOutros(
					vendaFechadaBean.getValorOutros() != null ? vendaFechadaBean.getValorOutros() : BigDecimal.ZERO);
			vendaFechada.setDocumento(vendaFechadaBean.getDocumento() == "" ? "0" : vendaFechadaBean.getDocumento());
			vendaFechada.setNumeroBilhete(vendaFechadaBean.getNumeroBilhete());
			vendaFechada.setPdaId(pdaService.buscarPorId(vendaFechadaBean.getPda()));
			Horario horario = horarioService.buscarPorCodigoHorario(vendaFechadaBean.getCodigoHorario());
			// Horario horario =
			// horarioService.buscarHorario(vendaFechadaBean.getLinha(),
			// vendaFechadaBean.getOrigem(),
			// vendaFechadaBean.getViagemSentidoViagem(),
			// vendaFechadaBean.getViagemHoraViagem());
			vendaFechada.setHorario(horario);
			vendaFechada.setOperadorId(operadorService.buscarPorId(vendaFechadaBean.getOperador()));
			vendaFechada.setFormaPagamento(formaPagamentoService.buscarPorId(vendaFechadaBean.getFormaPagamento()));
			// vendaFechada.setOrigem(localidadeService.buscarPorId(vendaFechadaBean.getOrigem()));
			// vendaFechada.setDestino(localidadeService.buscarPorId(vendaFechadaBean.getDestino()));
			// vendaFechada.setEmpresa(empresaService.buscarPorCodigo(vendaFechadaBean.getEmpresa()));
			// vendaFechada.setLinha(linhaService.buscarPorId(vendaFechadaBean.getLinha()));
			// vendaFechada.setClasse(classeService.buscarPorId(vendaFechadaBean.getClasse()));
			OperacaoPK operacaoPK = new OperacaoPK();
			operacaoPK.setCodigoEmpresa(vendaFechadaBean.getEmpresa());
			operacaoPK.setCodigoLinha(vendaFechadaBean.getLinha());
			operacaoPK.setOrigemId(vendaFechadaBean.getOrigem());
			operacaoPK.setDestinoId(vendaFechadaBean.getDestino());
			operacaoPK.setCodigoClasse(vendaFechadaBean.getClasse());
			vendaFechada.setOperacao(operacaoService.buscarPorId(operacaoPK));
			// vendaFechada.setViagemFechada(viagemFechadaService.buscarViagem(vendaFechadaBean.getViagemCodigoVeiculo(),
			// vendaFechadaBean.getViagemSituacaoViagem(),
			// vendaFechadaBean.getViagemCodigoHorario()));
			vendaFechada.setCodigoTipoPagamento(tipoPagamentoService.buscarPorId(vendaFechada.getCodigoVenda()));

			ViagemFechadaPK viagemFechadaPK = new ViagemFechadaPK();
			viagemFechadaPK.setIdViagem(vendaFechadaBean.getViagemId());
			viagemFechadaPK.setIdDispositivo(vendaFechadaBean.getIdDispositivo());

			ViagemFechada viagemFechada = viagemFechadaService.buscarPorId(viagemFechadaPK);

			vendaFechada.setViagemFechada(viagemFechada);

			vendaFechada.setRetornoTotalBus(99);
			lsVendaFechada.add(vendaFechada);
		}
		return lsVendaFechada;
	}

	public List<HorarioBean> horarioToHorarioBean(List<Horario> lsHorario) {
		List<HorarioBean> lsHorarioBean = new ArrayList<>();

		for (Horario horario : lsHorario) {
			HorarioBean hb = new HorarioBean();
			hb.setCodigoEmpresa(horario.getEmpresa());
			hb.setCodigoHorario(horario.getHorarioPK().getCodigoHorario());
			hb.setCodigoLinha(horario.getLinha());
			hb.setDestinoHorario(localidadeService.buscarPorId(horario.getHorarioPK().getDestinoHorario()));
			hb.setOrigemHorario(localidadeService.buscarPorId(horario.getHorarioPK().getOrigemHorario()));
			hb.setHoraHorario(horario.getHorarioPK().getHoraHorario());
			hb.setSentidoHorario(horario.getHorarioPK().getSentidoHorario());
			hb.setTdsHorario(horario.getTdsHorario());
			lsHorarioBean.add(hb);
		}

		return lsHorarioBean;
	}

	public SuperLinhaBean superLinhaToSuperLinhaBean(SuperLinha superLinha) {
		SuperLinhaBean bean = new SuperLinhaBean();

		bean.setDescricao(superLinha.getDescricao());
		bean.setDestino(superLinha.getDestino());
		bean.setOrigem(superLinha.getOrigem());
		bean.setId(superLinha.getId());

		return bean;
	}

	public List<SuperLinhaBean> superLinhaToSuperLinhaBean(List<SuperLinha> superLinhalList) {
		List<SuperLinhaBean> listBean = new ArrayList<>();

		for (Iterator<SuperLinha> iterator = superLinhalList.iterator(); iterator.hasNext();) {
			SuperLinha superLinha = (SuperLinha) iterator.next();

			SuperLinhaBean bean = new SuperLinhaBean();

			bean.setDescricao(superLinha.getDescricao());
			bean.setDestino(superLinha.getDestino());
			bean.setOrigem(superLinha.getOrigem());
			bean.setId(superLinha.getId());

			listBean.add(bean);

		}

		return listBean;
	}

	public HorarioBean horarioToHorarioBean(Horario horario) {

		HorarioBean hb = new HorarioBean();
		hb.setCodigoEmpresa(horario.getEmpresa());
		hb.setCodigoHorario(horario.getHorarioPK().getCodigoHorario());
		hb.setCodigoLinha(horario.getLinha());
		hb.setDestinoHorario(localidadeService.buscarPorId(horario.getHorarioPK().getDestinoHorario()));
		hb.setOrigemHorario(localidadeService.buscarPorId(horario.getHorarioPK().getOrigemHorario()));
		hb.setHoraHorario(horario.getHorarioPK().getHoraHorario());
		hb.setSentidoHorario(horario.getHorarioPK().getSentidoHorario());

		return hb;
	}

	// public List<Horario> HorarioBeanToHorario(List<HorarioBean>
	// lsHorarioBean) {
	// List<Horario> lsHorario = new ArrayList<>();
	//
	// for (HorarioBean hBean : lsHorarioBean) {
	// Horario horario = new Horario();
	// HorarioPK hPK = new HorarioPK();
	// hPK.setCodigoHorario(hBean.getCodigoHorario());
	// hPK.setCodigoLinha(hBean.getCodigoLinha().getCodigoLinha());
	// hPK.setCodigoEmpresa(hBean.getCodigoEmpresa().getCodigoEmpresa());
	// hPK.setOrigemHorario(hBean.getOrigemHorario().getLocalidadeId());
	// hPK.setDestinoHorario(hBean.getDestinoHorario().getLocalidadeId());
	// hPK.setHoraHorario(hBean.getHoraHorario());
	// hPK.setSentidoHorario(hBean.getSentidoHorario());
	// horario.setHorarioPK(hPK);
	// horario.setEmpresa(hBean.getCodigoEmpresa());
	// horario.setLinha(hBean.getCodigoLinha());
	// horario.setLocalidade(hBean.getOrigemHorario());
	// horario.setLocalidade1(hBean.getDestinoHorario());
	//
	// lsHorario.add(horario);
	// }
	//
	// return lsHorario;
	// }

	public List<DetalheHorario> DetalheHorarioBeanToDetalheHorario(List<DetalheHorarioBean> lsDetalheHorarioBean) {
		List<DetalheHorario> lsDetalheHorario = new ArrayList<>();

		for (DetalheHorarioBean dhBean : lsDetalheHorarioBean) {

			DetalheHorario dh = new DetalheHorario();
			DetalheHorarioPK dhPK = new DetalheHorarioPK();
			dhPK.setSequenciaDaLocalidade(dhBean.getSequenciaDaLocalidade());
			dhPK.setHorarioCodigoHorario(dhBean.getCodigoHorario());
			dhPK.setHorarioCodigoLinha(dhBean.getCodigoLinha());
			dhPK.setHorarioCodigoEmpresa(dhBean.getCodigoEmpresa());
			dhPK.setHorarioDestinoHorario(dhBean.getDestinoHorario());
			dhPK.setHorarioOrigemHorario(dhBean.getOrigemHorario());
			dhPK.setHorarioHoraHorario(dhBean.getHoraHorario());
			dhPK.setHorarioSentidoHorario(dhBean.getSentidoHorario());
			// dh = detalheHorarioService.buscarPorChave(dhPK);
			dh = detalheHorarioService.buscarPorChave(dhPK, 0);

			if (dh != null) {
				dh.setLatitude(dhBean.getLatitude());
				dh.setLongitude(dhBean.getLongitude());
				lsDetalheHorario.add(dh);
			}
		}

		return lsDetalheHorario;
	}

	private List<SuperLinhaSequenciaBean> superLinhaSequenciaToSuperLinhaSequenciaBean(
			List<SuperLinhaSequencia> lsSuperLinhaSequencia) {
		List<SuperLinhaSequenciaBean> lsSuperLinhaSequenciaBean = new ArrayList<>();

		for (SuperLinhaSequencia superLinhaSequencia : lsSuperLinhaSequencia) {
			SuperLinhaSequenciaBean spBean = new SuperLinhaSequenciaBean();
			spBean.setHorario(this.horarioToHorarioBean(superLinhaSequencia.getHorario()));
			spBean.setSequencia(superLinhaSequencia.getSequencia());
			spBean.setSuperLinha(this.superLinhaToSuperLinhaBean(superLinhaSequencia.getSuperLinha()));

			lsSuperLinhaSequenciaBean.add(spBean);
		}

		return lsSuperLinhaSequenciaBean;
	}

	public List<VendaEmbarcadaCliVO> registraVendaTotalBus(List<VendaEmbarcadaCliVO> listaVendaDaruma) {
		List<VendaEmbarcadaCliVO> lsTotalBus = new ArrayList<>();

		for (VendaEmbarcadaCliVO vendaEmbarcada : listaVendaDaruma) {
			int codigoStatus = wsConsumerVendaEmbarcada.gerarVendaTotalBus(vendaEmbarcada,
					parametroService.buscarPorId(Parametro.ENDERECO_ADM));
			vendaEmbarcada.setStatusVenda(EnumStatusVendaEmbarcada.retornaStatus(codigoStatus));
			lsTotalBus.add(vendaEmbarcada);
			// for (EnumStatusVendaEmbarcada status:
			// EnumStatusVendaEmbarcada.values()){
			// if (codigoStatus == status.getCodigoStatus()){
			// vendaEmbarcada.setStatusVenda(status);
			// break;
			// }
			// }
		}
		return lsTotalBus;
	}

	public List<VendaFechadaBean> vendaFechadaBeanModelToVendaFechadaBean(
			List<VendaFechadaBeanModel> listaSelecionados) {
		List<VendaFechadaBean> lsVendaFechadaBean = new ArrayList<VendaFechadaBean>();
		for (VendaFechadaBeanModel vendaFechadaBeanModel : listaSelecionados) {
			SimpleDateFormat dtFull = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.US);
			SimpleDateFormat dtHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
			dtHora.setTimeZone(TimeZone.getTimeZone("GMT-3"));
			dt.setTimeZone(TimeZone.getTimeZone("GMT-3"));
			VendaFechadaBean vendaFechadaBean = new VendaFechadaBean();
			vendaFechadaBean.setVendaId(vendaFechadaBeanModel.getVendaId());
			vendaFechadaBean.setNumeroBilhete(vendaFechadaBeanModel.getNumeroBilhete());
			try {
				vendaFechadaBean.setDataAberturaViagem(converteDataFull(vendaFechadaBeanModel.getDataAberturaViagem()));
				vendaFechadaBean
						.setDataAberturaVenda(dtFull.parse(vendaFechadaBeanModel.getDataAberturaVenda().toString()));
				vendaFechadaBean.setDataAberturaCaixa(converteDataFull(vendaFechadaBeanModel.getDataAberturaCaixa()));
				vendaFechadaBean.setHoraAberturaVenda(converteDataFull(vendaFechadaBeanModel.getHoraAberturaVenda()));
				vendaFechadaBean.setHoraAberturaCaixa(converteDataFull(vendaFechadaBeanModel.getHoraAberturaCaixa()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			vendaFechadaBean.setCodigoSessao(vendaFechadaBeanModel.getCodigoSessao());
			vendaFechadaBean.setCodigoVenda(vendaFechadaBeanModel.getCodigoVenda());
			vendaFechadaBean.setValorTotalBilhete(vendaFechadaBeanModel.getValorTotalBilhete());
			vendaFechadaBean.setTipoVenda(vendaFechadaBeanModel.getTipoVenda());
			vendaFechadaBean.setCodigoVeiculo(vendaFechadaBeanModel.getCodigoVeiculo());
			vendaFechadaBean.setDataFechamento(vendaFechadaBeanModel.getDataFechamento());
			vendaFechadaBean.setHoraFechamento(vendaFechadaBeanModel.getHoraFechamento());
			vendaFechadaBean.setValorTaxaEmbarque(vendaFechadaBeanModel.getValorTaxaEmbarque());
			vendaFechadaBean.setValorSeguro(vendaFechadaBeanModel.getValorSeguro());
			vendaFechadaBean.setVendaCancelada(vendaFechadaBeanModel.getVendaCancelada());
			vendaFechadaBean.setValorPedagio(vendaFechadaBeanModel.getValorPedagio());
			vendaFechadaBean.setValorOutros(vendaFechadaBeanModel.getValorOutros());
			vendaFechadaBean.setDocumento(vendaFechadaBeanModel.getDocumento().toString());
			vendaFechadaBean.setPda(vendaFechadaBeanModel.getPda());
			vendaFechadaBean.setClasse(vendaFechadaBeanModel.getClasse());
			vendaFechadaBean.setEmpresa(vendaFechadaBeanModel.getEmpresa());
			vendaFechadaBean.setDestino(vendaFechadaBeanModel.getDestino());
			vendaFechadaBean.setOrigem(vendaFechadaBeanModel.getOrigem());
			vendaFechadaBean.setLinha(vendaFechadaBeanModel.getLinha());
			vendaFechadaBean.setCodigoHorario(vendaFechadaBeanModel.getCodigoHorario());
			vendaFechadaBean.setViagemId(vendaFechadaBeanModel.getViagemId());
			vendaFechadaBean.setOperador(vendaFechadaBeanModel.getOperador());
			vendaFechadaBean.setFormaPagamento(vendaFechadaBeanModel.getFormaPagamento());
			vendaFechadaBean.setOperadorEmail(
					vendaFechadaBeanModel.getOperadorEmail() != null ? vendaFechadaBeanModel.getOperadorEmail() : "");

			// ViagemFechada viagemFechada =
			// viagemFechadaService.buscarPorId(getViagemPK(vendaFechadaBeanModel));

			// Horario horario = getHorario(vendaFechadaBeanModel);
			vendaFechadaBean.setViagemCodigoVeiculo(vendaFechadaBeanModel.getCodigoVeiculo());
			vendaFechadaBean.setViagemDataViagem(converteDataFull(vendaFechadaBeanModel.getViagemDataViagem()));
			if (converteDataFull(vendaFechadaBeanModel.getViagemDataFechamentoViagem()) != null) {
				vendaFechadaBean.setViagemDataFechamentoViagem(
						converteDataFull(vendaFechadaBeanModel.getViagemDataFechamentoViagem()));
			} else {
				vendaFechadaBean
						.setViagemDataFechamentoViagem(converteDataFull(vendaFechadaBeanModel.getViagemDataViagem()));
			}
			if (vendaFechadaBeanModel.getViagemHoraFechamentoViagem() != null) {
				vendaFechadaBean.setViagemHoraFechamentoViagem(
						converteDataFull(vendaFechadaBeanModel.getViagemHoraFechamentoViagem()));
			} else {
				vendaFechadaBean
						.setViagemHoraFechamentoViagem(converteDataFull(vendaFechadaBeanModel.getHoraAberturaVenda()));
			}
			vendaFechadaBean.setViagemHoraViagem(vendaFechadaBeanModel.getViagemHoraViagem());
			vendaFechadaBean.setViagemSituacaoViagem(vendaFechadaBeanModel.getViagemSituacaoViagem());
			vendaFechadaBean.setViagemCodigoHorario(vendaFechadaBeanModel.getViagemCodigoHorario());
			vendaFechadaBean.setViagemSentidoViagem(vendaFechadaBeanModel.getViagemSentidoViagem());
			vendaFechadaBean.setIdDispositivo(vendaFechadaBeanModel.getIdDispositivo());
			lsVendaFechadaBean.add(vendaFechadaBean);
		}
		return lsVendaFechadaBean;
	}

	private ParametrosGeracaoVenda preecheParametros() {
		ParametrosGeracaoVenda parametrosGeracaoVenda = new ParametrosGeracaoVenda();
		parametrosGeracaoVenda.setLibSrvp(parametroService.buscarPorId(Parametro.LIB_SRVP).getValor());
		parametrosGeracaoVenda.setOperadorAgencia(parametroService.buscarPorId(Parametro.AGENCIA_SRVP).getValor());
		parametrosGeracaoVenda.setSenhaSrvp(parametroService.buscarPorId(Parametro.SENHA_SRVP).getValor());
		parametrosGeracaoVenda.setServidorSrvp(parametroService.buscarPorId(Parametro.IP_SRVP).getValor());
		parametrosGeracaoVenda.setUsuarioSrvp(parametroService.buscarPorId(Parametro.USUARIO_SRVP).getValor());
		return parametrosGeracaoVenda;
	}

	public VendaFechada vendaFechadaBeanToVendaFechada(VendaFechadaBean vendaFechadaBean) {
		VendaFechada vendaFechada = new VendaFechada();
		VendaFechadaPK vendaFechadaPK = new VendaFechadaPK();
		vendaFechadaPK.setVendaId(vendaFechadaBean.getVendaId());
		vendaFechadaPK.setIdDispositivo(vendaFechadaBean.getIdDispositivo().toString());
		vendaFechada.setVendaFechadaPK(vendaFechadaPK);
		vendaFechada.setCodigoTipoPagamento(tipoPagamentoService.buscarPorId(vendaFechadaBean.getCodigoVenda()));
		vendaFechada.setNumeroBilhete(vendaFechadaBean.getNumeroBilhete());
		vendaFechada.setDataAberturaViagem(vendaFechadaBean.getDataAberturaViagem());
		vendaFechada.setDataAberturaVenda(vendaFechadaBean.getDataAberturaVenda());
		vendaFechada.setDataAberturaCaixa(vendaFechadaBean.getDataAberturaCaixa());
		vendaFechada.setHoraAberturaVenda(vendaFechadaBean.getHoraAberturaVenda());
		vendaFechada.setHoraAberturaCaixa(vendaFechadaBean.getHoraAberturaCaixa());
		vendaFechada.setCodigoSessao(vendaFechadaBean.getCodigoSessao());
		vendaFechada.setCodigoVenda(vendaFechadaBean.getCodigoVenda());
		vendaFechada.setValorTotalBilhete(vendaFechadaBean.getValorTotalBilhete() != null
				? vendaFechadaBean.getValorTotalBilhete() : BigDecimal.ZERO);
		vendaFechada.setTipoVenda(vendaFechadaBean.getTipoVenda());
		vendaFechada.setCodigoVenda(vendaFechadaBean.getCodigoVenda());
		vendaFechada.setCodigoVeiculo(vendaFechadaBean.getCodigoVeiculo());
		vendaFechada.setDataFechamento(vendaFechadaBean.getDataFechamento());
		vendaFechada.setHoraFechamento(vendaFechadaBean.getHoraFechamento());
		vendaFechada.setValorTaxaEmbarque(vendaFechadaBean.getValorTaxaEmbarque() != null
				? vendaFechadaBean.getValorTaxaEmbarque() : BigDecimal.ZERO);
		vendaFechada.setValorSeguro(
				vendaFechadaBean.getValorSeguro() != null ? vendaFechadaBean.getValorSeguro() : BigDecimal.ZERO);
		vendaFechada.setVendaCancelada(vendaFechadaBean.getVendaCancelada());
		vendaFechada.setValorPedagio(
				vendaFechadaBean.getValorPedagio() != null ? vendaFechadaBean.getValorPedagio() : BigDecimal.ZERO);
		vendaFechada.setValorOutros(
				vendaFechadaBean.getValorOutros() != null ? vendaFechadaBean.getValorOutros() : BigDecimal.ZERO);
		vendaFechada.setDocumento(vendaFechadaBean.getDocumento() == "" ? "0" : vendaFechadaBean.getDocumento());
		vendaFechada.setNumeroBilhete(vendaFechadaBean.getNumeroBilhete());
		vendaFechada.setPdaId(pdaService.buscarPorId(vendaFechadaBean.getPda()));
		Horario horario = horarioService.buscarPorCodigoHorario(vendaFechadaBean.getCodigoHorario());
		vendaFechada.setHorario(horario);
		vendaFechada.setOperadorId(operadorService.buscarPorId(vendaFechadaBean.getOperador()));
		vendaFechada.setFormaPagamento(formaPagamentoService.buscarPorId(vendaFechadaBean.getFormaPagamento()));
		OperacaoPK operacaoPK = new OperacaoPK();
		operacaoPK.setCodigoEmpresa(vendaFechadaBean.getEmpresa());
		operacaoPK.setCodigoLinha(vendaFechadaBean.getLinha());
		operacaoPK.setOrigemId(vendaFechadaBean.getOrigem());
		operacaoPK.setDestinoId(vendaFechadaBean.getDestino());
		operacaoPK.setCodigoClasse(vendaFechadaBean.getClasse());
		vendaFechada.setOperacao(operacaoService.buscarPorId(operacaoPK));
		vendaFechada.setCodigoTipoPagamento(tipoPagamentoService.buscarPorId(vendaFechada.getCodigoVenda()));

		ViagemFechadaPK viagemFechadaPK = new ViagemFechadaPK();
		viagemFechadaPK.setIdViagem(vendaFechadaBean.getViagemId());
		viagemFechadaPK.setIdDispositivo(vendaFechadaBean.getIdDispositivo());

		ViagemFechada viagemFechada = viagemFechadaService.buscarPorId(viagemFechadaPK);

		vendaFechada.setViagemFechada(viagemFechada);

		vendaFechada.setRetornoTotalBus(99);
		return vendaFechada;
	}

	private void registraExportacao(VendaFechadaBean vendaFechadaBean) {

		// VendaFechadaPK vfPK = new VendaFechadaPK();
		// vfPK.setIdDispositivo(vendaFechada.getVendaFechadaPK().getIdDispositivo());
		// vfPK.setVendaId(vendaFechada.getVendaFechadaPK().getVendaId());
		// VendaFechada vf = vendafechadaService.buscarPorId(vfPK);
		VendaFechadaBeanModel vf = vendaFechadaBeanModelService.buscarVenda(vendaFechadaBean.getIdDispositivo(),
				vendaFechadaBean.getNumeroBilhete(), vendaFechadaBean.getOperador().toString(),
				vendaFechadaBean.getDataAberturaVenda(), vendaFechadaBean.getViagemId());
		vf.setRetornoVenda(vendaFechadaBean.getCodigoSessao() != null ? vendaFechadaBean.getCodigoSessao() : 99);

		try {
			vendaFechadaBeanModelService.atualizar(vf);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		VendaExportadaSrvp exportada = new VendaExportadaSrvp();

		exportada.setData(Calendar.getInstance());
		exportada.setVendaFechadaBeanModel(vf);

		VendaExportadaSrvpPK pk = new VendaExportadaSrvpPK();

		pk.setId(vf.getVendaId());
		pk.setVendaFechadaBeanModelVendaBeanId(vf.getVendaBeanId());
		exportada.setVendaExportadaSrvpPK(pk);

		try {
			vendaExportadaSrvpService.savarOuAtualizar(exportada);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private Calendar dateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public Date converteDataFull(Date dataParaConverter) {
		SimpleDateFormat dt = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.US);
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		String data = dataParaConverter.toString();

		try {
			Date date = dt.parse(data);
			Date date2 = dt1.parse(dt1.format(date));

			return date2;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// public static void main(String[] args) {
	// SimpleDateFormat dt = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz
	// yyyy", Locale.US);
	// SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
	// String data = "Mon Feb 06 17:27:24 BRST 2017";
	//
	// try {
	// Date date = dt.parse(data);
	// Date date2 = dt1.parse(dt1.format(date));
	//
	// System.out.println(date2);
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
}
