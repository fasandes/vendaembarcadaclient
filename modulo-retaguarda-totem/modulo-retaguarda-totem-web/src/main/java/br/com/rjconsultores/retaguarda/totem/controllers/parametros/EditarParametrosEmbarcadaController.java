package br.com.rjconsultores.retaguarda.totem.controllers.parametros;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.PadroesParametroEmbarcado;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;
import br.com.rjconsultores.retaguarda.totem.entidades.ParametroEmbarcado;
import br.com.rjconsultores.retaguarda.totem.service.ParametroEmbarcadoService;
import br.com.rjconsultores.retaguarda.totem.service.ParametroService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class EditarParametrosEmbarcadaController extends MyGenericSelectorComposer<Component> {

	@Wire
	private Checkbox chkPedagio;
	@Wire
	private Checkbox chkOutros;
	@Wire
	private Checkbox chkCarro;
	@Wire
	private Checkbox chkCatraca;
	@Wire
	private Checkbox chkEmbarque;
	@Wire
	private Checkbox chkSeguro;
	@Wire
	private Checkbox chkTaxa;
	// @Wire
	// private Checkbox chkAvancoLocalidadeGPS;
	// @Wire
	// private Checkbox chkFiscal;
	@Wire
	private Radio rdObrigatorioSeguro;
	@Wire
	private Radio rdRemovidoSeguro;
	@Wire
	private Radio rdMarcadoSeguro;
	@Wire
	private Radio rdDesmarcadoSeguro;
	@Wire
	private Radio rdObrigatorioEmbarque;
	@Wire
	private Radio rdRemovidoEmbarque;
	@Wire
	private Radio rdMarcadoEmbarque;
	@Wire
	private Radio rdDesmarcadoEmbarque;
	@Wire
	private Window winEditarParametrosEmbarcada;
	@Wire
	private Radio rdAvancoAutomatico;
	@Wire
	private Radio rdAvancoManual;
	@Wire
	private Radio rdAvancoAutomaticoManual;
	@Wire
	private Radio rdgrpGPS;
	@Wire
	private Radio rdgrpCancelamento;
	@Wire
	private Radio rdCancelamentoAtivado;
	@Wire
	private Radio rdCancelamentoDesativado;
	@Wire
	private Radio rdgrpSegundaVia;
	@Wire
	private Radio rdgrpConfirmaVenda;
	@Wire
	private Radio rdSegundaViaAtivado;
	@Wire
	private Radio rdSegundaViaDesativado;
	@Wire
	private Radio rdConfirmaVendaAtivado;
	@Wire
	private Radio rdConfirmaVendaDesativado;
	@Wire
	private Radio rdCadastroSenhaAtivado;
	@Wire
	private Radio rdCadastroSenhaDesativado;
	@WireVariable("parametroEmbarcadoService")
	private ParametroEmbarcadoService parametroEmbarcadoService;
	@WireVariable("pda")
	private ParametroEmbarcado parametroEmbarcado;
	@Wire
	private Radio rdImportacaoAutomaticaAtivado;
	@Wire
	private Radio rdImportacaoAutomaticaDesativada;
	@WireVariable("parametroService")
	private ParametroService parametroService;
	@WireVariable("parametro")
	private Parametro parametro;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		parametroEmbarcado = parametroEmbarcadoService.buscarPorId(1);

		chkOutros.setChecked(parametroEmbarcado.getCobraOutros());
		chkPedagio.setChecked(parametroEmbarcado.getCobraPedagio());
		// chkEmbarque.setChecked(pda.getPadraoEmbarque()==1?true:false);
		// chkSeguro.setChecked(pda.getPadraoSeguro()==1?true:false);
		// chkAvancoLocalidadeGPS.setChecked(pda.getAvancaLocalidadeGps());
		// chkFiscal.setChecked(pda.getFiscal());
		PadroesParametroEmbarcado escolhaSeguro = PadroesParametroEmbarcado.values()[parametroEmbarcado
				.getPadraoSeguro()];
		switch (escolhaSeguro) {
		case OBRIGATORIO:
			rdObrigatorioSeguro.setChecked(true);
			break;
		case OPCIONAL_DESMARCADO:
			rdDesmarcadoSeguro.setChecked(true);
			break;
		case OPCIONAL_MARCADO:
			rdMarcadoSeguro.setChecked(true);
			break;
		case REMOVIDO:
			rdRemovidoSeguro.setChecked(true);
			break;

		default:
			break;
		}
		PadroesParametroEmbarcado escolhaEmbarque = PadroesParametroEmbarcado.values()[parametroEmbarcado
				.getPadraoEmbarque()];
		switch (escolhaEmbarque) {
		case OBRIGATORIO:
			rdObrigatorioEmbarque.setChecked(true);
			break;
		case OPCIONAL_DESMARCADO:
			rdDesmarcadoEmbarque.setChecked(true);
			break;
		case OPCIONAL_MARCADO:
			rdMarcadoEmbarque.setChecked(true);
			break;
		case REMOVIDO:
			rdRemovidoEmbarque.setChecked(true);
			break;

		default:
			break;
		}

	}

	@Listen("onClick = #btnSalvar")
	public void onClick$btnSalvar(Event ev) {
		parametroEmbarcado.setCobraOutros(chkOutros.isChecked());
		parametroEmbarcado.setCobraPedagio(chkPedagio.isChecked());
		// pda.setPadraoEmbarque(chkEmbarque.isChecked()==true?1:0);
		// pda.setPadraoSeguro(chkSeguro.isChecked()==true?1:0);
		// pda.setAvancaLocalidadeGps(chkAvancoLocalidadeGPS.isChecked());
		// pda.setFiscal(chkFiscal.isChecked());

		if (rdDesmarcadoSeguro.isChecked()) {
			parametroEmbarcado.setPadraoSeguro(PadroesParametroEmbarcado.OPCIONAL_DESMARCADO.getValor());
		} else if (rdMarcadoSeguro.isChecked()) {
			parametroEmbarcado.setPadraoSeguro(PadroesParametroEmbarcado.OPCIONAL_MARCADO.getValor());
		} else if (rdObrigatorioSeguro.isChecked()) {
			parametroEmbarcado.setPadraoSeguro(PadroesParametroEmbarcado.OBRIGATORIO.getValor());
		} else if (rdRemovidoSeguro.isChecked()) {
			parametroEmbarcado.setPadraoSeguro(PadroesParametroEmbarcado.REMOVIDO.getValor());
		}

		if (rdDesmarcadoEmbarque.isChecked()) {
			parametroEmbarcado.setPadraoEmbarque(PadroesParametroEmbarcado.OPCIONAL_DESMARCADO.getValor());
		} else if (rdMarcadoEmbarque.isChecked()) {
			parametroEmbarcado.setPadraoEmbarque(PadroesParametroEmbarcado.OPCIONAL_MARCADO.getValor());
		} else if (rdObrigatorioEmbarque.isChecked()) {
			parametroEmbarcado.setPadraoEmbarque(PadroesParametroEmbarcado.OBRIGATORIO.getValor());
		} else if (rdRemovidoEmbarque.isChecked()) {
			parametroEmbarcado.setPadraoEmbarque(PadroesParametroEmbarcado.REMOVIDO.getValor());
		}

		parametroEmbarcadoService.atualizar(parametroEmbarcado);
		parametroService.atualizar(parametro);
		Messagebox.show("Informações Atualizadas!", "Parametros Embarcada", Messagebox.OK, Messagebox.INFORMATION);
	}

	@Listen("onClick = #btnFechar")
	public void onClick$btnFechar(Event ev) {
		winEditarParametrosEmbarcada.detach();
	}

}
