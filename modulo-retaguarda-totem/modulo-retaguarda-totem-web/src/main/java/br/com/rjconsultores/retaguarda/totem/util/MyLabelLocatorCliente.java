///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package br.com.rjconsultores.retaguarda.util;
//
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.Locale;
//
//import org.jboss.logging.Logger;
//
///**
// * 
// * @author gleimar
// */
//public class MyLabelLocatorCliente implements org.zkoss.util.resource.LabelLocator {
//
//	private static Logger log = Logger.getLogger(MyLabelLocatorCliente.class);
//
//	private static final String pathTraduccionCliente = "/com/rjconsultores/ventaboletos/web/cliente/traduccion/";
//
//
//	public URL locate(Locale locale) {
//		URL resource = null;
//		try {
//
//			resource = new ClassPathResource(pathTraduccionCliente + getI3LabelName(locale)).getURL();
//
//			if (resource == null) {
//				resource = new ClassPathResource(pathTraduccionCliente + getI3LabelDefault()).getURL();
//			}
//
//		} catch (IOException ex) {
//			log.error("Error al cargar locale:" + locale);
//			try {
//				resource = new ClassPathResource(pathTraduccionCliente + getI3LabelDefault()).getURL();
//				
//			} catch (MalformedURLException ex1) {
//				log.error("Error al cargar la traduccion estandard.", ex1);
//			} catch (IOException e) {
//				log.error("No fue posible cargar el archivo de traduccion del cliente");
//			}
//		}
//
//		return resource;
//
//	}
//
//	private static final String getI3LabelName(Locale locale) {
//		return "i3-label_" + locale + ".label";
//	}
//
//	private static final String getI3LabelDefault() {
//		return "i3-label_pt_BR.label";
//	}
//}
