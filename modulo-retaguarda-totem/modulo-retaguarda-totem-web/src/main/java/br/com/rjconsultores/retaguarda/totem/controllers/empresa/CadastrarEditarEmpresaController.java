package br.com.rjconsultores.retaguarda.totem.controllers.empresa;

import java.awt.Component;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;

public class CadastrarEditarEmpresaController extends MyGenericSelectorComposer<Component> {
	
	@Wire
	private Textbox txtCodigo;
	@Wire
	private Textbox txtNome;
	private Empresa empresa;
	private boolean novo;
	@WireVariable("empresaService")
	private EmpresaService empresaService;
	@Wire
	private Window winCadastrarEditarEmpresa;
	
	@Override
	public void doAfterCompose(org.zkoss.zk.ui.Component comp) throws Exception {
		super.doAfterCompose(comp);
		
		this.empresa = (Empresa) Executions.getCurrent().getArg().get("empresa");
		this.novo = (Boolean) Executions.getCurrent().getArg().get("novo");

		if (!novo) {
			txtCodigo.setText(empresa.getCodigoEmpresa());
			txtNome.setText(empresa.getNome());
		} else {
			this.empresa = new Empresa();
		}

	}

	@Listen("onClick = #btnSalvar")
	public void onClick$btnSalvar(Event ev) {

		this.empresa.setCodigoEmpresa(empresa.getCodigoEmpresa());
		this.empresa.setNome(this.txtNome.getText());

		if (novo) {
			Messagebox.show("Deseja cadastrar uma nova Empresa?", "Excluir", Messagebox.YES | Messagebox.NO,
					Messagebox.QUESTION, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							if (((int) event.getData()) == Messagebox.YES) {
								empresaService.salvar(empresa);
								Messagebox.show("Nova Empresa Cadastrada!",
										"Empresa Cadastrada com Sucesso!",
										Messagebox.OK, Messagebox.INFORMATION);
								winCadastrarEditarEmpresa.detach();
							}
						}
					});

		} else {
			Messagebox.show("Deseja Editar os dados da Empresa?", "Editar", Messagebox.YES | Messagebox.NO,
					Messagebox.QUESTION, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							if (((int) event.getData()) == Messagebox.YES) {
								empresaService.atualizar(empresa);
								Messagebox.show("Empresa Editada!",
										"Dados editados com sucesso!",
										Messagebox.OK, Messagebox.INFORMATION);
								winCadastrarEditarEmpresa.detach();
							}
						}
					});
		}
	}

}
