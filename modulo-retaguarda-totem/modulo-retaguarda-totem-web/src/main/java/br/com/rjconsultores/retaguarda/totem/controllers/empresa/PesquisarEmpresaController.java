package br.com.rjconsultores.retaguarda.totem.controllers.empresa;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.HibernateSearchObject;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.PagedListWrapper;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderEmpresa;

public class PesquisarEmpresaController extends MyGenericSelectorComposer<Component> {

	@WireVariable("plwLista")
	private transient PagedListWrapper<Empresa> plwLista;
	@WireVariable("empresaService")
	private EmpresaService empresaService;
	@Wire
	private MyListbox<Empresa> empresaList;
	@Wire
	private Textbox txtNome;
	@Wire
	private Paging pagingEmpresa;
	private static final long serialVersionUID = 1L;

	@Override
	public void doAfterCompose(org.zkoss.zk.ui.Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		adiconaListeners();
		atualizarLista();
	}

	private void adiconaListeners() {
		empresaList.setItemRenderer(new RenderEmpresa());
		empresaList.addEventListener("onClick", new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				verEmpresa(empresaList.getSelected());

			}

		});

	}

	private void verEmpresa(Empresa empresa) {
		Map<String, Object> args = new HashMap<String, Object>();
		Boolean novo = (empresa == null);
		args.put("empresa", empresa);
		args.put("novo", novo);

		if (novo) {
			openWindow("/gui/manutencao/empresa/cadastrarEditarEmpresa.zul",
					Labels.getLabel("cadastrarEditarempresaController.title.cadastrar"), args, MODAL, true);

		} else {
			openWindow("/gui//manutencao/empresa/cadastrarEditarEmpresa.zul",
					Labels.getLabel("cadastrarEditarempresaController.title.editar"), args, MODAL, true);
		}

	}

	private void atualizarLista() {
		HibernateSearchObject<Empresa> lsEmpresa = new HibernateSearchObject<Empresa>(Empresa.class,
				pagingEmpresa.getPageSize());

		if (!txtNome.getText().isEmpty()) {
			lsEmpresa.addFilterLike("codigoEmpresa", "%" + txtNome.getValue().trim().concat("%"));
		}

		lsEmpresa.addSortAsc("codigoEmpresa");

		plwLista.init(lsEmpresa, empresaList, pagingEmpresa);

		// if (operadorList.getDataList().size() == 0) {
		// Messagebox.show("Nenhum Operador Cadastrado!", "Pesquisar Operador",
		// Messagebox.OK,
		// Messagebox.INFORMATION);
		// }

	}

	@Listen("onClick=#btnNovo")
	public void onClick$btnNovo(Event ev) {
		verEmpresa(null);
	}

	@Listen("onClick=#btnPesquisa")
	public void onClick$btnPesquisa(Event ev) {
		if (!txtNome.getText().isEmpty()) {
			atualizarLista();
		}
	}

	@Listen("onClick=#bntRefresh")
	public void onClick$btnRefresh(Event ev) {
		txtNome.setText("");
		atualizarLista();
	}

}
