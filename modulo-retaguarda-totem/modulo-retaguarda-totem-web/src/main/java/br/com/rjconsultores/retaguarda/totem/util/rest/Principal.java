package br.com.rjconsultores.retaguarda.totem.util.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Principal {

	public static void main(String[] args) {

		try {

			Connection conn;
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/embarcada_retaguarda?user=root&password=root");
			Statement stm = conn.createStatement();

			StringBuilder sb = new StringBuilder("SELECT embarcada_retaguarda.venda_fechada.id_dispositivo, ");
			sb.append("embarcada_retaguarda.venda_fechada.venda_id, ");
			sb.append("embarcada_retaguarda.venda_fechada.numero_bilhete, ");
			sb.append("embarcada_retaguarda.venda_fechada.data_abertura_viagem, ");
			sb.append("embarcada_retaguarda.venda_fechada.data_abertura_venda, ");
			sb.append("embarcada_retaguarda.venda_fechada.data_abertura_caixa, ");
			sb.append("embarcada_retaguarda.venda_fechada.hora_abertura_venda, ");
			sb.append("embarcada_retaguarda.venda_fechada.hora_abertura_caixa, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_sessao, ");
			sb.append("embarcada_retaguarda.venda_fechada.valor_total_bilhete, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_venda, ");
			sb.append("embarcada_retaguarda.venda_fechada.tipo_venda, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_veiculo, ");
			sb.append("embarcada_retaguarda.venda_fechada.hora_fechamento, ");
			sb.append("embarcada_retaguarda.venda_fechada.data_fechamento, ");
			sb.append("embarcada_retaguarda.venda_fechada.valor_taxa_embarque, ");
			sb.append("embarcada_retaguarda.venda_fechada.valor_seguro, ");
			sb.append("embarcada_retaguarda.venda_fechada.venda_cancelada, ");
			sb.append("embarcada_retaguarda.venda_fechada.valor_pedagio, ");
			sb.append("embarcada_retaguarda.venda_fechada.valor_outros, ");
			sb.append("embarcada_retaguarda.venda_fechada.forma_pagamento, ");
			sb.append("embarcada_retaguarda.venda_fechada.operador_id, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_linha, ");
			sb.append("embarcada_retaguarda.venda_fechada.origem_id, ");
			sb.append("embarcada_retaguarda.venda_fechada.destino_id, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_empresa, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_classe, ");
			sb.append("embarcada_retaguarda.venda_fechada.pda_id, ");
			sb.append("embarcada_retaguarda.venda_fechada.codigo_tipo_pagamento, ");
			sb.append("embarcada_retaguarda.venda_fechada.documento, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_codigo_horario, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_codigo_linha, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_codigo_empresa, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_origem_horario, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_destino_horario, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_hora_horario, ");
			sb.append("embarcada_retaguarda.venda_fechada.horario_sentido_horario, ");
			sb.append("embarcada_retaguarda.venda_fechada.retorno_total_bus, ");
			sb.append("embarcada_retaguarda.venda_fechada.viagem_fechada_id_dispositivo, ");
			sb.append(" embarcada_retaguarda.venda_fechada.viagem_fechada_id_viagem, ");
			sb.append(" embarcada_retaguarda.operador.operador_name ");
			sb.append("FROM embarcada_retaguarda.venda_fechada ");
			sb.append(
					" inner join embarcada_retaguarda.operador on embarcada_retaguarda.venda_fechada.operador_id = embarcada_retaguarda.operador.operador_id ");

			ResultSet rs = stm.executeQuery(sb.toString());

			JasperDesign caminho = JRXmlLoader.load(
					"C:\\Ambiente\\projetos\\retaguarda_santacruz\\comporte_bch\\modulo-retaguarda\\modulo-retaguarda-web\\src\\main\\webapp\\gui\\relatorios\\vendas\\relatorioVendasPorOperador.jrxml");
			JasperReport relatorio = JasperCompileManager.compileReport(caminho);

			HashMap[] parametros = new HashMap[8];

			int i = 0;

			while (rs.next()) {
				HashMap<Object, Object> hm = new HashMap<Object, Object>();
				hm.put("hora_abertura_venda", rs.getTimestamp("hora_abertura_venda"));
				hm.put("codigo_linha", rs.getInt("codigo_linha"));
				hm.put("origem_id", rs.getInt("origem_id"));
				hm.put("destino_id", rs.getInt("destino_id"));
				hm.put("horario_codigo_horario", rs.getInt("horario_codigo_horario"));
				hm.put("horario_hora_horario", rs.getString("horario_hora_horario"));
				hm.put("horario_sentido_horario", rs.getString("horario_sentido_horario"));
				hm.put("valor_total_bilhete", rs.getBigDecimal("valor_total_bilhete"));
				hm.put("operador_id", rs.getInt("operador_id"));
				hm.put("operador_name", rs.getString("operador_name"));
				parametros[i] = hm;
				// Integer id = rs.getInt("daruma_id");
				// String bilhete = rs.getString("bilhete");
				// String nome_origem = rs.getString("nome_origem");
				// String nome_destino = rs.getString("nome_destino");
				// BigDecimal valor_tarifa = rs.getBigDecimal("valor_tarifa");
				i++;
				// System.out.println(id + " " + bilhete + " " + nome_origem + "
				// " + nome_destino + " " + valor_tarifa);
			}

			JRMapArrayDataSource ds = new JRMapArrayDataSource(parametros);
			JRDataSource dados = (JRDataSource) ds;

			// parametros.put("nota", new Double(10));
			JasperPrint impressao = JasperFillManager.fillReport(relatorio, null, dados);

			// exibe o resultado
			JasperViewer viewer = new JasperViewer(impressao, true);
			viewer.setVisible(true);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
