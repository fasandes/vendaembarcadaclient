/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import org.zkoss.zul.Paging;

/**
 *
 * @author gleimar
 */
@SuppressWarnings("serial")
public class MyPaging extends Paging {

	public static final int PAGE_SIZE_DEFAULT = 15;

	public MyPaging(int totalsz, int pagesz) {
		super(totalsz, PAGE_SIZE_DEFAULT);
	}

	public MyPaging() {
		this.setPageSize(PAGE_SIZE_DEFAULT);
	}
}
