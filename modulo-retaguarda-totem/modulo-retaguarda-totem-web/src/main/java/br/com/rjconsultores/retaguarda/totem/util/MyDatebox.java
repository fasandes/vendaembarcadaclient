/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Datebox;

/**
 *
 * @author Administrador
 */
@SuppressWarnings("serial")
public class MyDatebox extends Datebox {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MyDatebox() {
		setLenient(Boolean.FALSE);

		this.addEventListener(Events.ON_CHANGING, new EventListener() {

			String vazia = " ";
			char[] charVazio = vazia.toCharArray();
			char c = charVazio[0];

			@Override
			public void onEvent(Event event) throws Exception {
				// tentativa de atualizar o Datebox e retornar o caracter que
				// acabou de se digitado:
				MyDatebox.this.getValue();

				String digitado = MyDatebox.this.getText();
				Boolean temLetraDigitado = digitado.matches("^[a-zA-Z]+$");

				if (temLetraDigitado) {
					digitado = digitado.replace(digitado.charAt(digitado.length() - 1), c);
					MyDatebox.this.setText(digitado);
				}
			}
		});
	}
}
