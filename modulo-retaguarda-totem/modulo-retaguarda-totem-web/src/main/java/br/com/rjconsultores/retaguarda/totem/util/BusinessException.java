/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

/**
 *
 * @author Rafael
 */
public class BusinessException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 * @param message
	 *            - Chave do arquivo de tradução
	 */
	public BusinessException(String message) {
		super(message);
	}

	/**
	 *
	 * @param message-
	 *            Chave do arquivo de tradução
	 * @param oMsg
	 *            - Parametros do
	 */
	public BusinessException(String message, Object oMsg) {
		super("error");
	}

	public BusinessException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
