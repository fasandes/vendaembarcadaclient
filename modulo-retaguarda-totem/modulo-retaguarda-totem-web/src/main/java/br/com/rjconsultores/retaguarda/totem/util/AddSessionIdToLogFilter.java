package br.com.rjconsultores.retaguarda.totem.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.MDC;

/**
 * Servlet Filter implementation class AddSessionIdToLogFilter
 */
public class AddSessionIdToLogFilter implements Filter {
	private final String SESSION_ID = "SessionID";

	/**
	 * Default constructor.
	 */
	public AddSessionIdToLogFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {

		final boolean addSession = addSessionId(request);

		try {
			chain.doFilter(request, response);
		} finally {
			if (addSession) {
				clearSessionId();
			}
		}

	}

	private void clearSessionId() {
		MDC.remove(SESSION_ID);
	}

	private boolean addSessionId(final ServletRequest request) {
		boolean addSession = false;

		if (request instanceof HttpServletRequest) {
			final HttpServletRequest httpRequest = (HttpServletRequest) request;
			final HttpSession session = httpRequest.getSession(false);
			if (session != null) {
				final String sessionID = session.getId();

				MDC.put(SESSION_ID, sessionID);

				addSession = true;
			}
		}

		return addSession;
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(final FilterConfig fConfig) throws ServletException {
	}

}
