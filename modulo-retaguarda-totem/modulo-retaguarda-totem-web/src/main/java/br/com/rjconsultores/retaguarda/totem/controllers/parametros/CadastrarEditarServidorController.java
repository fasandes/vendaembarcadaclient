package br.com.rjconsultores.retaguarda.totem.controllers.parametros;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;
import br.com.rjconsultores.retaguarda.totem.enuns.Parametros;
import br.com.rjconsultores.retaguarda.totem.service.ParametroService;
import br.com.rjconsultores.retaguarda.totem.util.as400.As400Service;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
// public class CadastrarEditarServidorController extends
// MyGenericForwardComposer {
public class CadastrarEditarServidorController extends SelectorComposer<Component> {

	@WireVariable("parametroService")
	private ParametroService parametroService;
	@WireVariable("as400Service")
	private As400Service as400Service;
	@Wire
	private Textbox txtServidor;
	@Wire
	private Textbox txtLib;
	@Wire
	private Textbox txtTblPreco;
	@Wire
	private Textbox txtAgencia;
	@Wire
	private Textbox txtUsuarioSRVP;
	@Wire
	private Textbox txtSenhaSRVP;

	private static final long serialVersionUID = 1L;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		Parametro parametroLib = parametroService.buscarPorId(Parametros.LIB_SRVP.getChave());
		Parametro parametroIp = parametroService.buscarPorId(Parametros.IP_SRVP.getChave());
		Parametro parametroTbl = parametroService.buscarPorId(Parametros.PRECO_SRVP.getChave());
		Parametro parametroAg = parametroService.buscarPorId(Parametros.AGENCIA_SRVP.getChave());
		Parametro parametroUsuarioSRVP = parametroService.buscarPorId(Parametros.USUARIO_SRVP.getChave());
		Parametro parametroSenhaSRVP = parametroService.buscarPorId(Parametros.SENHA_SRVP.getChave());

		if (parametroIp != null) {
			txtServidor.setText(parametroIp.getValor());
		}

		if (parametroLib != null) {
			txtLib.setText(parametroLib.getValor());
		}

		if (parametroTbl != null) {
			txtTblPreco.setText(parametroTbl.getValor());
		}

		if (parametroAg != null) {
			txtAgencia.setText(parametroAg.getValor());
		}

		if (parametroUsuarioSRVP != null) {
			txtUsuarioSRVP.setText(parametroUsuarioSRVP.getValor());
		}

		if (parametroSenhaSRVP != null) {
			txtSenhaSRVP.setText(parametroSenhaSRVP.getValor());
		}

	}

	@Listen("onClick = #btnSalvar")
	public void onClick$btnSalvar(Event ev) {

		String ip = txtServidor.getText();
		String lib = txtLib.getText();
		String tbl = txtTblPreco.getText();
		String ag = txtAgencia.getText();
		String usuarioSRVP = txtUsuarioSRVP.getText();
		String senhaSRVP = txtSenhaSRVP.getText();

		Parametro parametroLib = new Parametro();
		parametroLib.setChave(Parametros.LIB_SRVP.getChave());
		parametroLib.setValor(lib);

		Parametro parametroIp = new Parametro();
		parametroIp.setChave(Parametros.IP_SRVP.getChave());
		parametroIp.setValor(ip);

		Parametro parametroTbl = new Parametro();
		parametroTbl.setChave(Parametros.PRECO_SRVP.getChave());
		parametroTbl.setValor(tbl);

		Parametro parametroAg = new Parametro();
		parametroAg.setChave(Parametros.AGENCIA_SRVP.getChave());
		parametroAg.setValor(ag);

		Parametro parametroUsuarioSRVP = new Parametro();
		parametroUsuarioSRVP.setChave(Parametros.USUARIO_SRVP.getChave());
		parametroUsuarioSRVP.setValor(usuarioSRVP);

		Parametro parametroSenhaSRVP = new Parametro();
		parametroSenhaSRVP.setChave(Parametros.SENHA_SRVP.getChave());
		parametroSenhaSRVP.setValor(senhaSRVP);

		parametroService.savarOuAtualizar(parametroLib);
		parametroService.savarOuAtualizar(parametroIp);
		parametroService.savarOuAtualizar(parametroTbl);
		parametroService.savarOuAtualizar(parametroAg);
		parametroService.savarOuAtualizar(parametroUsuarioSRVP);
		parametroService.savarOuAtualizar(parametroSenhaSRVP);

		// origem não usuário
		if (ev != null) {

			Messagebox.show("Configurações salvas!", "Configurações Servidor", Messagebox.OK, Messagebox.INFORMATION);
		}

	}

	@Listen("onClick = #btnSincronizar")
	public void onClick$btnSincronizar(Event ev) {

		this.onClick$btnSalvar(null);

		if (!as400Service.conexaoValida(txtServidor.getValue(), txtLib.getValue())) {
			Messagebox.show("Erro ao tentar a conexão. Verifique a configuração!",
					Labels.getLabel("sincroniaController.win.winSincronia"), Messagebox.OK, Messagebox.ERROR);
		} else {
			as400Service.executaOperacao(txtServidor.getValue(), txtLib.getValue(), txtTblPreco.getValue());
			Messagebox.show("Dados Sincronizados!", Labels.getLabel("sincroniaController.win.winSincronia"),
					Messagebox.OK, Messagebox.INFORMATION);
		}

	}

}
