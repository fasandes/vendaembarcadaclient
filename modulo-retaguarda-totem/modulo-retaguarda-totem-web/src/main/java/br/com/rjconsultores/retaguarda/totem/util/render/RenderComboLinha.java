package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Linha;


public class RenderComboLinha implements ComboitemRenderer<Linha>{

	@Override
	public void render(Comboitem item, Linha data, int index) throws Exception {
		
		item.setValue(data);
		item.setLabel(data.getCodigoLinha()+" - "+data.getDescricao());
		
	}

}
