package br.com.rjconsultores.retaguarda.totem.util.as400;

public class Linha {
	
	private Integer codLinha;
	private String descLinha;
	
	public Linha() {
	
	}

	public Integer getCodLinha() {
		return codLinha;
	}

	public void setCodLinha(Integer codLinha) {
		this.codLinha = codLinha;
	}

	public String getDescLinha() {
		return descLinha;
	}

	public void setDescLinha(String descLinha) {
		this.descLinha = descLinha;
	}
	
	

}
