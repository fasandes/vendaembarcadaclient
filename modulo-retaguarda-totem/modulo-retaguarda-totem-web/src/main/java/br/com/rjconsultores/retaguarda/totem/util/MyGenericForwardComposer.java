/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import java.util.Map;

import org.jboss.logging.Logger;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Window;
/**
 * 
 * @author gleimar
 */
@SuppressWarnings("rawtypes")
public class MyGenericForwardComposer extends GenericForwardComposer<Component> {
	

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(MyGenericForwardComposer.class);

	public static int OVERLAPPED = PantallaUtileria.OVERLAPPED;
	public static int MODAL = PantallaUtileria.MODAL;

	public void openWindow(String component, String title, Map args) {
		PantallaUtileria.openWindow(component, title, args, OVERLAPPED, desktop);
	}

	public void openWindowPosi(String component, String title, Map args, int type, String pos) {
		PantallaUtileria.openWindowPosi(component, title, args, type, pos, desktop);
	}

	public void openWindow(String component, String title, Map args, int type, Boolean closabled) {
		PantallaUtileria.openWindow(component, title, args, type, closabled, desktop);
	}

	public void openWindow(String component, String title, Map args, int type) {
		PantallaUtileria.openWindow(component, title, args, type, desktop);
	}

	public void openWindowPosi(String component, String title, Map args, String pos) {
		PantallaUtileria.openWindowPosi(component, title, args, OVERLAPPED, pos, desktop);
	}

	public void closeWindow() {
		if (this.self instanceof Window) {
			((Window) this.self).detach();
		}
	}

	@Override
	public boolean doCatch(Throwable ex) throws Exception {
		
		if (ex instanceof WrongValueException){
			throw (WrongValueException)ex;
		}
		
		log.error("Erro ao abrir a janela", ex);
		
		alert(Labels.getLabel("MSG.Error.Open.Windows"));

		this.closeWindow();

		return true;
	}

}
