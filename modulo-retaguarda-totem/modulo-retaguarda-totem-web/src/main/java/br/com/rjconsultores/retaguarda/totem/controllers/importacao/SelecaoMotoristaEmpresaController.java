package br.com.rjconsultores.retaguarda.totem.controllers.importacao;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.util.MyCombobox;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderComboEmpresa;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderComboOperador;

public class SelecaoMotoristaEmpresaController extends MyGenericSelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@WireVariable
	private EmpresaService empresaService;
	@WireVariable
	private OperadorService operadorService;
	@Wire
	private MyCombobox cboEmpresa;
	@Wire
	private MyCombobox cboMotorista;
	@Wire
	private Window selecaoMotoristaEmpresa;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		cboEmpresa.setItemRenderer(new RenderComboEmpresa());
		cboEmpresa.setModel(new ListModelList<Empresa>(empresaService.buscarTodos()));
		cboEmpresa.getItemCount();

		cboMotorista.setItemRenderer(new RenderComboOperador());
		cboMotorista.setModel(new ListModelList<Operador>(operadorService.buscarTodos()));
		cboMotorista.getItemCount();

	}

	@Listen("onClick=#btnAvancar")
	public void onClick$btnAvancar(Event ev) {

		if (!(cboEmpresa.getSelectedItem() != null || cboMotorista.getSelectedItem() != null)) {
			Messagebox.show("Favor Selecionar o Motorista/Empresa do responsável!!!", "Seleção Motorista/Empresa",
					Messagebox.OK, Messagebox.EXCLAMATION);
		} else {
			Empresa empresa = cboEmpresa.getSelectedItem().getValue();
			Operador operador = cboMotorista.getSelectedItem().getValue();
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("empresa", empresa);
			args.put("operador", operador);
			selecaoMotoristaEmpresa.detach();
			openWindow("/gui/importacao/daruma/arquivosDaruma.zul", "Importação Daruma", args, MODAL, Boolean.TRUE);
		}

	}

}
