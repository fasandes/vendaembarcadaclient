package br.com.rjconsultores.authentication;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;

public class AuthenticationInit implements Initiator {

	@Override
	public void doInit(Page arg0, Map<String, Object> arg1) throws Exception {
		// Executions.sendRedirect("/login.zul");
		AuthenticationServiceSession authService = new AuthenticationServiceSession();
		Usuario usuario = authService.getUserCredential();
		if (usuario == null) {
			Execution exec = Executions.getCurrent();
			HttpServletResponse response = (HttpServletResponse) exec.getNativeResponse();
			response.sendRedirect(response.encodeRedirectURL("/modulo-retaguarda-web/login.zul")); // assume
																									// there
																									// is
																									// /login
			// exec.setVoided(true); //no need to create UI since redirect will
			// take place
			return;
		}
	}

}
