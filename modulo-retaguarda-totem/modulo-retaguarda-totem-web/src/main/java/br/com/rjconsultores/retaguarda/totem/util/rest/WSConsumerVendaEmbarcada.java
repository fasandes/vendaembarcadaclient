package br.com.rjconsultores.retaguarda.totem.util.rest;

import javax.inject.Named;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaEmbarcadaCliVO;

@Named("wsConsumerVendaEmbarcada")
public class WSConsumerVendaEmbarcada {

	public int gerarVendaTotalBus(VendaEmbarcadaCliVO vendaDaruma, Parametro enderecoADM) {

		Client client = Client.create();
		String retorno[];

		WebResource webResource = client.resource(enderecoADM.getValor() + vendaDaruma.toString());
		// "http://54.173.147.150:8480/ventaboletosadm/rest/vendaEmbarcada/gerarVenda/"
		// + vendaDaruma.toString());
		// "http://54.208.79.11:8080/ventaboletosadm/rest/vendaEmbarcada/gerarVenda/"
		// + vendaDaruma.toString());

		// WebResource webResource =
		// client.resource("http://54.173.147.150:8480/ventaboletosadm/rest/vendaEmbarcada/gerarVenda/VE_00/USUARIO_COMUM/7459/5509/300844/24_02_2016__15_50_00/24_02_2016__15_50_00/7.05/3/569/3/1346/6683/3/0/0/0/0.00/275/1/1");

		ClientResponse response = webResource.type("application/json").post(ClientResponse.class,
				vendaDaruma.toString());

		if (response.getStatus() != 200) {
			System.out.println(response.getStatus());
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
		}

		System.out.println("Output from Server .... \n");

		retorno = response.getEntity(String.class).replace("}", "").split(":");

		return Integer.parseInt(retorno[1].replace("'", ""));
	}

}
