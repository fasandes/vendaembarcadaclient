package br.com.rjconsultores.retaguarda.totem.util.rest;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

public class ServiceEndpoint {
	/**
	 * @author vpaiva
	 * @param asyncResponse
	 *            13:28:14 22/07/2015 2015
	 */
	protected void trataTimeOut(final AsyncResponse asyncResponse) {
		asyncResponse.setTimeoutHandler((hdl) -> {
			asyncResponse
					.resume(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Operation time out.").build());

		});
		asyncResponse.setTimeout(30, TimeUnit.SECONDS);
	}
}
