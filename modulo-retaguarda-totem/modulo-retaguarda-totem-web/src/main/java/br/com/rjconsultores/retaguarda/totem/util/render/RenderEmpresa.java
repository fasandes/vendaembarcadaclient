package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;

public class RenderEmpresa implements ListitemRenderer<Empresa> {

	@Override
	public void render(Listitem item, Empresa empresa, int index) throws Exception {
		Listcell lc = new Listcell(empresa.getCodigoEmpresa().toString());
		lc.setParent(item);

		lc = new Listcell(empresa.getNome());
		lc.setParent(item);

		lc.setValue(empresa);

	}

}
