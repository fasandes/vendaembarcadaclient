package br.com.rjconsultores.retaguarda.totem.util.render;

import java.text.SimpleDateFormat;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.rjonsultores.vendedor.vo.Viagem;

public class RenderViagem implements ListitemRenderer<Viagem> {

	@Override
	public void render(Listitem listItem, Viagem viagem, int arg2) throws Exception {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

		Listcell lc = new Listcell(viagem.getIdViagem().toString());
		lc.setParent(listItem);

		lc = new Listcell(dateFormat.format(viagem.getDataViagem()));
		lc.setParent(listItem);

		lc = new Listcell(dateFormat.format(viagem.getDataFechamentoViagem()));
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(viagem.getLinha().getDescLinha()));
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(viagem.getCodServico().toString()));
		lc.setParent(listItem);

		listItem.setValue(viagem);

	}
}
