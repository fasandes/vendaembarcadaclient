package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.rjonsultores.vendedor.vo.Venda;

public class RenderVenda implements ListitemRenderer<Venda> {

	@Override
	public void render(Listitem listItem, Venda venda, int arg2) throws Exception {

		Listcell lc = new Listcell(venda.getNumeroBilhete().toString());
		lc.setParent(listItem);

		lc = new Listcell(venda.getOrigem().getDescLocalidade());
		lc.setParent(listItem);

		lc = new Listcell(venda.getDestino().getDescLocalidade());
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(venda.getValorTotalBilhete()));
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(venda.getLinha().getCodLinha()));
		lc.setParent(listItem);

		listItem.setValue(venda);

	}
}
