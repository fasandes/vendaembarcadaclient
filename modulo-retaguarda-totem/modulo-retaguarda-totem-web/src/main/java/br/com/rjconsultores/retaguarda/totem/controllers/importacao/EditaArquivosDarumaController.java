package br.com.rjconsultores.retaguarda.totem.controllers.importacao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.DarumaImport;
import br.com.rjconsultores.retaguarda.totem.entidades.DetalheHorario;
import br.com.rjconsultores.retaguarda.totem.service.DetalheHorarioService;
import br.com.rjconsultores.retaguarda.totem.service.HorarioService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderComboHora;

public class EditaArquivosDarumaController extends MyGenericSelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());
	@Wire
	private Textbox txtData;
	@Wire
	private Combobox cboHora;
	@Wire
	private Textbox txtCodOrigem;
	@Wire
	private Textbox txtOrigem;
	@Wire
	private Textbox txtCodDestino;
	@Wire
	private Textbox txtDestino;
	@Wire
	private Textbox txtEmissao;
	@Wire
	private Textbox txtValor;
	// @Wire
	// private Textbox txtAliquota;
	// @Wire
	// private Textbox txtValorAliquota;
	@Wire
	private Textbox txtEstado;
	@Wire
	private Textbox txtSeguro;
	@Wire
	private Window editaBilhete;
	@WireVariable
	private DetalheHorarioService detalheHorarioService;
	@WireVariable(value = "serviceLinha")
	private LinhaService linhaService;
	@WireVariable(value = "horarioService")
	private HorarioService horarioService;
	private DarumaImport daruma;
	private MyListbox<DarumaImport> listDarumaImport;
	private List<Integer> lsCodigoLinhaMesmaDescricao;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		daruma = (DarumaImport) Executions.getCurrent().getArg().get("daruma");
		listDarumaImport = (MyListbox<DarumaImport>) Executions.getCurrent().getArg().get("listDarumaImport");
		SimpleDateFormat formatHora = new SimpleDateFormat("HHmm");
		SimpleDateFormat formatHoraSalvar = new SimpleDateFormat("HH:mm");
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");

		if (daruma != null) {
			txtData.setText(formatData.format(daruma.getDtBilhete()));
			cboHora.setItemRenderer(new RenderComboHora());
			lsCodigoLinhaMesmaDescricao = linhaService.buscarCodigoLinhaMesmaDescricao(daruma.getLinha().split(" ")[1]);
			List<DetalheHorario> lsDetalheHorario = detalheHorarioService.listarHorarios(lsCodigoLinhaMesmaDescricao,
					daruma.getCodigo_origem(), daruma.getCodigo_destino());
			if (lsDetalheHorario != null) {
				cboHora.setModel(new ListModelList<DetalheHorario>(lsDetalheHorario));
			}
			cboHora.getItemCount();
			txtCodOrigem.setText(daruma.getCodigo_origem());
			txtOrigem.setText(daruma.getNome_origem());
			txtCodDestino.setText(daruma.getCodigo_destino());
			txtDestino.setText(daruma.getNome_destino());
			txtEmissao.setText(formatHora.format(daruma.getEmissao()));
			txtValor.setText(daruma.getValor_tarifa().toString());
			// txtAliquota.setText(daruma.getAliquota());
			// txtValorAliquota.setText(daruma.getValor_aliquota().toString());
			txtEstado.setText(daruma.getEstado());
			txtSeguro.setText(daruma.getSeguro().toString());
		}
	}

	@Listen("onClick = #btnSalvar")
	public void onClick$btnSalvar() throws WrongValueException, ParseException {
		SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
		SimpleDateFormat formatData = new SimpleDateFormat("dd/MM/yyyy");
		daruma.setDtBilhete(formatData.parse(txtData.getValue()));
		Calendar calendarHora = Calendar.getInstance();
		calendarHora.setTime(daruma.getHora());
		calendarHora.set(Calendar.HOUR_OF_DAY,
				Integer.parseInt(cboHora.getSelectedItem().getValue().toString().substring(0, 2)));
		calendarHora.set(Calendar.MINUTE,
				Integer.parseInt(cboHora.getSelectedItem().getValue().toString().substring(2, 4)));
		daruma.setHora(calendarHora.getTime());
		daruma.setHorario_linha(cboHora.getSelectedItem().getValue().toString());
		daruma.setCodigo_origem(txtCodOrigem.getValue());
		daruma.setNome_origem(txtOrigem.getValue());
		daruma.setCodigo_destino(txtCodDestino.getValue());
		daruma.setNome_destino(txtDestino.getValue());
		Calendar calendarEmissao = Calendar.getInstance();
		calendarEmissao.setTime(daruma.getEmissao());
		calendarEmissao.set(Calendar.HOUR_OF_DAY, Integer.parseInt(txtEmissao.getValue().substring(0, 2)));
		calendarEmissao.set(Calendar.MINUTE, Integer.parseInt(txtEmissao.getValue().substring(2, 4)));
		daruma.setEmissao(calendarEmissao.getTime());
		BigDecimal valor = new BigDecimal(txtValor.getValue());
		daruma.setValor_tarifa(valor.setScale(2, RoundingMode.HALF_EVEN));
		// daruma.setAliquota(txtAliquota.getValue());
		// BigDecimal valor_aliquota = new
		// BigDecimal(txtValorAliquota.getValue());
		// daruma.setValor_aliquota(valor_aliquota.setScale(2,
		// RoundingMode.HALF_EVEN));
		daruma.setEstado(txtEstado.getValue());
		BigDecimal seguro = new BigDecimal(txtSeguro.getValue());
		daruma.setSeguro(seguro.setScale(2, RoundingMode.HALF_EVEN));
		daruma.setLinha(
				horarioService.buscarLinhaPorHoraHorario(daruma.getHorario_linha(), lsCodigoLinhaMesmaDescricao));

		listDarumaImport.updateItem(daruma);
		Messagebox.show("Dados Alterados com Sucesso!", "Alteração", Messagebox.OK, Messagebox.INFORMATION);
		editaBilhete.detach();

	}

}