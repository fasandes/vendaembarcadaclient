package br.com.rjconsultores.retaguarda.totem.controllers.operador;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.Horario;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.entidades.OperadorSuperLinha;
import br.com.rjconsultores.retaguarda.totem.entidades.OperadorSuperLinhaPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Operadorlinha;
import br.com.rjconsultores.retaguarda.totem.entidades.OperadorlinhaPK;
import br.com.rjconsultores.retaguarda.totem.entidades.SuperLinha;
import br.com.rjconsultores.retaguarda.totem.service.HorarioService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorLinhaService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorSuperLinhaService;
import br.com.rjconsultores.retaguarda.totem.service.SuperLinhaService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.HibernateSearchObject;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.PagedListWrapper;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderHorario;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderLinha;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderOperador;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderSuperLinha;

public class SelecaoOperadorController extends MyGenericSelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Logger log = Logger.getLogger(this.getClass());

	@WireVariable("plwLista")
	private transient PagedListWrapper<Operador> plwListaOperador;

	@WireVariable("plwLista")
	private transient PagedListWrapper<Linha> plwListaLinha;

	@WireVariable("plwLista")
	private transient PagedListWrapper<SuperLinha> plwListaSuperLinha;

	@WireVariable("plwLista")
	private transient PagedListWrapper<Horario> plwListaHorario;

	@WireVariable("operadorService")
	private OperadorService serviceOperador;

	@WireVariable("superLinhaService")
	private SuperLinhaService serviceSuperLinha;

	@WireVariable("serviceLinha")
	private LinhaService serviceLinha;

	@WireVariable("horarioService")
	private HorarioService serviceHorario;

	@WireVariable("operadorLinhaService")
	private OperadorLinhaService serviceOperadorLinha;

	@WireVariable("operadorSuperLinhaService")
	private OperadorSuperLinhaService serviceOperadorSuperLinha;

	private Operadorlinha operadorLinha;
	private OperadorlinhaPK operadorLinhaPK;

	private OperadorSuperLinha operadorSuperLinha;
	private OperadorSuperLinhaPK operadorSuperLinhaPK;

	@Wire
	private Paging pagingLinha;

	@Wire
	private Paging pagingSuperLinha;

	@Wire
	private Paging pagingHorario;

	@Wire
	private MyListbox<Linha> linhaList;

	@Wire
	private MyListbox<SuperLinha> superLinhaList;

	@Wire
	private List<Linha> linhasSelecionadas;

	@Wire
	private List<Linha> linhasNaoSelecionadas;

	@Wire
	private List<SuperLinha> superLinhasSelecionadas;

	@Wire
	private List<SuperLinha> superLinhasNaoSelecionadas;

	@Wire
	private MyListbox<Horario> horarioList;
	// @Wire
	// private ListModelList<Dispositivo> modelDispositivo;
	@Wire
	private ListModelList<Linha> modelLinha;

	@Wire
	private ListModelList<SuperLinha> modelSuperLinha;

	@Wire
	private ListModelList<Operador> modelOperador;

	@Wire
	private ListModelList<Linha> model;

	@Wire
	private Textbox txtDescricaoLinha;

	@Wire
	private Textbox txtDescricaoSuperLinha;

	@Wire
	private ListModelList<Horario> modelHorario;

	@Wire
	private List<Operador> operadorSelecionados;

	@Wire
	private List<Operador> operadorNaoSelecionadas;

	@Wire
	private List<Horario> horariosSelecionadas;

	@Wire
	private List<Horario> horariosNaoSelecionadas;

	@Wire
	private Textbox txtNome;

	@Wire
	private Paging pagingOperador;

	@Wire
	private MyListbox<Operador> operadorList;

	@Wire
	private Tabbox tabBox;

	@Wire
	private Window winSelecaoOperador;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		operadorList.setItemRenderer(new RenderOperador());
		operadorList.setMultiple(true);
		operadorSelecionados = new ArrayList<Operador>();
		operadorNaoSelecionadas = serviceOperador.buscarTodosAtivos();
		atualizarLista();
	}

	private void atualizarLista() {
		HibernateSearchObject<Operador> lsOperador = new HibernateSearchObject<Operador>(Operador.class,
				pagingOperador.getPageSize());

		if (!txtNome.getText().isEmpty()) {
			lsOperador.addFilterLike("operadorName", "%" + txtNome.getValue().trim().concat("%"));
		}

		lsOperador.addSortAsc("operadorName");

		plwListaOperador.init(lsOperador, operadorList, pagingOperador);

		List<Operador> lsOperadorAtivo = serviceOperador.buscarTodosAtivos();

		// modelOperador = new ListModelList<Operador>(lsOperadorAtivo);

		operadorList.setData(lsOperadorAtivo);
		operadorList.renderAll();

		// if (operadorList.getDataList().size() == 0) {
		// Messagebox.show("Nenhum Operador Cadastrado!", "Pesquisar Operador",
		// Messagebox.OK,
		// Messagebox.INFORMATION);
		// }

	}

	public void atualizaListaLinha() {
		HibernateSearchObject<Linha> linhas = new HibernateSearchObject<Linha>(Linha.class, pagingLinha.getPageSize());

		if (!txtDescricaoLinha.getValue().isEmpty()) {
			linhas.addFilterLike("descricao", "%" + txtDescricaoLinha.getValue().trim().concat("%"));
		}

		linhas.addSortAsc("codigoLinha");

		plwListaLinha.init(linhas, linhaList, pagingLinha);

		if (linhaList.getDataList().size() == 0) {
			Messagebox.show("Nenhum Registro", "Pesquisar Horário", Messagebox.OK, Messagebox.INFORMATION);
		}

		List<Linha> lsLinhas = new ArrayList<Linha>();

		if (linhasSelecionadas != null && !linhasSelecionadas.isEmpty()) {
			lsLinhas.addAll(linhasSelecionadas);

			model = linhaList.getDataList();
			ListModelList<Linha> novoModel = new ListModelList<Linha>(lsLinhas);
			model.removeAll(novoModel);
			novoModel.addAll(model);
			novoModel.setMultiple(true);

			linhaList.setData(novoModel);
			linhaList.renderAll();
			model = linhaList.getDataList();

			int size = linhasSelecionadas.size();
			for (int i = 0; i < size; i++) {
				if (linhasSelecionadas.contains(novoModel.get(i))) {
					linhaList.getItemAtIndex(i).setSelected(true);
				}
			}
		}

	}

	@Listen("onSelect = #tabLinhas")
	public void onSelect$tabLinhas(Event event) {
		operadorSelecionados = operadorList.getSelectedsItens();
		if (operadorSelecionados.size() == 0) {
			Messagebox.show("Nenhum Operador Selecionado", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(0);
		} else if (operadorSelecionados.size() > 1) {
			Messagebox.show("Vários Operadores Selecionados", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(0);
		} else {
			linhaList.setItemRenderer(new RenderLinha());
			linhaList.setMultiple(true);
			List<Integer> lsCodigoLinhas = serviceOperadorLinha
					.buscarLinhasOperadorLinha(operadorSelecionados.get(0).getOperadorId());
			if (lsCodigoLinhas != null && !lsCodigoLinhas.isEmpty()) {
				linhasSelecionadas = serviceLinha.buscarLinhas(lsCodigoLinhas);
				linhasNaoSelecionadas = serviceLinha.buscarTodos();
				linhasNaoSelecionadas.removeAll(linhasSelecionadas);
			} else {
				linhasNaoSelecionadas = serviceLinha.buscarTodos();
			}
			atualizaListaLinha();
		}
	}

	public void atualizaListaSuperLinha() {
		HibernateSearchObject<SuperLinha> linhas = new HibernateSearchObject<SuperLinha>(SuperLinha.class,
				pagingLinha.getPageSize());

		if (!txtDescricaoSuperLinha.getValue().isEmpty()) {
			linhas.addFilterLike("descricao", "%" + txtDescricaoSuperLinha.getValue().trim().concat("%"));
		}

		linhas.addSortAsc("id");

		plwListaSuperLinha.init(linhas, superLinhaList, pagingSuperLinha);

		if (superLinhaList.getDataList().size() == 0) {
			Messagebox.show("Nenhum Registro", "Pesquisar Horário", Messagebox.OK, Messagebox.INFORMATION);
		}

		List<SuperLinha> lsSuperLinhas = new ArrayList<SuperLinha>();

		if (superLinhasSelecionadas != null && !superLinhasSelecionadas.isEmpty()) {
			lsSuperLinhas.addAll(superLinhasSelecionadas);

			modelSuperLinha = superLinhaList.getDataList();
			ListModelList<SuperLinha> novoModel = new ListModelList<SuperLinha>(lsSuperLinhas);
			modelSuperLinha.removeAll(novoModel);
			novoModel.addAll(modelSuperLinha);
			novoModel.setMultiple(true);

			superLinhaList.setData(novoModel);
			superLinhaList.renderAll();
			modelSuperLinha = superLinhaList.getDataList();

			int size = superLinhasSelecionadas.size();
			for (int i = 0; i < size; i++) {
				if (superLinhasSelecionadas.contains(novoModel.get(i))) {
					superLinhaList.getItemAtIndex(i).setSelected(true);
				}
			}
		}

	}

	@Listen("onSelect = #tabSuperLinha")
	public void onSelect$tabSuperLinha(Event event) {
		operadorSelecionados = operadorList.getSelectedsItens();

		if (operadorSelecionados.size() == 0) {

			Messagebox.show("Nenhum Operador Selecionado", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(0);

		} else if (operadorSelecionados.size() > 1) {

			Messagebox.show("Vários Operadores Selecionados", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(0);

		} else {

			superLinhaList.setItemRenderer(new RenderSuperLinha());
			superLinhaList.setMultiple(true);

			List<Integer> lsCodigoLinhas = serviceOperadorSuperLinha
					.buscarLinhasOperadorLinha(operadorSelecionados.get(0).getOperadorId());

			if (lsCodigoLinhas != null && !lsCodigoLinhas.isEmpty()) {

				superLinhasSelecionadas = serviceSuperLinha.buscarLinhas(lsCodigoLinhas);
				superLinhasNaoSelecionadas = serviceSuperLinha.buscarTodos();
				superLinhasNaoSelecionadas.removeAll(superLinhasSelecionadas);

			} else {

				superLinhasNaoSelecionadas = serviceSuperLinha.buscarTodos();

			}
			atualizaListaSuperLinha();
		}
	}

	@Listen("onSelect = #tabServicos")
	public void onSelect$tabServicos(Event event) {
		linhasSelecionadas = linhaList.getSelectedsItens();
		if (linhasSelecionadas.size() == 0) {
			Messagebox.show("Nenhuma Linha/SuperLinha Selecionada", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(1);
		} else if (linhasSelecionadas.size() > 20) {
			Messagebox.show(
					"Você selecionou mais de 20 linhas. Caso deseje selecionar um número maior de linhas, favor entrar em contato com o suporte.",
					"Seleção", Messagebox.OK, Messagebox.INFORMATION);
			tabBox.setSelectedIndex(1);
		} else {
			List<Integer> lsCodigoHorario = serviceOperadorLinha
					.buscarHorarioOperadorLinha(operadorSelecionados.get(0).getOperadorId(), linhasSelecionadas);
			if (lsCodigoHorario != null && !lsCodigoHorario.isEmpty()) {
				horariosSelecionadas = serviceHorario.buscarHorarios(lsCodigoHorario);
				horariosNaoSelecionadas = serviceHorario.localizarTodosPorLinha(linhasSelecionadas);
				horariosNaoSelecionadas.removeAll(horariosSelecionadas);
				horarioList.setItemRenderer(new RenderHorario());
			} else {
				horariosNaoSelecionadas = serviceHorario.localizarTodosPorLinha(linhasSelecionadas);
				horarioList.setItemRenderer(new RenderHorario());
			}
			atualizaListaHorario();
		}

	}

	private void atualizaListaHorario() {

		HibernateSearchObject<Horario> horarios = new HibernateSearchObject<Horario>(Horario.class,
				pagingLinha.getPageSize());

		if (!txtDescricaoLinha.getValue().isEmpty()) {
			horarios.addFilterLike("descricao", "%" + txtDescricaoLinha.getValue().trim().concat("%"));
		}

		horarios.addSortAsc("horarioPK.codigoHorario");

		plwListaHorario.init(horarios, horarioList, pagingHorario);

		if (horarioList.getDataList().size() == 0) {
			Messagebox.show("Nenhum Registro", "Pesquisar Horário", Messagebox.OK, Messagebox.INFORMATION);
		}

		List<Horario> lsHorarios = serviceHorario.localizarTodosPorLinha(linhasSelecionadas);

		modelHorario = new ListModelList<Horario>(lsHorarios);
		ListModelList<Horario> novoModel = new ListModelList<Horario>(lsHorarios);
		// novoModel.addAll(modelHorario);
		// novoModel.setMultiple(true);

		horarioList.setData(lsHorarios);
		horarioList.renderAll();
		// modelHorario = horarioList.getDataList();

		if (horariosSelecionadas != null && !horariosSelecionadas.isEmpty()) {
			int size = lsHorarios.size();
			for (int i = 0; i < size; i++) {
				if (horariosSelecionadas.contains(novoModel.get(i))) {
					horarioList.getItemAtIndex(i).setSelected(true);
				}
			}
		}

	}

	private void salvaOperadorLinha() {
		serviceOperadorLinha.deletaOperadorLinha(operadorSelecionados.get(0).getOperadorId());

		horariosSelecionadas = horarioList.getSelectedsItens();
		operadorLinha = new Operadorlinha();
		operadorLinhaPK = new OperadorlinhaPK();
		operadorLinhaPK.setOperadorOperadorId(operadorSelecionados.get(0).getOperadorId());
		// operadorLinha.setOperador(operadorSelecionados.get(0));
		for (Horario h : horariosSelecionadas) {
			operadorLinhaPK.setHorarioCodigoHorario(h.getHorarioPK().getCodigoHorario());
			operadorLinhaPK.setHorarioCodigoLinha(h.getHorarioPK().getCodigoLinha());
			operadorLinhaPK.setHorarioCodigoEmpresa(h.getHorarioPK().getCodigoEmpresa());
			operadorLinhaPK.setHorarioOrigemHorario(h.getHorarioPK().getOrigemHorario());// origem
			operadorLinhaPK.setHorarioDestinoHorario(h.getHorarioPK().getDestinoHorario());// destino
			operadorLinhaPK.setHorarioHoraHorario(h.getHorarioPK().getHoraHorario());
			operadorLinhaPK.setHorarioSentidoHorario(h.getHorarioPK().getSentidoHorario());
			operadorLinha.setDataCriacao(new Date());
			operadorLinha.setOperadorlinhaPK(operadorLinhaPK);
			try {
				serviceOperadorLinha.salvar(operadorLinha);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void salvaOperadorSuperLinha() {
		serviceOperadorSuperLinha.deletaOperadorLinha(operadorSelecionados.get(0).getOperadorId());

		superLinhasSelecionadas = superLinhaList.getSelectedsItens();

		// operadorLinha.setOperador(operadorSelecionados.get(0));
		for (SuperLinha superLinha : superLinhasSelecionadas) {
			operadorSuperLinha = new OperadorSuperLinha();
			operadorSuperLinhaPK = new OperadorSuperLinhaPK();
			operadorSuperLinhaPK.setOperadorOperadorId(operadorSelecionados.get(0).getOperadorId());

			operadorSuperLinhaPK.setSuperLinhaId(superLinha.getId());
			operadorSuperLinha.setOperadorSuperLinhaPK(operadorSuperLinhaPK);

			try {
				serviceOperadorSuperLinha.salvar(operadorSuperLinha);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Listen("onClick=#btnConcluir")
	public void onClick$btnConcluir(Event ev) {
		salvaOperadorLinha();
		salvaOperadorSuperLinha();

		Messagebox.show("Operação Concluída!", "Seleção", Messagebox.OK, Messagebox.INFORMATION);
		winSelecaoOperador.detach();
	}

	@Listen("onClick=#btnPesquisa")
	public void onClick$btnPesquisa(Event ev) {
		if (!txtNome.getText().isEmpty()) {
			atualizarLista();
		}
	}

}
