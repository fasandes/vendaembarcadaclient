package br.com.rjconsultores.retaguarda.totem.controllers.importacao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import com.rjconsultores.vendedor.util.XMLUtil;
import com.rjonsultores.vendedor.vo.Caixa;
import com.rjonsultores.vendedor.vo.Venda;
import com.rjonsultores.vendedor.vo.Viagem;
import com.thoughtworks.xstream.XStream;

import br.com.rjconsultores.retaguarda.totem.entidades.CaixaFechado;
import br.com.rjconsultores.retaguarda.totem.entidades.CaixaFechadoPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Classe;
import br.com.rjconsultores.retaguarda.totem.entidades.DarumaImport;
import br.com.rjconsultores.retaguarda.totem.entidades.DetalheHorario;
import br.com.rjconsultores.retaguarda.totem.entidades.DetalheHorarioPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.FormaPagamento;
import br.com.rjconsultores.retaguarda.totem.entidades.Horario;
import br.com.rjconsultores.retaguarda.totem.entidades.HorarioPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;
import br.com.rjconsultores.retaguarda.totem.entidades.Operacao;
import br.com.rjconsultores.retaguarda.totem.entidades.OperacaoPK;
import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaFechada;
import br.com.rjconsultores.retaguarda.totem.entidades.ViagemFechada;
import br.com.rjconsultores.retaguarda.totem.service.CaixaFechadoService;
import br.com.rjconsultores.retaguarda.totem.service.ClasseService;
import br.com.rjconsultores.retaguarda.totem.service.DarumaImportService;
import br.com.rjconsultores.retaguarda.totem.service.DetalheHorarioService;
import br.com.rjconsultores.retaguarda.totem.service.EmpresaService;
import br.com.rjconsultores.retaguarda.totem.service.FormaPagamentoService;
import br.com.rjconsultores.retaguarda.totem.service.HorarioService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.service.LocalidadeService;
import br.com.rjconsultores.retaguarda.totem.service.OperacaoService;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.service.TipoPagamentoService;
import br.com.rjconsultores.retaguarda.totem.service.VendaFechadaService;
import br.com.rjconsultores.retaguarda.totem.service.ViagemFechadaService;
import br.com.rjconsultores.retaguarda.totem.util.BusinessException;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderClasse;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderDetalheHorario;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderEmpresa;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderHorario;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderLinha;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderLocalidade;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderOperacao;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderTipoPagamento;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class ImportacaoController extends SelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());
	@WireVariable("vendaFechadaService")
	private VendaFechadaService vendaFechadaService;
	@WireVariable("caixaFechadoService")
	private CaixaFechadoService caixaFechadoService;
	@WireVariable("viagemFechadaService")
	private ViagemFechadaService viagemFechadaService;
	@WireVariable("empresaService")
	private EmpresaService empresaService;
	@WireVariable("operadorService")
	private OperadorService operadorService;
	@WireVariable("horarioService")
	private HorarioService horarioService;
	@WireVariable("localidadeService")
	private LocalidadeService localidadeService;
	@WireVariable("tipoPagamentoService")
	private TipoPagamentoService tipoPagamentoService;
	@WireVariable("operacaoService")
	private OperacaoService operacaoService;
	@WireVariable("formaPagamentoService")
	private FormaPagamentoService formaPagamentoService;
	@WireVariable("serviceLinha")
	private LinhaService serviceLinha;
	@WireVariable("darumaImportService")
	private DarumaImportService darumaImportService;
	@WireVariable("classeService")
	private ClasseService classeService;
	@WireVariable("detalheHorarioService")
	private DetalheHorarioService detalheHorarioService;
	@Wire
	private MyListbox<Operacao> listOperacao;
	@Wire
	private MyListbox<TipoPagamento> listTipoPagamento;
	@Wire
	private MyListbox<Localidade> listLocalidade;
	@Wire
	private MyListbox<Horario> listHorario;
	@Wire
	private MyListbox<DetalheHorario> listDetalheHorario;
	@Wire
	private MyListbox<Linha> listLinha;
	@Wire
	private MyListbox<Classe> listClasse;
	@Wire
	private MyListbox<Empresa> listEmpresa;
	@Wire
	private MyListbox<DarumaImport> listDarumaImport;
	@Wire
	private Button btnSalvarArquivos;
	@Wire
	private Tab tabClasse;
	@Wire
	private Tab tabEmpresa;
	@Wire
	private Tab tabTrecho;
	@Wire
	private Tab tabLinhas;
	@Wire
	private Tab tabLocalidades;
	@Wire
	private Tab tabTipoVenda;
	@Wire
	private Tab tabHorario;
	@Wire
	private Tab tabDetalheHorario;
	@Wire
	private List<CaixaFechado> lsCaixaFechado;
	@Wire
	private List<ViagemFechada> lsViagemFechada;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		List<Classe> lsClasse = classeService.buscarTodos() != null ? classeService.buscarTodos()
				: new ArrayList<Classe>();
		listClasse.setData(lsClasse);
		tabClasse.setVisible(true);
		tabClasse.setSelected(true);

		listOperacao.setItemRenderer(new RenderOperacao());
		listLocalidade.setItemRenderer(new RenderLocalidade());

		listLinha.setItemRenderer(new RenderLinha());
		listTipoPagamento.setItemRenderer(new RenderTipoPagamento());

		listHorario.setItemRenderer(new RenderHorario());
		listDetalheHorario.setItemRenderer(new RenderDetalheHorario());

		listClasse.setItemRenderer(new RenderClasse());
		listEmpresa.setItemRenderer(new RenderEmpresa());

		lsCaixaFechado = new ArrayList<CaixaFechado>();
		lsViagemFechada = new ArrayList<ViagemFechada>();

	}

	@SuppressWarnings("unchecked")
	@Listen("onUpload=#btnSobeVendas")
	public void onUpload$btnSobeVendas(UploadEvent ev) throws IOException, ClassNotFoundException {

		ObjectInputStream objectInputStream = new ObjectInputStream(ev.getMedia().getStreamData());
		String xml = (String) XMLUtil.deserializaObjetoXML(objectInputStream);
		XStream stream = new XStream();
		stream.alias("venda", Venda.class);
		ArrayList<Venda> lsVendas = (ArrayList<Venda>) stream.fromXML(xml);
		// listOperacao.setData(lsVendas);
		btnSalvarArquivos.setDisabled(false);
		objectInputStream.close();

	}

	@Listen("onSelect = #tabLocalidades")
	public void onSelect$tabLocalidades(Event event) {

		List<Localidade> lsLocalidade = localidadeService.buscarTodas() != null ? localidadeService.buscarTodas()
				: new ArrayList<Localidade>();
		listLocalidade.setData(lsLocalidade);
		tabLocalidades.setVisible(true);
		tabLocalidades.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoLocalidade")
	public void onUpload$btnImportacaoLocalidade(UploadEvent ev) throws IOException, ClassNotFoundException {

		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					Localidade localidade = new Localidade();
					localidade.setLocalidadeId(Integer.parseInt(l[0]));
					localidade.setCidade(l[1].replace("'", ""));
					localidadeService.salvar(localidade);
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}
	}

	@Listen("onSelect = #tabLinhas")
	public void onSelect$tabLinhas(Event event) {
		List<Linha> lsLinha = serviceLinha.buscarTodos() != null ? serviceLinha.buscarTodos() : new ArrayList<Linha>();
		listLinha.setData(lsLinha);
		tabLinhas.setVisible(true);
		tabLinhas.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoLinha")
	public void onUpload$btnImportacaoLinha(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					Linha lin = new Linha();
					lin.setCodigoLinha(Integer.parseInt(l[2].replace("'", "")));
					lin.setDescricao(l[3].replace("'", ""));
					lin.setEmpresa(empresaService.buscarPorCodigo("P"));
					lin.setPrefixo(l[2].replace("'", ""));
					serviceLinha.salvar(lin);
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}

	}

	@Listen("onSelect = #tabTrecho")
	public void onSelect$tabTrecho(Event event) {
		List<Operacao> lsOperacao = operacaoService.buscarTodos() != null ? operacaoService.buscarTodos()
				: new ArrayList<Operacao>();
		listOperacao.setData(lsOperacao);
		tabTrecho.setVisible(true);
		tabTrecho.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoTrecho")
	public void onUpload$btnSalvarArquivos(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					try {
						// int origem = Integer.parseInt(l[2]);
						Operacao ope = new Operacao();
						OperacaoPK opePK = new OperacaoPK();
						Linha lin = serviceLinha.buscarPorId(Integer.parseInt(l[7].replace("'", "")));
						opePK.setCodigoLinha(lin.getCodigoLinha());
						Localidade origem = localidadeService.buscarPorId(Integer.parseInt(l[2]));
						Localidade destino = localidadeService.buscarPorId(Integer.parseInt(l[3]));
						opePK.setOrigemId(origem.getLocalidadeId());
						opePK.setDestinoId(destino.getLocalidadeId());
						opePK.setCodigoEmpresa(lin.getEmpresa().getCodigoEmpresa());
						Classe classe = classeService.buscarPorId(l[10].replace("'", ""));
						opePK.setCodigoClasse(classe.getCodigoClasse());
						ope.setLinha(lin);
						ope.setClasse(classe);
						ope.setEmpresa(lin.getEmpresa());
						ope.setOrigem(origem);
						ope.setDestino(destino);
						ope.setOperacaoPK(opePK);
						ope.setValorTarifa(new BigDecimal(l[5].replace(",", ".")));
						ope.setValorTaxa(BigDecimal.ZERO);
						ope.setValorSeguro(new BigDecimal(l[6].replace(",", ".")));
						ope.setValorPedagio(BigDecimal.ZERO);
						ope.setValorOutros(BigDecimal.ZERO);
						ope.setDistanciaTrecho(BigDecimal.ZERO);
						operacaoService.salvar(ope);
					} catch (Exception e) {
						// e.getMessage();
					}
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}
	}

	@Listen("onSelect = #tabTipoVenda")
	public void onSelect$tabTipoVenda(Event event) {
		List<TipoPagamento> lsTipoPagamento = tipoPagamentoService.buscarTodos() != null
				? tipoPagamentoService.buscarTodos() : new ArrayList<TipoPagamento>();
		listTipoPagamento.setData(lsTipoPagamento);
		tabTipoVenda.setVisible(true);
		tabTipoVenda.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoTipoPagamento")
	public void onUpload$btnImportacaoTipoPagamento(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			Integer i = 0;
			tipoPagamentoService.deletarTodos();
			// insereFormaPagamentoDefault();
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					TipoPagamento tp = new TipoPagamento();
					tp.setCodigoTipoPagamento(i.toString());
					tp.setFator(new BigDecimal(l[1].replace("'", "").toString()));
					tp.setDescricao(l[0].replace("'", ""));
					tp.setExigeDocumento(false);
					tp.setAtivo(true);
					tipoPagamentoService.salvar(tp);
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}
	}

	private void insereFormaPagamentoDefault() {
		formaPagamentoService.deletarTodos();
		FormaPagamento fp = new FormaPagamento();
		fp.setCodigoFormaPagamento("DI");
		fp.setDescricao("DINHEIRO");
		fp.setFator(BigDecimal.ONE);
		fp.setAtivo(true);
		try {
			formaPagamentoService.salvar(fp);
		} catch (Exception e) {
			e.getMessage();
		}

	}

	@Listen("onSelect = #tabHorario")
	public void onSelect$tabHorario(Event event) {
		List<Horario> lsHorario = horarioService.buscarTodos() != null ? horarioService.buscarTodos()
				: new ArrayList<Horario>();
		listHorario.setData(lsHorario);
		tabHorario.setVisible(true);
		tabHorario.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoHorario")
	public void onUpload$btnImportacaoHorario(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			Integer i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					try {
						Horario horario = new Horario();
						HorarioPK hPK = new HorarioPK();
						hPK.setCodigoEmpresa(l[6].replace("'", ""));
						hPK.setCodigoHorario(Integer.parseInt(l[0].replace("'", "")));
						hPK.setCodigoLinha(Integer.parseInt(l[1].replace("'", "")));
						hPK.setOrigemHorario(Integer.parseInt(l[2]));
						hPK.setDestinoHorario(Integer.parseInt(l[3]));
						hPK.setHoraHorario(l[4].replace(":", "").replace("'", ""));
						hPK.setSentidoHorario(l[5].replace("'", "").charAt(0));
						horario.setHorarioPK(hPK);
						horarioService.salvar(horario);
					} catch (Exception e) {
						// e.getMessage();
					}
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}
	}

	@Listen("onSelect = #tabDetalheHorario")
	public void onSelect$tabDetalheHorario(Event event) {
		List<DetalheHorario> lsDetalheHorario = detalheHorarioService.buscarTodos() != null
				? detalheHorarioService.buscarTodos() : new ArrayList<DetalheHorario>();
		listDetalheHorario.setData(lsDetalheHorario);
		tabDetalheHorario.setVisible(true);
		tabDetalheHorario.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoDetalheHorario")
	public void onUpload$btnImportacaoDetalheHorario(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			DetalheHorario detalheHorario = new DetalheHorario();
			DetalheHorarioPK dhPK = new DetalheHorarioPK();
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			Integer i = 0;
			int ida = 1;
			int volta = 1;
			int ultimaLinhaIda = 0;
			int ultimaLinhaVolta = 0;
			int linhaAtual = 0;
			// List<Horario> lsHorarioIda =
			// horarioService.buscarListaHorariosPorSentido('I');
			// List<Horario> lsHorarioVolta =
			// horarioService.buscarListaHorariosPorSentido('V');
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					linhaAtual = Integer.parseInt(l[1].replace("'", ""));
					if (ultimaLinhaIda != linhaAtual && ultimaLinhaIda != 0 && linhaAtual != 0) {
						salvaDetalheHorarioSequenciaFinal(detalheHorario, dhPK,
								horarioService.buscarListaHorariosPorSentidoLinha('I', ultimaLinhaIda), ida);
						ida = 1;
					}
					if (l[5].replace("'", "").equals("I")) {
						List<Horario> lsHorarioIda = horarioService.buscarListaHorariosPorSentidoLinha(
								l[5].replace("'", "").charAt(0), Integer.parseInt(l[1].replace("'", "")));
						ultimaLinhaIda = Integer.parseInt(l[1].replace("'", ""));
						salvaDetalheHorarioSequencia(l, detalheHorario, dhPK, lsHorarioIda, ida);
						ida++;
					} else {
						linhaAtual = Integer.parseInt(l[1].replace("'", ""));
						if (ultimaLinhaVolta != linhaAtual && ultimaLinhaVolta != 0 && linhaAtual != 0) {
							salvaDetalheHorarioSequenciaFinal(detalheHorario, dhPK,
									horarioService.buscarListaHorariosPorSentidoLinha('V', ultimaLinhaVolta), volta);
							volta = 1;
						}
						List<Horario> lsHorarioVolta = horarioService.buscarListaHorariosPorSentidoLinha(
								l[5].replace("'", "").charAt(0), Integer.parseInt(l[1].replace("'", "")));
						ultimaLinhaVolta = Integer.parseInt(l[1].replace("'", ""));
						salvaDetalheHorarioSequencia(l, detalheHorario, dhPK, lsHorarioVolta, volta);
						volta++;
					}
					// for(Horario h :
					// horarioService.buscarListaPorCodigoHorario()){
					// dhPK.setHorarioCodigoEmpresa(h.getHorarioPK().getCodigoEmpresa());
					// dhPK.setHorarioCodigoHorario(h.getHorarioPK().getCodigoHorario());
					// dhPK.setHorarioCodigoLinha(h.getHorarioPK().getCodigoLinha());
					// dhPK.setHorarioDestinoHorario(h.getHorarioPK().getDestinoHorario());
					// dhPK.setHorarioHoraHorario(h.getHorarioPK().getHoraHorario());
					// dhPK.setHorarioOrigemHorario(h.getHorarioPK().getOrigemHorario());
					// dhPK.setHorarioSentidoHorario(h.getHorarioPK().getSentidoHorario());
					// detalheHorario.setDetalheHorarioPK(dhPK);
					// detalheHorario.setLocalidade(localidadeService.buscarPorId(h.getHorarioPK().getOrigemHorario()));
					// detalheHorarioService.salvar(detalheHorario);
					// }
				}
				i++;
			}
			salvaDetalheHorarioSequenciaFinal(detalheHorario, dhPK,
					horarioService.buscarListaHorariosPorSentidoLinha('I', ultimaLinhaIda), ida);
			salvaDetalheHorarioSequenciaFinal(detalheHorario, dhPK,
					horarioService.buscarListaHorariosPorSentidoLinha('V', ultimaLinhaVolta), volta);
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}
	}

	private void salvaDetalheHorarioSequencia(String[] l, DetalheHorario detalheHorario, DetalheHorarioPK dhPK,
			List<Horario> lsHorario, Integer sequencia) {
		if (lsHorario != null) {
			dhPK.setSequenciaDaLocalidade(sequencia);
			for (Horario h : lsHorario) {
				dhPK.setHorarioCodigoEmpresa(h.getHorarioPK().getCodigoEmpresa());
				dhPK.setHorarioCodigoHorario(h.getHorarioPK().getCodigoHorario());
				dhPK.setHorarioCodigoLinha(h.getHorarioPK().getCodigoLinha());
				dhPK.setHorarioDestinoHorario(h.getHorarioPK().getDestinoHorario());
				dhPK.setHorarioHoraHorario(h.getHorarioPK().getHoraHorario());
				dhPK.setHorarioOrigemHorario(h.getHorarioPK().getOrigemHorario());
				dhPK.setHorarioSentidoHorario(h.getHorarioPK().getSentidoHorario());
				detalheHorario.setDetalheHorarioPK(dhPK);
				detalheHorario.setLocalidade(localidadeService.buscarPorId(Integer.parseInt(l[2].replace("'", ""))));
				try {
					detalheHorarioService.salvar(detalheHorario);
				} catch (Exception e) {
					// e.getMessage();
				}
			}
		}
	}

	private void salvaDetalheHorarioSequenciaFinal(DetalheHorario detalheHorario, DetalheHorarioPK dhPK,
			List<Horario> lsHorario, Integer sequencia) {
		if (lsHorario != null) {
			dhPK.setSequenciaDaLocalidade(sequencia);
			for (Horario h : lsHorario) {
				dhPK.setHorarioCodigoEmpresa(h.getHorarioPK().getCodigoEmpresa());
				dhPK.setHorarioCodigoHorario(h.getHorarioPK().getCodigoHorario());
				dhPK.setHorarioCodigoLinha(h.getHorarioPK().getCodigoLinha());
				dhPK.setHorarioDestinoHorario(h.getHorarioPK().getDestinoHorario());
				dhPK.setHorarioHoraHorario(h.getHorarioPK().getHoraHorario());
				dhPK.setHorarioOrigemHorario(h.getHorarioPK().getOrigemHorario());
				dhPK.setHorarioSentidoHorario(h.getHorarioPK().getSentidoHorario());
				detalheHorario.setDetalheHorarioPK(dhPK);
				detalheHorario.setLocalidade(localidadeService.buscarPorId(h.getHorarioPK().getDestinoHorario()));
				try {
					detalheHorarioService.salvar(detalheHorario);
				} catch (Exception e) {
					// e.getMessage();
				}
			}
		}
	}

	@Listen("onSelect = #tabClasse")
	public void onSelect$tabClasse(Event event) {
		List<Classe> lsClasse = classeService.buscarTodos() != null ? classeService.buscarTodos()
				: new ArrayList<Classe>();
		listClasse.setData(lsClasse);
		tabClasse.setVisible(true);
		tabClasse.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoClasse")
	public void onUpload$btnSobeViagens(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					Classe classe = new Classe();
					classe.setCodigoClasse(l[0].replace("'", ""));
					classe.setDescricao(l[1].replace("'", ""));
					classeService.salvar(classe);
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}

	}

	@Listen("onSelect = #tabEmpresa")
	public void onSelect$tabEmpresa(Event event) {
		List<Empresa> lsEmpresa = empresaService.buscarTodos() != null ? empresaService.buscarTodos()
				: new ArrayList<Empresa>();
		listEmpresa.setData(lsEmpresa);
		tabEmpresa.setVisible(true);
		tabEmpresa.setSelected(true);
	}

	@Listen("onUpload=#btnImportacaoEmpresa")
	public void onUpload$btnImportacaoEmpresa(UploadEvent ev) throws IOException, ClassNotFoundException {
		if (ev.getMedia().isBinary()) {
			String linha = null;
			InputStream is = ev.getMedia().getStreamData();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			int i = 0;
			while ((linha = br.readLine()) != null) {
				String[] l = linha.split(";");
				if (i > 0) {
					Empresa empresa = new Empresa();
					empresa.setCodigoEmpresa(l[0].replace("'", ""));
					empresa.setNome(l[1].replace("'", ""));
					empresaService.salvar(empresa);
				}
				i++;
			}
			Messagebox.show("Dados importados com sucesso!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("Formato Inválido!", "Importação", Messagebox.OK, Messagebox.INFORMATION);
			// String s = ev.getMedia().getStringData();
		}

	}

	// private void salvarCaixasSelecionados() throws BusinessException {
	//
	// for (Caixa caixa : listLocalidade.getDataList()) {
	//
	// CaixaFechado caixaFechado;
	// try {
	// caixaFechado = caixaToCaixaFechado(caixa);
	// caixaFechadoService.salvarOuAtualizar(caixaFechado);
	// } catch (Exception e) {
	// throw new BusinessException("Ocorreu um erro na tranformacao VO para
	// Entidade", e);
	// }
	// }
	//
	// }

	// private void salvarViagensSelecionadas() throws BusinessException {
	//
	// for (Viagem viagem : listLinha.getDataList()) {
	// ViagemFechada viagemFechada;
	//
	// try {
	// viagemFechada = viagemToViagemFechada(viagem);
	// viagemFechadaService.salvarOuAtualizar(viagemFechada);
	//
	// } catch (Exception e) {
	// throw new BusinessException("Ocorreu um erro na tranformacao VO para
	// Entidade", e);
	// }
	// }
	// }

	// private void salvarVendasSelecionadas() throws BusinessException {
	// for (Venda venda : listOperacao.getDataList()) {
	// VendaFechada vendaFechada;
	// try {
	// vendaFechada = vendaToVendaFechada(venda);
	// vendaFechadaService.salvarOuAtualizar(vendaFechada);
	// } catch (Exception e) {
	// throw new BusinessException("Ocorreu um erro na tranformacao VO para
	// Entidade", e);
	// }
	// }
	// }

	private CaixaFechado caixaToCaixaFechado(Caixa caixa) throws ParseException {
		CaixaFechado caixaFechado = new CaixaFechado();

		SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
		CaixaFechadoPK caixaFechadoPK = new CaixaFechadoPK();
		caixaFechadoPK.setCaixaId(caixa.getCaixaId());
		caixaFechado.setCaixaFechadoPK(caixaFechadoPK);
		caixaFechado.setCodigoSessao(caixa.getSessao());
		caixaFechado.setDataAbertura(caixa.getDataAbertura());
		caixaFechado.setDataFechamento(caixa.getDataFechaento());
		caixaFechado.setEmpresaCodigoEmpresa(empresaService.buscarPorCodigo(caixa.getEmpresa().getCodEmpresa()));
		caixaFechado.setHoraAbertura(formatHora.parse(caixa.getHoraAbertura()));
		caixaFechado.setHoraFechamento(formatHora.parse(caixa.getHoraFechamento()));
		caixaFechado.setOperadorOperadorId(
				operadorService.buscarPorId(Integer.parseInt(caixa.getOperador().getMatricula())));
		caixaFechado.setValorLiquidoCaixa(new BigDecimal(caixa.getValorLiquidoCaixa()));
		caixaFechado.setValorTotalAcertos(new BigDecimal(caixa.getValorTotalAcertos()));
		caixaFechado.setValorTotalComissoes(new BigDecimal(caixa.getValorTotalComissoes()));
		caixaFechado.setValorTotalTrocos(new BigDecimal(caixa.getValorTotalTrocos()));
		caixaFechado.setValorTotalVendas(new BigDecimal(caixa.getValorTotalVendas()));

		return caixaFechado;
	}

	private ViagemFechada viagemToViagemFechada(Viagem viagem) throws ParseException, BusinessException {
		ViagemFechada viagemFechada = new ViagemFechada();

		SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
		// viagemFechada.setCaixaId(caixaFechadoService.buscarPorId(viagem.getCaixa().getCaixaId()));

		viagemFechada.setCodigoVeiculo(viagem.getCodVeiculo());
		viagemFechada.setDataFechamentoViagem(viagem.getDataFechamento());
		viagemFechada.setDataViagem(viagem.getDataViagem());
		viagemFechada.setHoraFechamentoViagem(formatHora.parse(viagem.getHoraFechamentoViagem()));

		HorarioPK pk = new HorarioPK();
		pk.setCodigoEmpresa(viagem.getHorario().getCodEmpresa());
		pk.setCodigoHorario(Integer.parseInt(viagem.getHorario().getCodServico()));
		pk.setCodigoLinha(viagem.getHorario().getCodLinha());
		pk.setDestinoHorario(viagem.getHorario().getDestino());
		pk.setHoraHorario(viagem.getHorario().getHoraServico().toString());
		pk.setOrigemHorario(viagem.getHorario().getOrigem());
		pk.setSentidoHorario(viagem.getHorario().getSentido().charAt(0));
		viagemFechada.setHorario(horarioService.buscarPorId(pk));
		viagemFechada.setHoraViagem(formatHora.parse(viagem.getHoraViagem()));
		// viagemFechada.setIdViagem(viagem.getIdViagem());
		viagemFechada.setSituacaoViagem(viagem.getSituacaoViagem());

		return viagemFechada;
	}

	private VendaFechada vendaToVendaFechada(Venda venda) throws ParseException, BusinessException {
		VendaFechada vendaFechada = new VendaFechada();

		SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
		vendaFechada.setCodigoSessao(venda.getCodSessao());
		vendaFechada.setCodigoVeiculo(venda.getCodVeiculo());
		vendaFechada.setCodigoVenda(venda.getCodVenda());
		vendaFechada.setDataAberturaCaixa(venda.getDataAberturaCaixa());
		vendaFechada.setDataAberturaVenda(venda.getDataAberturaVenda());
		vendaFechada.setDataAberturaViagem(venda.getDataAberturaViagem());

		vendaFechada
				.setFormaPagamento(formaPagamentoService.buscarPorId(venda.getFormaPagamento().getCodFormaPagamento()));
		vendaFechada.setHoraAberturaCaixa(formatHora.parse(venda.getHoraAberturaCaixa()));
		vendaFechada.setHoraAberturaVenda(formatHora.parse(venda.getHoraAberturaVenda()));

		// vendaFechada.getHorario().getHorarioPK().setCodigoHorario(venda.getCodHorario());

		OperacaoPK operacaoPK = new OperacaoPK();
		operacaoPK.setCodigoClasse(venda.getOperacao().getCodClasse());
		operacaoPK.setCodigoEmpresa(venda.getOperacao().getCodEmpresa());
		operacaoPK.setCodigoLinha(venda.getOperacao().getCodLinha());
		operacaoPK.setDestinoId(venda.getOperacao().getCodDestino());
		operacaoPK.setOrigemId(venda.getOperacao().getCodOrigem());
		// vendaFechada.setOperacao(operacaoService.buscarPorId(operacaoPK));
		vendaFechada.setOperadorId(operadorService.buscarPorId(venda.getCodUsuario()));
		vendaFechada.setTipoVenda(venda.getTipoVenda());
		vendaFechada.setValorOutros(new BigDecimal(venda.getOutros()));
		vendaFechada.setValorPedagio(new BigDecimal(venda.getPedagio()));
		vendaFechada.setValorSeguro(new BigDecimal(venda.getValorSeguro()));
		vendaFechada.setValorTaxaEmbarque(new BigDecimal(venda.getValorTaxaEmbarque()));
		vendaFechada.setValorTotalBilhete(new BigDecimal(venda.getValorTotalBilhete()));
		vendaFechada.setVendaCancelada(venda.getVendaCancelada());
		vendaFechada.setCodigoTipoPagamento(
				tipoPagamentoService.buscarPorId(venda.getTipoPagamento().getCodigoTipoPagamento()));
		vendaFechada.setDocumento(venda.getDocumentoPassageiro().toString());

		// vendaFechada.setViagemFechadaId(viagemFechadaService.buscarPorId(venda.getViagem().getIdViagem()));

		// vendaFechada.setVendaFechadaPK(new
		// VendaFechadaPK(venda.getNumeroBilhete(),
		// Integer.parseInt(venda.getCodPDA()))); //corrigir dps

		return vendaFechada;
	}

}
