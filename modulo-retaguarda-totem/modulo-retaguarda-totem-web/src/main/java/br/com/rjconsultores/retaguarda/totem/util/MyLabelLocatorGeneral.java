/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import java.net.URL;
import java.util.Locale;

import org.jboss.logging.Logger;

/**
 *
 * @author gleimar
 */
public class MyLabelLocatorGeneral implements org.zkoss.util.resource.LabelLocator {

	private static Logger log = Logger.getLogger(MyLabelLocatorGeneral.class);

	public URL locate(Locale locale) {
		URL resource = null;
		resource = this.getClass().getClassLoader().getResource("/WEB-INF/" + getI3LabelName(locale));
		if (resource == null) {
			resource = this.getClass().getClassLoader().getResource("/WEB-INF/" + getI3LabelDefault());
		}
		return resource;

	}

	private static final String getI3LabelName(Locale locale) {
		return "i3-label_" + locale + ".label";
	}

	private static final String getI3LabelDefault() {
		return "i3-label_pt_BR.label";
	}
}
