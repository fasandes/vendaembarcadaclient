package br.com.rjconsultores.retaguarda.totem.controllers.operador;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.HibernateSearchObject;
import br.com.rjconsultores.retaguarda.totem.util.paginacao.PagedListWrapper;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderOperador;

/**
 * @author fabri_000
 * 
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class PesquisarOperadorController extends MyGenericSelectorComposer<Component> {
	@WireVariable("plwLista")
	private transient PagedListWrapper<Operador> plwLista;
	@WireVariable("operadorService")
	private OperadorService operadorService;
	@Wire
	private MyListbox<Operador> operadorList;
	@Wire
	private Textbox txtNome;
	@Wire
	private Paging pagingOperador;
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		adiconaListeners();
		atualizarLista();

	}

	private void adiconaListeners() {
		operadorList.setItemRenderer(new RenderOperador());
		operadorList.addEventListener("onClick", new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				verOperador(operadorList.getSelected());

			}

		});

	}

	private void verOperador(Operador operador) {
		Map<String, Object> args = new HashMap<String, Object>();
		Boolean novo = (operador == null);
		args.put("operador", operador);
		args.put("novo", novo);

		if (novo) {
			openWindow("/gui/manutencao/operador/cadastrarEditarOperador.zul",
					Labels.getLabel("cadastrarEditarOperadorController.title.cadastrar"), args, MODAL, true);

		} else {
			openWindow("/gui/manutencao/operador/cadastrarEditarOperador.zul",
					Labels.getLabel("cadastrarEditarOperadorController.title.editar"), args, MODAL, true);
		}

	}

	private void atualizarLista() {
		HibernateSearchObject<Operador> lsOperador = new HibernateSearchObject<Operador>(Operador.class,
				pagingOperador.getPageSize());

		if (!txtNome.getText().isEmpty()) {
			lsOperador.addFilterLike("operadorName", "%" + txtNome.getValue().trim().concat("%"));
		}

		lsOperador.addSortAsc("operadorName");

		plwLista.init(lsOperador, operadorList, pagingOperador);

		// if (operadorList.getDataList().size() == 0) {
		// Messagebox.show("Nenhum Operador Cadastrado!", "Pesquisar Operador",
		// Messagebox.OK,
		// Messagebox.INFORMATION);
		// }

	}

	@Listen("onClick=#btnNovo")
	public void onClick$btnNovo(Event ev) {
		verOperador(null);
	}

	@Listen("onClick=#btnPesquisa")
	public void onClick$btnPesquisa(Event ev) {
		if (!txtNome.getText().isEmpty()) {
			atualizarLista();
		}
	}

	@Listen("onClick=#bntRefresh")
	public void onClick$btnRefresh(Event ev) {
		txtNome.setText("");
		atualizarLista();
	}

}
