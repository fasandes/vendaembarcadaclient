/*
 * 
 */
package br.com.rjconsultores.retaguarda.totem.controllers.operador;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.service.OperadorService;

/**
 * @author fabri_000
 * 
 */
@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class CadastrarEditarOperadorController extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
	@WireVariable("operadorService")
	private OperadorService operadorService;
	@Wire
	private Textbox txtMatricula;
	@Wire
	private Textbox txtNome;
	@Wire
	private Textbox txtSenha;
	@Wire
	private Checkbox chkAtivo;
	private Operador operador;
	private Boolean novo;
	@Wire
	private Window winCadastrarEditarOperador;
	@Wire
	private Combobox cboTipoOperador;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		this.operador = (Operador) Executions.getCurrent().getArg().get("operador");
		this.novo = (Boolean) Executions.getCurrent().getArg().get("novo");

		if (!novo) {
			this.txtMatricula.setText(operador.getOperadorLogin());
			this.txtNome.setText(operador.getOperadorLogin());
			if (operador.getOperadorSenha() != null) {
				this.txtSenha.setText(operador.getOperadorSenha());
			}
			this.chkAtivo.setChecked(operador.getOperadorAtivo());
			String tipoOperador;
			if (operador.getOperadorTipo() == 1) {
				tipoOperador = "App - Venda";
			}
			if (operador.getOperadorTipo() == 2) {
				tipoOperador = "App - Gps";
			} else {
				tipoOperador = "App - Venda e Gps";
			}
			this.cboTipoOperador.setText(tipoOperador);
		} else {
			this.operador = new Operador();
		}
	}

	@Listen("onClick=#btnSalvar")
	public void onClick$btnSalvar(Event ev) {

		if (this.txtMatricula.isInvalidated() || this.txtNome.isInvalidated() || this.txtSenha.isInvalidated()
				|| this.cboTipoOperador.isInvalidated()) {
			return;
		}

		this.operador.setOperadorId(Integer.parseInt(this.txtMatricula.getText()));
		this.operador.setOperadorAtivo(this.chkAtivo.isChecked());
		this.operador.setOperadorSenha(this.txtSenha.getText());
		this.operador.setOperadorTipo(Integer.parseInt(cboTipoOperador.getSelectedItem().getValue().toString()));

		if (novo) {
			Messagebox.show("Deseja cadastrar um novo Operador?", "Excluir", Messagebox.YES | Messagebox.NO,
					Messagebox.QUESTION, new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							if (((int) event.getData()) == Messagebox.YES) {
								operadorService.salvar(operador);
								Messagebox.show("Novo Operador Cadastrado!",
										Labels.getLabel("cadastrarEditarOperadorController.title.cadastrar"),
										Messagebox.OK, Messagebox.INFORMATION);
								winCadastrarEditarOperador.detach();
							}
						}
					});

		} else {
			Messagebox.show("Deseja Editar o Operador?", "Editar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							if (((int) event.getData()) == Messagebox.YES) {
								operadorService.atualizar(operador);
								Messagebox.show("Operador Editado!",
										Labels.getLabel("cadastrarEditarOperadorController.title.cadastrar"),
										Messagebox.OK, Messagebox.INFORMATION);
								winCadastrarEditarOperador.detach();
							}
						}
					});
		}
	}

}
