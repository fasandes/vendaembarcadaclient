package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;

public class RenderVincularTipoPagamento implements ListitemRenderer<TipoPagamento> {

	@Override
	public void render(Listitem item, TipoPagamento tipoPagamento, int index) throws Exception {

		Listcell lc = new Listcell("");
		lc.setParent(item);

		lc = new Listcell(tipoPagamento.getDescricao());
		lc.setParent(item);

		lc.setParent(item);

	}

}
