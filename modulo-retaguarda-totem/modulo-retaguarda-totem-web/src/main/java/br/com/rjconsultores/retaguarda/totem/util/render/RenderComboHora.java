package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

public class RenderComboHora implements ComboitemRenderer<String> {
	@Override
	public void render(Comboitem item, String data, int index) throws Exception {
		item.setValue(data);
		item.setLabel(data);

	}
}
