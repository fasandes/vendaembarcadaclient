package br.com.rjconsultores.retaguarda.totem.util.render;

import java.text.SimpleDateFormat;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.rjonsultores.vendedor.vo.Caixa;

public class RenderCaixa implements ListitemRenderer<Caixa> {

	@Override
	public void render(Listitem listItem, Caixa caixa, int arg2) throws Exception {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

		Listcell lc = new Listcell(caixa.getOperador().getMatricula());
		lc.setParent(listItem);

		lc = new Listcell(dateFormat.format(caixa.getDataAbertura()));
		lc.setParent(listItem);

		lc = new Listcell(dateFormat.format(caixa.getDataFechaento()));
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(caixa.getValorTotalVendas()));
		lc.setParent(listItem);

		lc = new Listcell(String.valueOf(caixa.getValorLiquidoCaixa()));
		lc.setParent(listItem);

		listItem.setValue(caixa);

	}
}
