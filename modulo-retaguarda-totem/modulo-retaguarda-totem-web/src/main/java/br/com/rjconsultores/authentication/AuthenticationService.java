package br.com.rjconsultores.authentication;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;
import br.com.rjconsultores.retaguarda.totem.service.UsuarioService;

public class AuthenticationService {

	@WireVariable("usuarioService")
	private UsuarioService usuarioService;

	public boolean login(Usuario usu) {
		Usuario usuario = usu;
		// a simple plan text password verification
		if (usuario == null) {
			return false;
		} else {
			Session sess = Sessions.getCurrent();
			sess.setAttribute("usuario", usuario);
			return true;
		}
	}

	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("usuario");
		Execution exec = Executions.getCurrent();
		exec.sendRedirect("index.zul");
	}
}
