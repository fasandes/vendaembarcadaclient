/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.jboss.logging.Logger;

/**
 * 
 * @author Rafael
 */
public class DateUtil {

	private static Logger log = Logger.getLogger(DateUtil.class);

	public static Integer DIA = Integer.valueOf(1000 * 60 * 60 * 24);
	public static Integer HORA = Integer.valueOf(60 * 60 * 1000);

	public Date somarDia(final Date d, final Integer dia) {

		final Calendar calendarData = Calendar.getInstance();
		calendarData.setTime(d);
		calendarData.add(Calendar.DATE, dia);
		final Date d1 = calendarData.getTime();

		return d1;
	}

	public Date somarHora(final Date d, final Integer hora) {

		final Calendar calendarData = Calendar.getInstance();
		calendarData.setTime(d);
		calendarData.add(Calendar.HOUR, hora);
		final Date d1 = calendarData.getTime();

		return d1;
	}

	public Boolean mesmoDia(final Date d, final Date d1) {
		final SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");
		if (sf.format(d).equals(sf.format(d1))) {
			return true;
		}
		return false;
	}

	public Integer diferencaDeDias(final Date d, final Date d1) {
		final SimpleDateFormat sf = new SimpleDateFormat("ddMMyyyy");

		final Calendar calendarD = Calendar.getInstance();
		calendarD.setTime(d);

		final Calendar calendarD1 = Calendar.getInstance();
		calendarD1.setTime(d);

		log.info("Data Hoje : " + sf.format(d));
		log.info("Data cons : " + sf.format(d1));

		final Long diferenca = ((d.getTime() - d1.getTime()) / DIA);
		return diferenca.intValue();
	}

	public Integer diferencaDeHoras(final Date d, final Date d1) {
		final SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		log.info("Data Hoje : " + sf.format(d));
		log.info("Data cons : " + sf.format(d1));

		final Long diferenca = ((d.getTime() - d1.getTime()) / HORA);
		return diferenca.intValue();
	}

	/**
	 * Converte uma String para um objeto Date. Caso a String seja vazia ou
	 * nula, retorna null - para facilitar em casos onde formulários podem ter
	 * campos de datas vazios.
	 * 
	 * @param data
	 *            String no formato dd/MM/yyyy a ser formatada
	 * @return Date Objeto Date ou null caso receba uma String vazia ou nula
	 * @throws Exception
	 *             Caso a String esteja no formato errado
	 */
	public Date formataData(final String data) throws Exception {
		if (data == null || data.equals("")) {
			return null;
		}

		Date date = null;
		try {
			final DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
			date = formatter.parse(data);
		} catch (final ParseException e) {
			throw e;
		}
		return date;
	}

	public String preencheSemana(final String idioma, final Date d, final Integer qtdDia) {

		final Calendar hoje = Calendar.getInstance();
		hoje.setTime(this.somarDia(new Date(), -1));

		final Calendar calendarData = Calendar.getInstance();
		calendarData.setTime(d);
		calendarData.add(Calendar.DATE, qtdDia);
		final Date d1 = calendarData.getTime();

		if (d1.before(hoje.getTime())) {
			return "";
		}

		final DateFormat df2 = new SimpleDateFormat("EEEE", new Locale(idioma, "BR"));
		String semana = df2.format(d1).toUpperCase();
		semana = semana.replace("-FEIRA", "");
		return semana;
	}

	public String preencheDiaMes(final String idioma, final Date d, final Integer qtdDia) {

		final Calendar hoje = Calendar.getInstance();
		hoje.setTime(this.somarDia(new Date(), -1));

		final Calendar calendarData = Calendar.getInstance();
		calendarData.setTime(d);
		calendarData.add(Calendar.DATE, qtdDia);
		final Date d1 = calendarData.getTime();

		if (d1.before(hoje.getTime())) {
			return "";
		}

		final DateFormat df2 = new SimpleDateFormat("dd' 'MMMM", new Locale(idioma, "BR"));
		String dia = df2.format(d1).toUpperCase();
		dia = dia.substring(0, 6);
		return dia;
	}

}
