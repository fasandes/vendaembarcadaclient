/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.util;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;

/**
 *
 * @author gleimar
 */
@SuppressWarnings("serial")
public class MyListbox<T> extends Listbox {

	private T selected = null;
	private ListModelList<T> modelList = new ListModelList<T>();

	public void setData(List<T> data) {

		this.modelList.clear();
		this.modelList.addAll(data);

		super.setModel(modelList);
	}

	public MyListbox() {
		super.setModel(modelList);
		setVflex(Boolean.TRUE);
	}

	/**
	 * Adiciona um item na lista caso ele n�o esteja presente na lista
	 * 
	 * @param item
	 * @return - True se o item foi adicionado
	 */
	@SuppressWarnings("unchecked")
	public boolean addItem(T item) {
		boolean ok = false;
		if (!this.modelList.contains(item)) {
			ok = this.modelList.add(item);
		}

		return ok;
	}

	/**
	 * Atualiza se o item est� presente na lista. Caso n�o esteja presente, o
	 * item � adicionado.
	 * 
	 * @param item
	 */
	@SuppressWarnings("unchecked")
	public void updateItem(T item) {
		if (this.modelList.contains(item)) {
			this.modelList.set(modelList.indexOf(item), item);
		} else {
			this.addItem(item);
		}
	}

	public void removeItem(T item) {
		this.modelList.remove(item);
	}

	public T removeSelectedItem() {
		int index = this.getSelectedIndex();
		selected = null;
		if (index != -1) {
			selected = this.modelList.get(index);
			this.modelList.remove(index);
		}
		return selected;
	}

	public T getSelected() {
		int index = this.getSelectedIndex();
		selected = null;
		if (index != -1) {
			selected = this.modelList.get(index);
		}

		return this.selected;
	}

	public List<T> getSelectedsItens() {
		// List<Object> selecteds = new ArrayList<Object>();
		// Set set = getSelectedItems();
		// Object[] os = set.toArray();
		// selecteds.addAll(Arrays.asList(os));
		List<T> selecteds = new ArrayList<T>();

		for (Listitem listItem : this.getSelectedItems()) {
			selecteds.add(this.modelList.get(listItem.getIndex()));
		}

		return selecteds;
	}

	public int getSize() {
		return (modelList == null) ? 0 : modelList.getSize();
	}

	@Deprecated
	public T[] getData() {
		return (T[]) this.modelList.toArray();
	}

	public ListModelList<T> getDataList() {
		return this.modelList;
	}

	public List<T> getNotSelectedItens() {
		List<T> nonSelected = new ArrayList<T>();

		for (T obj : this.modelList) {
			if (!getSelectedsItens().contains(obj)) {
				nonSelected.add(obj);
			}
		}

		return nonSelected;

	}
}
