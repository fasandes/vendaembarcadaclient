package br.com.rjconsultores.retaguarda.totem.util.as400;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public interface Executa {
	public int sincronizaLocalidade(Connection c);
	public int sincronizaClasse(Connection c);
	public int sincronizaEstado(Connection c);
	public int sincronizaEmpresa(Connection c);
	public int sincronizaPreco(Connection c, String tabelaPreco);
	public int sincronizaLinha(Connection c) throws RuntimeException;
	public int sincronizaTipoPagamento(Connection c);
	public int sincronizaFormaPagamento(Connection c) ;
	public int sincronizaTrecho(Connection c);
	public int sincronizaBilheteiros(Connection c, String agencia);
	public void atualizaBolto(Map<String, String> parametros, Connection connection) throws SQLException;
	public int sincronizaServico(Connection c) throws RuntimeException;
}
