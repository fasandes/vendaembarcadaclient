package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;

public class RenderComboEmpresa implements ComboitemRenderer<Empresa> {

	@Override
	public void render(Comboitem item, Empresa empresa, int index) throws Exception {
		item.setValue(empresa);
		item.setLabel(empresa.getNome());

	}

}
