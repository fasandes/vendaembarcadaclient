package br.com.rjconsultores.retaguarda.totem.util.as400;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.enuns.Parametros;
import br.com.rjconsultores.retaguarda.totem.service.ParametroService;

/**
 * 
 * @author Rafius
 */
@Named( value = "as400Service")
public class As400Service {

	private final Logger log = Logger.getLogger(As400Service.class);
	@Inject
	private Executa executa;
	
	private static Connection con;
	
	@Inject
	private ParametroService parametroService;
	
	public boolean conexaoValida(String ip,String lib) {
		return (getConnection(ip,lib) != null);
	}

	public void executaOperacao(String ip, String lib,String tabelaPreco) {

		log.info("Dados apagados");
		log.info("Iniciando sincronia");
		log.info("Sincronizado empresa: " + executa.sincronizaEmpresa(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado estado: " + executa.sincronizaEstado(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado forma pagamento: " + executa.sincronizaFormaPagamento(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado tipo pagamento: " + executa.sincronizaTipoPagamento(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado bilheteiros: " + executa.sincronizaBilheteiros(getConnection(ip,lib),parametroService.buscarPorId(Parametros.AGENCIA_SRVP.getChave()).getValor()) + " Registros");
		log.info("Sincronizado classe: " + executa.sincronizaClasse(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado linha: " + executa.sincronizaLinha(getConnection(ip,lib)) + " Registros");		
		log.info("Sincronizado localidades: " + executa.sincronizaLocalidade(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado servico: " + executa.sincronizaServico(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado trecho: " + executa.sincronizaTrecho(getConnection(ip,lib)) + " Registros");
		log.info("Sincronizado preco: " + executa.sincronizaPreco(getConnection(ip,lib),parametroService.buscarPorId(Parametros.AGENCIA_SRVP.getChave()).getValor()) + " Registros");
		
		
	}
	


	private Connection getConnection(String ip, String lib) {

		if (As400Service.con == null) {
			final String url = "jdbc:as400://" + ip + ":23/" + lib + ";prompt=false";
			final String driver = "com.ibm.as400.access.AS400JDBCDriver";
			final String user = "rjc";
			final String pass = "rjc09";

			try {
				Class.forName(driver);
				log.info("URL As400 - " + url);				
				con = DriverManager.getConnection(url, user, pass);
				con.setAutoCommit(true);

			} catch (final Exception e) {
				System.out.println(e);
			}

			return con;

		} else {
			return con;
		}

	}
}
