package br.com.rjconsultores.retaguarda.totem.util.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Named;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import br.com.rjconsultores.retaguarda.totem.dto.VendaFechadaDto;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;

@Named("wsConsumerGeraVendaSrvp")
public class WsConsumerGeraVendaSrvp {

	private static final Logger LOG = Logger.getLogger(WsConsumerGeraVendaSrvp.class.getName());

	public String[] geraVendaSrvp(VendaFechadaDto vendaFechadaListDto, Parametro enderecoWsGeraVendaSrvp) {
		// METODO POST - http://localhost:8080/rest/registraVendaSRVP/
		// String uri =
		// "http://localhost:8080/gera-venda-srvp-web/rest/geraVendaSrvp/registraVendaSRVP/";
		String[] retorno = new String[2];
		String uri = enderecoWsGeraVendaSrvp.getValor();
		int http_status = -1;

		try {
			Gson gson = new Gson();
			String vendaFechadaListDtoJson = gson.toJson(vendaFechadaListDto);

			// System.out.println("VendaFechadaListDto serializado (json):");
			// System.out.println(vendaFechadaListDtoJson);

			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(vendaFechadaListDtoJson);
			out.close();

			http_status = connection.getResponseCode();

			if (http_status / 100 != 2) {
				LOG.log(Level.SEVERE, "Ocorreu algum erro. Codigo de reposta: {0}{0}", http_status);
				retorno[0] = "99";
				// retorno[1] = "Ocorreu algum erro. Codigo de reposta: " +
				// http_status +"\n";
			} else {
				retorno[0] = "0";
				retorno[1] = "Vendas Exportadas Com Sucesso!";
				// return retorno;
				// retorno[1] = "Vendas Exportadas Com Sucesso! Favor
				// verificar quais vendas foram realizadas.";
			}

			if (connection.getErrorStream() != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String line;

				while ((line = reader.readLine()) != null) {
					System.out.println(line);
					retorno[1] += line;
				}
			}

			return retorno;

		} catch (Exception e) {
			// LOG.log(Level.SEVERE, null, e);
			// retorno[0] = "99";
			retorno[1] += e.getMessage();
		}
		return retorno;
	}

	public String[] fechamentoCaixaSrvp(VendaFechadaDto vendaFechadaListDto, Parametro enderecoWsGeraVendaSrvp) {
		String[] retorno = new String[2];
		String uri = enderecoWsGeraVendaSrvp.getValor();
		uri = uri.replace("registraVendaSRVP", "fechamentoCaixaSRVP");
		int http_status = -1;

		try {
			Gson gson = new Gson();
			String vendaFechadaListDtoJson = gson.toJson(vendaFechadaListDto);

			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON + ";charset=utf-8");
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(vendaFechadaListDtoJson);
			out.close();

			http_status = connection.getResponseCode();

			if (http_status / 100 != 2) {
				LOG.log(Level.SEVERE, "Ocorreu algum erro. Codigo de reposta: {0}{0}", http_status);
				retorno[0] = "99";
			} else {
				retorno[0] = "0";
				retorno[1] = "Vendas Exportadas Com Sucesso!";
			}

			if (connection.getErrorStream() != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
				String line;

				while ((line = reader.readLine()) != null) {
					System.out.println(line);
					retorno[1] += line;
				}
			}

			return retorno;

		} catch (Exception e) {
			retorno[1] += e.getMessage();
		}
		return retorno;
	}

	// public static void main(String[] args) {
	//
	// ParametrosGeracaoVenda p = new ParametrosGeracaoVenda();
	// p.setLibSrvp("VTADEMON");
	// p.setOperadorAgencia("14");
	// p.setSenhaSrvp("fsan");
	// p.setServidorSrvp("201.49.218.38");
	// p.setUsuarioSrvp("fsan");
	// List<VendaFechadaBean> lsVendaFechadaBean = new
	// ArrayList<VendaFechadaBean>();
	// VendaFechadaBean vf = new VendaFechadaBean();
	// Date d = new Date();
	//
	// vf.setVendaId(1);
	// vf.setNumeroBilhete(99);
	// vf.setDataAberturaViagem(d);
	// vf.setDataAberturaVenda(d);
	// vf.setDataAberturaCaixa(d);
	// vf.setHoraAberturaVenda(d);
	// vf.setHoraAberturaCaixa(d);
	// vf.setCodigoSessao(00);
	// vf.setCodigoVenda("");
	// vf.setValorTotalBilhete(BigDecimal.TEN);
	// vf.setTipoVenda("");
	// vf.setCodigoVeiculo("11");
	// vf.setDataFechamento(d);
	// vf.setHoraFechamento(d);
	// vf.setValorTaxaEmbarque(BigDecimal.ZERO);
	// vf.setValorSeguro(BigDecimal.ZERO);
	// vf.setVendaCancelada(null);
	// vf.setValorPedagio(BigDecimal.ZERO);
	// vf.setValorOutros(BigDecimal.ZERO);
	// vf.setDocumento("");
	// vf.setPda(1);
	// vf.setClasse("C");
	// vf.setEmpresa("N");
	// vf.setDestino(3);
	// vf.setOrigem(1);
	// vf.setLinha(11);
	// vf.setCodigoHorario(1312);
	// vf.setViagemId(1);
	// vf.setOperador(9999);
	// vf.setFormaPagamento("DI");
	// vf.setViagemCodigoVeiculo("");
	// vf.setViagemDataViagem(d);
	// vf.setViagemHoraViagem("2300");
	// vf.setViagemSituacaoViagem("fechada");
	// vf.setViagemDataFechamentoViagem(d);
	// vf.setViagemHoraFechamentoViagem(d);
	// vf.setViagemCodigoHorario(1312);
	// vf.setViagemSentidoViagem('I');
	// vf.setIdDispositivo("Fri Jul 22 17:10:37 BRT
	// 2016-ffffffff-a04c-9a23-d43a-5f95095771e8");
	// vf.setOperadorEmail("MOT1");
	//
	// lsVendaFechadaBean.add(vf);
	//
	// VendaFechadaListDto vendaFechadaListDto = new VendaFechadaListDto();
	//
	// vendaFechadaListDto.setLsVendaFechadaBean(lsVendaFechadaBean);
	// vendaFechadaListDto.setParametrosGeracaoVenda(p);
	//
	// String uri =
	// "http://localhost:8080/gera-venda-srvp-web/rest/geraVendaSrvp/registraVendaSRVP/";
	//
	// try {
	// Gson gson = new Gson();
	// String vendaFechadaListDtoJson = gson.toJson(vendaFechadaListDto);
	//
	// System.out.println("VendaFechadaListDto serializado (json):");
	// System.out.println(vendaFechadaListDtoJson);
	//
	// try {
	// URL url = new URL(uri);
	// HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	// connection.setDoOutput(true);
	// connection.setRequestProperty("Content-Type", "application/json");
	// connection.setConnectTimeout(5000);
	// connection.setReadTimeout(5000);
	// OutputStreamWriter out = new
	// OutputStreamWriter(connection.getOutputStream());
	// out.write(vendaFechadaListDtoJson);
	// out.close();
	//
	// int http_status = connection.getResponseCode();
	// if (http_status / 100 != 2) {
	// LOG.log(Level.SEVERE, "Ocorreu algum erro. Codigo de reposta: {0}",
	// http_status);
	// }
	//
	// try (BufferedReader reader = new BufferedReader(new
	// InputStreamReader(connection.getInputStream()))) {
	// String line;
	// while ((line = reader.readLine()) != null) {
	// System.out.println(line);
	// }
	// }
	// } catch (Exception e) {
	// LOG.log(Level.SEVERE, null, e);
	// }
	// } catch (Exception e) {
	// LOG.log(Level.SEVERE, null, e);
	// }
	// }

}
