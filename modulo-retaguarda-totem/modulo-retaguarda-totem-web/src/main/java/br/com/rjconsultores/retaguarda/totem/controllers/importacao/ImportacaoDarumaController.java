package br.com.rjconsultores.retaguarda.totem.controllers.importacao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.input.ReaderInputStream;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import br.com.rjconsultores.authentication.AuthenticationServiceSession;
import br.com.rjconsultores.retaguarda.totem.entidades.DarumaImport;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.Horario;
import br.com.rjconsultores.retaguarda.totem.entidades.HorarioPK;
import br.com.rjconsultores.retaguarda.totem.entidades.Operacao;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;
import br.com.rjconsultores.retaguarda.totem.entidades.VendaEmbarcadaCliVO;
import br.com.rjconsultores.retaguarda.totem.service.DarumaImportService;
import br.com.rjconsultores.retaguarda.totem.service.HorarioService;
import br.com.rjconsultores.retaguarda.totem.service.LinhaService;
import br.com.rjconsultores.retaguarda.totem.service.LocalidadeService;
import br.com.rjconsultores.retaguarda.totem.service.OperacaoService;
import br.com.rjconsultores.retaguarda.totem.service.ParametroService;
import br.com.rjconsultores.retaguarda.totem.service.TipoPagamentoService;
import br.com.rjconsultores.retaguarda.totem.service.VendaFechadaService;
import br.com.rjconsultores.retaguarda.totem.util.MyGenericSelectorComposer;
import br.com.rjconsultores.retaguarda.totem.util.MyListbox;
import br.com.rjconsultores.retaguarda.totem.util.render.RenderDarumaImport;
import br.com.rjconsultores.retaguarda.totem.util.rest.SincroniaEndPoint;
import net.sf.jasperreports.engine.JRException;

public class ImportacaoDarumaController extends MyGenericSelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Logger log = Logger.getLogger(this.getClass());
	@WireVariable("darumaImportService")
	private DarumaImportService darumaImportService;
	@WireVariable("horarioService")
	private HorarioService horarioService;
	@WireVariable("localidadeService")
	private LocalidadeService localidadeService;
	@WireVariable("serviceLinha")
	private LinhaService linhaService;
	@WireVariable("operacaoService")
	private OperacaoService operacaoService;
	@WireVariable("parametroService")
	private ParametroService parametroService;
	@WireVariable("tipoPagamentoService")
	private TipoPagamentoService tipoPagamentoService;
	@WireVariable("vendaFechadaService")
	private VendaFechadaService vendaFechadaService;
	@Wire
	private MyListbox<DarumaImport> listDarumaImport;
	@Wire
	private Button btnSobeArquivoDaruma;
	@Wire
	private Button btnSalvarArquivosDaruma;
	@WireVariable
	private SincroniaEndPoint sincroniaEndPoint;
	// @Wire
	// private Button btnTotalBus;
	@Wire
	private Window winImportacao;
	private String cabecalho;
	private Empresa empresa;
	private Operador operador;
	private List<DarumaImport> lsDarumaImportVerificaBilhete;
	private List<VendaEmbarcadaCliVO> listaVendaDaruma;
	private Usuario usuario;
	private Boolean exibeMensagem;

	@SuppressWarnings({ "unchecked" })
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		listaVendaDaruma = new ArrayList<>();
		List<DarumaImport> lsVendasDarumaPendente = darumaImportService.buscarVendasPendentes();
		if (lsVendasDarumaPendente != null && !lsVendasDarumaPendente.isEmpty()) {
			btnSalvarArquivosDaruma.setDisabled(false);

			listDarumaImport.setData(lsVendasDarumaPendente);
		}
		listDarumaImport.setItemRenderer(new RenderDarumaImport());
		empresa = (Empresa) Executions.getCurrent().getArg().get("empresa");
		operador = (Operador) Executions.getCurrent().getArg().get("operador");
	}

	@Listen("onDoubleClick=#listDarumaImport")
	public void onDoubleClick$listDarumaImport() {
		DarumaImport daruma = new DarumaImport();
		daruma = (DarumaImport) listDarumaImport.getSelected();
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("daruma", daruma);
		args.put("listDarumaImport", listDarumaImport);
		Window window = (Window) Executions.createComponents("/gui/importacao/daruma/editaArquivosDaruma.zul", null,
				args);
		window.doModal();
	}

	@SuppressWarnings("unchecked")
	@Listen("onUpload=#btnSobeArquivoDaruma")
	public void onUpload$btnSobeArquivoDaruma(UploadEvent ev) throws IOException, ClassNotFoundException {
		// org.zkoss.util.media.Media media = ev.getMedia();
		// darumaImportService.limpaTabelaDarumaImport(); // limpa a tabela
		// darumaIMport antes de carregar o novo arquivo
		BufferedReader texto = new BufferedReader(
				new InputStreamReader(new ReaderInputStream(ev.getMedia().getReaderData()), Charset.forName("UTF-8")));

		// InputStreamReader arquivo = ( InputStreamReader
		// )media.getReaderData();

		List<DarumaImport> lsDarumaImport = verificaBilhetes(texto);
		StringBuilder sb = new StringBuilder();

		if (lsDarumaImportVerificaBilhete.size() > 0) {
			for (DarumaImport daruma : lsDarumaImportVerificaBilhete) {
				HorarioPK horario = new HorarioPK();
				SimpleDateFormat formatoHora = new SimpleDateFormat("HHmm");
				horario.setHoraHorario(formatoHora.format(daruma.getHora()));
				String codigoLinha[] = daruma.getLinha().split(" ");
				horario.setCodigoLinha((linhaService.buscarPorId(Integer.parseInt(codigoLinha[0]))).getCodigoLinha());
				// if (!horarioService.validaHorarioLinha(horario)) {
				// sb.append("\nBilhete: " + daruma.getBilhete() + "Favor
				// verificar o horário do bilhete!");
				// }

				if (!horarioService.validaHorarioLinha(horario)) {
					if (!darumaImportService.verificaImportacaoDuplicada(daruma, false)) {
						sb.append("\nVenda exportada anteriormente, favor verificar! Bilhete: " + daruma.getBilhete());
					} else {
						sb.append("\nBilhete: " + daruma.getBilhete() + "Favor verificar o horário do bilhete!");
					}
				}

				// if (darumaImportService.verificaImportacaoDuplicada(daruma,
				// false)) {
				// sb.append("\nVenda exportada anteriormente, favor verificar!
				// Bilhete: " + daruma.getBilhete());
				// }else if(!horarioService.validaHorarioLinha(horario)){
				// sb.append("\nBilhete: " + daruma.getBilhete() + "Favor
				// verificar o horário do bilhete!");
				// }

				// preencheListaVendaDaruma(daruma, horario, codigoLinha);
			}

			if (!sb.toString().equals("")) {
				Messagebox.show(sb.toString(), "Arquivos Daruma", Messagebox.OK, Messagebox.INFORMATION);
			}

			// List<VendaEmbarcadaCliVO> lsVOTotalBus =
			// sincroniaEndPoint.registraVendaTotalBus(listaVendaDaruma);

			// int i = 0;
			// for (VendaEmbarcadaCliVO vendaVO : lsVOTotalBus) {
			// DarumaImport daruma = lsDarumaImportVerificaBilhete.get(i);
			// daruma.setRetornoTotalBus(vendaVO.getStatusVenda().getCodigoStatus());
			// darumaImportService.salvarOuAtualizar(daruma);
			// i++;
			//
			// if (vendaVO.getStatusVenda().getCodigoStatus() != 0) {
			// listDarumaImport.setData(lsDarumaImport);
			// btnSalvarArquivosDaruma.setDisabled(false);
			// Messagebox.show(vendaVO.getStatusVenda().getDescricaoStatus(),
			// "Retorno Total Bus", Messagebox.OK,
			// Messagebox.ERROR);
			// }
			// }
		}

		// if (!exibeMensagem) {
		// if (lsDarumaImport == null || lsDarumaImport.size() == 0) {
		// Messagebox.show("Todos os dados foram importados com sucesso",
		// "Arquivos Daruma", Messagebox.OK,
		// Messagebox.INFORMATION);
		//
		// } else {
		// Messagebox.show(lsDarumaImport.size() + " registros pendentes",
		// "Arquivos Daruma", Messagebox.OK,
		// Messagebox.INFORMATION);
		//
		listDarumaImport.setData(lsDarumaImportVerificaBilhete);
		// }
		// }

	}

	@Listen("onClick=#btnSalvarArquivosDaruma")
	public void onClick$btnSalvarArquivosDaruma() throws JRException {
		List<DarumaImport> listaDarumaImport = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		// int i = 0;
		for (DarumaImport daruma : listDarumaImport.getDataList()) {
			HorarioPK horario = new HorarioPK();
			SimpleDateFormat formatoHora = new SimpleDateFormat("HHmm");
			horario.setHoraHorario(formatoHora.format(daruma.getHora()));
			String codigoLinha[] = daruma.getLinha().split(" ");
			horario.setCodigoLinha((linhaService.buscarPorId(Integer.parseInt(codigoLinha[0]))).getCodigoLinha());
			// i++;
			if (!darumaImportService.verificaImportacaoDuplicada(daruma, true)) {
				if (horarioService.validaHorarioLinha(horario)) {
					preencheListaVendaDaruma(daruma, horario, codigoLinha);
					darumaImportService.salvarOuAtualizar(daruma);
					// sb.append("\nBilhete: " + daruma.getBilhete() + " - Venda
					// Gerada com Sucesso!");
				} else {
					darumaImportService.salvarOuAtualizar(daruma);
					listaDarumaImport.add(daruma);
					sb.append("\nBilhete: " + daruma.getBilhete() + "Favor verificar o horário do bilhete!");
				}
			} else {
				// geraRelatorio = false;
				// Messagebox.show("Venda exportada anteriormente, favor
				// verificar! Bilhete: " + daruma.getBilhete(),
				// "Retorno Total Bus", Messagebox.OK, Messagebox.INFORMATION);
				sb.append("\nVenda exportada anteriormente, favor verificar! Bilhete: " + daruma.getBilhete());
			}
			// if (listaDarumaImport.size() > 0) {
			// i = 0;
			// darumaImportService.salvar(daruma);
			// geraRelatorio = false;
			// sb.append("\nBilhete: " + daruma.getBilhete() + "Favor verificar
			// o horário do bilhete!");
			// Messagebox.show("Favor verificar o horário dos bilhetes!",
			// "Arquivos Daruma", Messagebox.OK,
			// Messagebox.EXCLAMATION);
			// } else {

			// sb.append("\nBilhete: " + daruma.getBilhete() + " - Venda Gerada
			// com Sucesso!");
			// }
			// geraRelatorio = true;
			// darumaImportService.salvar(daruma);
		}

		// Messagebox.show(sb.toString(), "Arquivos Daruma", Messagebox.OK,
		// Messagebox.EXCLAMATION);

		// listDarumaImport.setData(listaDarumaImport);
		// if (listaDarumaImport.size() == 0) {
		// geraRelatorio = true;
		// }
		// if (geraRelatorio) {

		// Messagebox.show("Dados das Vendas Não Enviadas Salvos com Sucesso!",
		// "Arquivos Daruma", Messagebox.OK,
		// Messagebox.INFORMATION);

		// SincroniaEndPoint sinc = new SincroniaEndPoint();
		// sinc.registraVendaTotalBus(listaVendaDaruma);

		List<VendaEmbarcadaCliVO> lsVOTotalBus = null;
		// StringBuilder sb1 = new StringBuilder();
		try {
			lsVOTotalBus = sincroniaEndPoint.registraVendaTotalBus(listaVendaDaruma);
			int i = 0;
			for (VendaEmbarcadaCliVO vendaVO : lsVOTotalBus) {
				DarumaImport daruma = listDarumaImport.getDataList().get(i);
				daruma.setRetornoTotalBus(vendaVO.getStatusVenda().getCodigoStatus());
				darumaImportService.salvarOuAtualizar(daruma);
				i++;
				//
				sb.append("\nBilhete:" + vendaVO.getNumeroBilhete() + "-Retorno: "
						+ vendaVO.getStatusVenda().getDescricaoStatus());
				// if (vendaVO.getStatusVenda().getCodigoStatus() >= 0) {
				// // listDarumaImport.setData(daruma);
				// Messagebox.show(vendaVO.getStatusVenda().getDescricaoStatus(),
				// "Retorno Total Bus",
				// Messagebox.OK, Messagebox.INFORMATION);
				// }
			}

			// listDarumaImport.setData(daruma);
			// Messagebox.show(sb1.toString() , "Retorno Total Bus",
			// Messagebox.OK, Messagebox.INFORMATION);
			//
		} catch (Exception e) {
			Messagebox.show("Erro ao estabelecer conexão com o web-service de geração de venda.", "Retorno Total Bus",
					Messagebox.OK, Messagebox.INFORMATION);
		}

		// // Map<String, Object> parametros = new HashMap<String,
		// Object>();
		// parametros.put("motorista", operador.getNome());
		// parametros.put("empresa", empresa.getNome());
		// JasperDesign caminho = JRXmlLoader.load(
		// "C:\\Ambiente\\projetos\\retaguarda_santacruz\\comporte_bch\\modulo-retaguarda\\modulo-retaguarda-web\\src\\main\\webapp\\gui\\relatorios\\daruma\\ddvb.jrxml");
		//
		// JasperReport relatorio =
		// JasperCompileManager.compileReport(caminho);
		// List<DarumaImport> lsDarumaImport =
		// darumaImportService.buscarTodosRelatorio();
		// JRBeanCollectionDataSource ds = new
		// JRBeanCollectionDataSource(lsDarumaImport, false);
		// JRDataSource dados = ds;
		//
		// byte[] teste = JasperRunManager.runReportToPdf(relatorio,
		// parametros, dados);
		// Filedownload.save(teste, "text/pdf", "teste.pdf");

		// JasperViewer viewer = new JasperViewer(jasperPrint, true);
		// JasperExportManager.exportReportToPdfFile(jasperPrint,
		// "teste.pdf");
		// viewer.setVisible(true);
		// }

		if (!sb.toString().equals("")) {
			Messagebox.show(sb.toString(), "Arquivos Daruma", Messagebox.OK, Messagebox.EXCLAMATION);
		}

		btnSalvarArquivosDaruma.setDisabled(true);

		winImportacao.detach();
	}

	public List<DarumaImport> verificaBilhetes(BufferedReader texto) throws IOException {
		List<DarumaImport> lsDarumaImport = new ArrayList<>();
		lsDarumaImportVerificaBilhete = new ArrayList<>();
		SimpleDateFormat formatHora = new SimpleDateFormat("ddMMyyyyHHmm");
		SimpleDateFormat formatData = new SimpleDateFormat("ddMMyyyy");
		DarumaImport darumaImport = new DarumaImport();
		String linha = texto.readLine();
		String operador = "";
		String desc_linha = "";
		String horario_linha = "";
		String sentido = "";
		String tabela_preco = "";
		String numeroEcf = "";
		StringBuilder sb = new StringBuilder();
		int i = 1;
		while (linha != null) {
			if (i > 1 && i <= 8) {
				cabecalho += linha + "\n";
			}

			if (i >= 6) {
				if (linha.length() >= 3 && linha.substring(0, 3).equalsIgnoreCase("ECF")) {
					numeroEcf = linha.substring(5, 11);
				}
			}

			if (i >= 11) {
				try {
					darumaImport = new DarumaImport();
					darumaImport.setNumeroECF(numeroEcf);
					HorarioPK horario = new HorarioPK();
					darumaImport.setCodigo(linha.substring(0, 2));
					if (darumaImport.getCodigo().equals("B1")) {
						operador = linha.substring(17, 23);
					}
					if (darumaImport.getCodigo().equals("B0")) {
						// SimpleDateFormat formatoHora = new
						// SimpleDateFormat("HHmm");
						horario_linha = linha.substring(55, 59);
						desc_linha = linha.substring(17, 49);
						sentido = linha.substring(53, 54);
						tabela_preco += linha.substring(89, 96);
					}
					if (darumaImport.getCodigo().equals("A6")) {
						darumaImport.setOperador(operador);
						darumaImport.setLinha(desc_linha);
						darumaImport.setSentido_linha(sentido);
						darumaImport.setTabela_preco(tabela_preco);
						darumaImport.setHorario_linha(horario_linha);
						darumaImport.setDtBilhete(formatData.parse(linha.substring(3, 11)));
						darumaImport.setHora(formatHora.parse(linha.substring(3, 11) + horario_linha));
						darumaImport.setBilhete(linha.substring(17, 23));
						darumaImport.setNome_origem(linha.substring(24, 40));
						darumaImport.setCodigo_origem(linha.substring(41, 47));
						darumaImport.setNome_destino(linha.substring(48, 64));
						darumaImport.setCodigo_destino(linha.substring(65, 71));
						darumaImport.setEmissao(formatHora.parse(linha.substring(3, 11) + linha.substring(72, 76)));
						double valor = Integer.parseInt(linha.substring(77, 83));
						darumaImport.setValor_tarifa(new BigDecimal(valor / 100).setScale(2, RoundingMode.HALF_EVEN));
						darumaImport.setAliquota(linha.substring(89, 91));
						double valor_aliquota = Integer.parseInt(linha.substring(91, 95));
						darumaImport.setValor_aliquota(
								new BigDecimal(valor_aliquota / 100).setScale(2, RoundingMode.HALF_EVEN));
						darumaImport.setEstado(linha.substring(95, 97));
						double seguro = Integer.parseInt(linha.substring(98, 104));
						darumaImport.setSeguro(new BigDecimal(seguro / 100).setScale(2, RoundingMode.HALF_EVEN));
						darumaImport.setAliquota_seguro(linha.substring(105, 106));
						darumaImport.setCategoria_venda(linha.substring(131, 147));
						darumaImport.setRetornoTotalBus(99);
						String codigoLinha[] = desc_linha.split(" ");
						horario.setCodigoLinha(
								linhaService.buscarPorId(Integer.parseInt(codigoLinha[0])).getCodigoLinha());
						horario.setHoraHorario(horario_linha);
						if (!darumaImportService.verificaImportacaoDuplicada(darumaImport,
								horarioService.validaHorarioLinha(horario))) {
							darumaImportService.salvar(darumaImport);
							if (horarioService.validaHorarioLinha(horario) == true) {
								lsDarumaImportVerificaBilhete.add(darumaImport);
							} else {
								lsDarumaImportVerificaBilhete.add(darumaImport);
								btnSalvarArquivosDaruma.setDisabled(false);
							}
							exibeMensagem = false;
						} else {
							exibeMensagem = true;
							sb.append("\nVenda exportada anteriormente, favor verificar! Bilhete: "
									+ darumaImport.getBilhete());
						}
					}
				} catch (Exception e) {
					e.getMessage();
					break;
				}
				// if (exibeMensagem) {
				// Messagebox.show(
				// "Venda exportada anteriormente, favor verificar! Bilhete: " +
				// darumaImport.getBilhete(),
				// "Retorno Total Bus", Messagebox.OK, Messagebox.INFORMATION);
				// }
			}
			i++;
			linha = texto.readLine();
		}
		if (!sb.toString().equals("")) {
			Messagebox.show(sb.toString(), "Retorno Total Bus", Messagebox.OK, Messagebox.INFORMATION);
		}
		return lsDarumaImport;
	}

	private void preencheListaVendaDaruma(DarumaImport daruma, HorarioPK horario, String[] codigoLinha) {
		// :::::::::::>>>>>>> refatorar
		SimpleDateFormat formatDataWS = new SimpleDateFormat("dd_MM_yyyy__HH_mm_ss");
		Parametro parametroPoltrona = parametroService.buscarPorId(Parametro.POLTRONA);
		Parametro parametroTipoVenda = parametroService.buscarPorId(Parametro.TIPO_VENDA);
		Parametro parametroPontoVenda = parametroService.buscarPorId(Parametro.PONTO_VENDA);
		Parametro parametroEstacao = parametroService.buscarPorId(Parametro.ESTACAO);
		Parametro parametroIdUsuario = parametroService.buscarPorId(Parametro.USUARIO);
		Parametro parametroFormaPago = parametroService.buscarPorId(Parametro.FORMA_PAGO);
		VendaEmbarcadaCliVO vendaDaruma = new VendaEmbarcadaCliVO();
		AuthenticationServiceSession authService = new AuthenticationServiceSession();
		usuario = (Usuario) authService.getUserCredential();
		// listaVendaDaruma = new ArrayList<>();
		vendaDaruma.setNumeroAssento(parametroPoltrona.getValor());
		vendaDaruma.setCategoriaVenda(daruma.getCategoria_venda().trim());
		vendaDaruma.setOrigemId(Integer.parseInt(daruma.getCodigo_origem()));
		vendaDaruma.setDestinoId(Integer.parseInt(daruma.getCodigo_destino()));
		Horario horarioDaruma = new Horario();
		horarioDaruma = horarioService.buscarHorario(Integer.parseInt(codigoLinha[0]),
				Integer.parseInt(daruma.getCodigo_origem()), Integer.parseInt(daruma.getCodigo_destino()),
				daruma.getSentido_linha().charAt(0), daruma.getHora());
		vendaDaruma.setServicoId(horarioDaruma == null ? 0 : horarioDaruma.getHorarioPK().getCodigoHorario());
		vendaDaruma.setDataCorrida(formatDataWS.format(daruma.getHora()));
		vendaDaruma.setDataVenda(formatDataWS.format(daruma.getEmissao()));
		vendaDaruma.setPrecoTarifa(daruma.getValor_tarifa());
		vendaDaruma.setTipoVendaId(Integer.parseInt(parametroTipoVenda.getValor()));
		vendaDaruma.setPontoVendaId(usuario.getPontoVendaTotalbus());
		vendaDaruma.setEmpresaCorridaId(Integer
				.parseInt(linhaService.buscarPorId(Integer.parseInt(codigoLinha[0])).getEmpresa().getCodigoEmpresa()));
		vendaDaruma.setEstacaoId(usuario.getEstacaoTotalbus());
		vendaDaruma.setUsuarioId(usuario.getUsuarioTotalbus());
		vendaDaruma.setEmpresaId(Integer
				.parseInt(linhaService.buscarPorId(Integer.parseInt(codigoLinha[0])).getEmpresa().getCodigoEmpresa()));
		vendaDaruma.setPrecoEmbarque(BigDecimal.ZERO);
		vendaDaruma.setPrecoPedagio(BigDecimal.ZERO);
		// vendaDaruma.setPrecoOutros(daruma.getValor_aliquota());
		vendaDaruma.setPrecoOutros(BigDecimal.ZERO);
		vendaDaruma.setPrecoSeguro(daruma.getSeguro());
		vendaDaruma.setCodigoRuta(linhaService.buscarPorId(Integer.parseInt(codigoLinha[0])).getCodigoLinha());
		vendaDaruma.setFormaPagoId(Integer.parseInt(parametroFormaPago.getValor()));
		Operacao op = operacaoService.buscaValorTarifa(linhaService.buscarPorId(Integer.parseInt(codigoLinha[0])),
				localidadeService.buscarPorId(Integer.parseInt(daruma.getCodigo_origem())),
				localidadeService.buscarPorId(Integer.parseInt(daruma.getCodigo_destino())));
		try {
			vendaDaruma.setClasseServicioId(Integer.parseInt(op.getClasse().getCodigoClasse()));
		} catch (Exception e) {
			Messagebox.show("Não localizamos preço para origem: " + daruma.getCodigo_origem() + " destino: "
					+ daruma.getCodigo_destino() + "", "Erro!", Messagebox.OK, Messagebox.ERROR);
		}
		vendaDaruma.setNumeroBilhete(daruma.getBilhete());
		vendaDaruma.setNumeroECF(daruma.getNumeroECF());
		if (op != null) {
			listaVendaDaruma.add(vendaDaruma);
		} else {
			listDarumaImport.addItem(daruma);
		}
	}

	// public VendaEmbarcadaVO toVO(DarumaImport entity) {
	// final VendaEmbarcadaVO vendaEmbarcada = new VendaEmbarcadaVO();
	// vendaEmbarcada.setUsuarioId();
	// vendaEmbarcada.setClienteId();
	// vendaEmbarcada.setCategoriaId(categoriaId);
	// vendaEmbarcada.setCorridaId(servicoId);
	// vendaEmbarcada.setDestinoId(destinoId);
	// vendaEmbarcada.setOrigemId(origemId);
	// vendaEmbarcada.setEmpresaCorridaId(empresaCorridaId);
	// vendaEmbarcada.setTipoVentaId(TIPO_VENDA_NORMAL);
	// vendaEmbarcada.setEmpresaId(empresaId);
	// vendaEmbarcada.setPrecio(preco);
	// vendaEmbarcada.setNumAsiento(numeroAssento);
	// vendaEmbarcada.setPuntoVentaId(pontoVendaId);
	// //fixme verificar campo empresaPuntoVentaId
	// vendaEmbarcada.setEmpresaPuntoVentaId(empresaId.intValue());
	// vendaEmbarcada.setEstacionId(estacaoId);
	// vendaEmbarcada.setRutaId(rutaId);
	// vendaEmbarcada.setClaseServicioId(classeServicoId);
	// vendaEmbarcada.setFormaPagoId(formaPagoId);
	// }

}