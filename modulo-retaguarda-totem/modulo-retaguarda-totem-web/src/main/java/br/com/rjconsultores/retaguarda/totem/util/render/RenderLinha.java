package br.com.rjconsultores.retaguarda.totem.util.render;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import br.com.rjconsultores.retaguarda.totem.entidades.Linha;

public class RenderLinha implements ListitemRenderer<Object> {

	@Override
	public void render(Listitem item, Object data, int index) throws Exception {
		Linha linha = (Linha) data;

		Listcell lc = new Listcell("");
		lc.setParent(item);

		lc = new Listcell(linha.getCodigoLinha().toString());
		lc.setParent(item);

		lc = new Listcell(linha.getDescricao());
		lc.setParent(item);

		lc = new Listcell(linha.getPrefixo());
		lc.setParent(item);

		lc = new Listcell(linha.getEmpresa().getNome());
		lc.setParent(item);

		item.setValue(linha);

	}

}
