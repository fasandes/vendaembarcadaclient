package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the forma_pagamento database table.
 * 
 */
@Entity
@Table(name="forma_pagamento")
@NamedQuery(name="FormaPagamento.findAll", query="SELECT f FROM FormaPagamento f")
public class FormaPagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="codigo_forma_pagamento")
	private String codigoFormaPagamento;

	private Boolean ativo;

	private String descricao;

	private BigDecimal fator;

	public FormaPagamento() {
	}

	public String getCodigoFormaPagamento() {
		return this.codigoFormaPagamento;
	}

	public void setCodigoFormaPagamento(String codigoFormaPagamento) {
		this.codigoFormaPagamento = codigoFormaPagamento;
	}

	public Boolean getAtivo() {
		return this.ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getFator() {
		return this.fator;
	}

	public void setFator(BigDecimal fator) {
		this.fator = fator;
	}

}