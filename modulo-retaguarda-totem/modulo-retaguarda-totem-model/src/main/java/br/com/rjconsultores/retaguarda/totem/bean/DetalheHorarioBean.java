package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;

public class DetalheHorarioBean {
	
	private Integer sequenciaDaLocalidade;
    private Integer codigoLocalidade;
    private Integer codigoHorario;
    private Integer codigoLinha;
    private String codigoEmpresa;
    private Integer origemHorario;
    private Integer destinoHorario;
    private String horaHorario;
    private char sentidoHorario;
    private BigDecimal latitude;
    private BigDecimal longitude;
	public Integer getSequenciaDaLocalidade() {
		return sequenciaDaLocalidade;
	}
	public void setSequenciaDaLocalidade(Integer sequenciaDaLocalidade) {
		this.sequenciaDaLocalidade = sequenciaDaLocalidade;
	}
	public Integer getCodigoLocalidade() {
		return codigoLocalidade;
	}
	public void setCodigoLocalidade(Integer codigoLocalidade) {
		this.codigoLocalidade = codigoLocalidade;
	}
	public Integer getCodigoHorario() {
		return codigoHorario;
	}
	public void setCodigoHorario(Integer codigoHorario) {
		this.codigoHorario = codigoHorario;
	}
	public Integer getCodigoLinha() {
		return codigoLinha;
	}
	public void setCodigoLinha(Integer codigoLinha) {
		this.codigoLinha = codigoLinha;
	}
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	public Integer getOrigemHorario() {
		return origemHorario;
	}
	public void setOrigemHorario(Integer origemHorario) {
		this.origemHorario = origemHorario;
	}
	public Integer getDestinoHorario() {
		return destinoHorario;
	}
	public void setDestinoHorario(Integer destinoHorario) {
		this.destinoHorario = destinoHorario;
	}
	public String getHoraHorario() {
		return horaHorario;
	}
	public void setHoraHorario(String horaHorario) {
		this.horaHorario = horaHorario;
	}
	public char getSentidoHorario() {
		return sentidoHorario;
	}
	public void setSentidoHorario(char sentidoHorario) {
		this.sentidoHorario = sentidoHorario;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoEmpresa == null) ? 0 : codigoEmpresa.hashCode());
		result = prime * result + ((codigoHorario == null) ? 0 : codigoHorario.hashCode());
		result = prime * result + ((codigoLinha == null) ? 0 : codigoLinha.hashCode());
		result = prime * result + ((codigoLocalidade == null) ? 0 : codigoLocalidade.hashCode());
		result = prime * result + ((destinoHorario == null) ? 0 : destinoHorario.hashCode());
		result = prime * result + ((horaHorario == null) ? 0 : horaHorario.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((origemHorario == null) ? 0 : origemHorario.hashCode());
		result = prime * result + sentidoHorario;
		result = prime * result + ((sequenciaDaLocalidade == null) ? 0 : sequenciaDaLocalidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalheHorarioBean other = (DetalheHorarioBean) obj;
		if (codigoEmpresa == null) {
			if (other.codigoEmpresa != null)
				return false;
		} else if (!codigoEmpresa.equals(other.codigoEmpresa))
			return false;
		if (codigoHorario == null) {
			if (other.codigoHorario != null)
				return false;
		} else if (!codigoHorario.equals(other.codigoHorario))
			return false;
		if (codigoLinha == null) {
			if (other.codigoLinha != null)
				return false;
		} else if (!codigoLinha.equals(other.codigoLinha))
			return false;
		if (codigoLocalidade == null) {
			if (other.codigoLocalidade != null)
				return false;
		} else if (!codigoLocalidade.equals(other.codigoLocalidade))
			return false;
		if (destinoHorario == null) {
			if (other.destinoHorario != null)
				return false;
		} else if (!destinoHorario.equals(other.destinoHorario))
			return false;
		if (horaHorario == null) {
			if (other.horaHorario != null)
				return false;
		} else if (!horaHorario.equals(other.horaHorario))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (origemHorario == null) {
			if (other.origemHorario != null)
				return false;
		} else if (!origemHorario.equals(other.origemHorario))
			return false;
		if (sentidoHorario != other.sentidoHorario)
			return false;
		if (sequenciaDaLocalidade == null) {
			if (other.sequenciaDaLocalidade != null)
				return false;
		} else if (!sequenciaDaLocalidade.equals(other.sequenciaDaLocalidade))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DetalheHorarioBean [sequenciaDaLocalidade=");
		builder.append(sequenciaDaLocalidade);
		builder.append(", codigoLocalidade=");
		builder.append(codigoLocalidade);
		builder.append(", codigoHorario=");
		builder.append(codigoHorario);
		builder.append(", codigoLinha=");
		builder.append(codigoLinha);
		builder.append(", codigoEmpresa=");
		builder.append(codigoEmpresa);
		builder.append(", origemHorario=");
		builder.append(origemHorario);
		builder.append(", destinoHorario=");
		builder.append(destinoHorario);
		builder.append(", horaHorario=");
		builder.append(horaHorario);
		builder.append(", sentidoHorario=");
		builder.append(sentidoHorario);
		builder.append(", latitude=");
		builder.append(latitude);
		builder.append(", longitude=");
		builder.append(longitude);
		builder.append("]");
		return builder.toString();
	}
	
}
