package br.com.rjconsultores.retaguarda.totem.dto;

public class ParametrosGeracaoVenda {

	private String servidorSrvp;
	private String usuarioSrvp;
	private String senhaSrvp;
	private String libSrvp;
	private String operadorAgencia;

	public ParametrosGeracaoVenda() {
		super();
	}

	public ParametrosGeracaoVenda(String servidorSrvp, String usuarioSrvp, String senhaSrvp, String libSrvp, String operadorAgencia) {
		super();
		this.servidorSrvp = servidorSrvp;
		this.usuarioSrvp = usuarioSrvp;
		this.senhaSrvp = senhaSrvp;
		this.libSrvp = libSrvp;
		this.operadorAgencia = operadorAgencia;
	}

	public String getServidorSrvp() {
		return servidorSrvp;
	}

	public void setServidorSrvp(String servidorSrvp) {
		this.servidorSrvp = servidorSrvp;
	}

	public String getUsuarioSrvp() {
		return usuarioSrvp;
	}

	public void setUsuarioSrvp(String usuarioSrvp) {
		this.usuarioSrvp = usuarioSrvp;
	}

	public String getSenhaSrvp() {
		return senhaSrvp;
	}

	public void setSenhaSrvp(String senhaSrvp) {
		this.senhaSrvp = senhaSrvp;
	}

	public String getLibSrvp() {
		return libSrvp;
	}

	public void setLibSrvp(String libSrvp) {
		this.libSrvp = libSrvp;
	}

	public String getOperadorAgencia() {
		return operadorAgencia;
	}

	public void setOperadorAgencia(String operadorAgencia) {
		this.operadorAgencia = operadorAgencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((libSrvp == null) ? 0 : libSrvp.hashCode());
		result = prime * result + ((operadorAgencia == null) ? 0 : operadorAgencia.hashCode());
		result = prime * result + ((senhaSrvp == null) ? 0 : senhaSrvp.hashCode());
		result = prime * result + ((servidorSrvp == null) ? 0 : servidorSrvp.hashCode());
		result = prime * result + ((usuarioSrvp == null) ? 0 : usuarioSrvp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametrosGeracaoVenda other = (ParametrosGeracaoVenda) obj;
		if (libSrvp == null) {
			if (other.libSrvp != null)
				return false;
		} else if (!libSrvp.equals(other.libSrvp))
			return false;
		if (operadorAgencia == null) {
			if (other.operadorAgencia != null)
				return false;
		} else if (!operadorAgencia.equals(other.operadorAgencia))
			return false;
		if (senhaSrvp == null) {
			if (other.senhaSrvp != null)
				return false;
		} else if (!senhaSrvp.equals(other.senhaSrvp))
			return false;
		if (servidorSrvp == null) {
			if (other.servidorSrvp != null)
				return false;
		} else if (!servidorSrvp.equals(other.servidorSrvp))
			return false;
		if (usuarioSrvp == null) {
			if (other.usuarioSrvp != null)
				return false;
		} else if (!usuarioSrvp.equals(other.usuarioSrvp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ParametrosGeracaoVenda [servidorSrvp=" + servidorSrvp + ", usuarioSrvp=" + usuarioSrvp + ", senhaSrvp="
				+ senhaSrvp + ", libSrvp=" + libSrvp + ", operadorAgencia=" + operadorAgencia + "]";
	}

}
