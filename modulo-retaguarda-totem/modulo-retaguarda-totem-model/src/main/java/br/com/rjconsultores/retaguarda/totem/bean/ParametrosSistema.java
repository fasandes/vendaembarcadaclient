package br.com.rjconsultores.retaguarda.totem.bean;

public class ParametrosSistema {
	
	public static final String VENDE = "vende";
	public static final String LOGIN = "login";
	public static final String LOGIN_EMAIL = "EMAIL";
	public static final String CONFIRMA_CADASTRO = "confirma_cadastro";
	public static final String EXIBE_SERVIOS_A_PARTOR_DE = "exibe_servicos_a_partir_de";
	public static final String EXIBE_SERVICOS_DO_DIA = "exibe_servidos_do_dia";
	public static final String PERMITE_DEVOLUCAO = "permite_devolucao";
	public static final String PAGAMENTO_MULTIEMPRESA = "pagamento_multiempresa";
	public static final String LIMITE_COMPRA_TRANSACAO = "limite_compra_transacao";
	
}
