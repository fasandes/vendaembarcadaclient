package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the parametro database table.
 * 
 */
@Entity
@NamedQuery(name = "ParametroEmbarcado.findAll", query = "SELECT p FROM ParametroEmbarcado p")
public class ParametroEmbarcado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name = "cobra_pedagio")
	private Boolean cobraPedagio;

	@Column(name = "cobra_outros")
	private Boolean cobraOutros;

	@Column(name = "padrao_seguro")
	private Integer padraoSeguro;

	@Column(name = "padrao_embarque")
	private Integer padraoEmbarque;

	public ParametroEmbarcado() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getCobraPedagio() {
		return cobraPedagio;
	}

	public void setCobraPedagio(Boolean cobraPedagio) {
		this.cobraPedagio = cobraPedagio;
	}

	public Boolean getCobraOutros() {
		return cobraOutros;
	}

	public void setCobraOutros(Boolean cobraOutros) {
		this.cobraOutros = cobraOutros;
	}

	public Integer getPadraoSeguro() {
		return padraoSeguro;
	}

	public void setPadraoSeguro(Integer padraoSeguro) {
		this.padraoSeguro = padraoSeguro;
	}

	public Integer getPadraoEmbarque() {
		return padraoEmbarque;
	}

	public void setPadraoEmbarque(Integer padraoEmbarque) {
		this.padraoEmbarque = padraoEmbarque;
	}
	
	

}
