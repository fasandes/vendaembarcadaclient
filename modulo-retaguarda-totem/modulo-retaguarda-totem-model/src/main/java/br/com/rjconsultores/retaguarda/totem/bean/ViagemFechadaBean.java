package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;
import java.util.Date;

public class ViagemFechadaBean {

    private Integer idViagem;
    private String codigoVeiculo;
    private Date dataViagem;
    private Date horaViagem;
    private String situacaoViagem;
    private Date dataFechamentoViagem;
    private Date horaFechamentoViagem;
    private Integer codigoHorario;
    private Date caixaDataAbertura;
    private Date caixaHoraAbertura;
    private Date caixaDataFechamento;
    private Date caixaHoraFechamento;
    private BigDecimal caixaValorTotalVendas;
    private BigDecimal caixaValorTotalComissoes;
    private BigDecimal caixaValorTotalAcertos;
    private BigDecimal caixaValorTotalTrocos;
    private BigDecimal caixaValorLiquidoCaixa;
    private String caixaEmpresa;
    private Integer caixaOperador;
    private Integer caixaNumeroVeiculo;
    private Integer caixaNumeroCatraca;
    private String horaHorario;
    private Character sentidoHorario;
    private Integer destinoHorario;
    private Integer origemHorario;
    private String codigoEmpresa;
    private Integer codigoLinha;
    private Integer caixaId;
    private String idDispositivo;
	public Integer getIdViagem() {
		return idViagem;
	}
	public void setIdViagem(Integer idViagem) {
		this.idViagem = idViagem;
	}
	public String getCodigoVeiculo() {
		return codigoVeiculo;
	}
	public void setCodigoVeiculo(String codigoVeiculo) {
		this.codigoVeiculo = codigoVeiculo;
	}
	public Date getDataViagem() {
		return dataViagem;
	}
	public void setDataViagem(Date dataViagem) {
		this.dataViagem = dataViagem;
	}
	public Date getHoraViagem() {
		return horaViagem;
	}
	public void setHoraViagem(Date horaViagem) {
		this.horaViagem = horaViagem;
	}
	public String getSituacaoViagem() {
		return situacaoViagem;
	}
	public void setSituacaoViagem(String situacaoViagem) {
		this.situacaoViagem = situacaoViagem;
	}
	public Date getDataFechamentoViagem() {
		return dataFechamentoViagem;
	}
	public void setDataFechamentoViagem(Date dataFechamentoViagem) {
		this.dataFechamentoViagem = dataFechamentoViagem;
	}
	public Date getHoraFechamentoViagem() {
		return horaFechamentoViagem;
	}
	public void setHoraFechamentoViagem(Date horaFechamentoViagem) {
		this.horaFechamentoViagem = horaFechamentoViagem;
	}
	public Integer getCodigoHorario() {
		return codigoHorario;
	}
	public void setCodigoHorario(Integer codigoHorario) {
		this.codigoHorario = codigoHorario;
	}
	public Date getCaixaDataAbertura() {
		return caixaDataAbertura;
	}
	public void setCaixaDataAbertura(Date caixaDataAbertura) {
		this.caixaDataAbertura = caixaDataAbertura;
	}
	public Date getCaixaHoraAbertura() {
		return caixaHoraAbertura;
	}
	public void setCaixaHoraAbertura(Date caixaHoraAbertura) {
		this.caixaHoraAbertura = caixaHoraAbertura;
	}
	public Date getCaixaDataFechamento() {
		return caixaDataFechamento;
	}
	public void setCaixaDataFechamento(Date caixaDataFechamento) {
		this.caixaDataFechamento = caixaDataFechamento;
	}
	public Date getCaixaHoraFechamento() {
		return caixaHoraFechamento;
	}
	public void setCaixaHoraFechamento(Date caixaHoraFechamento) {
		this.caixaHoraFechamento = caixaHoraFechamento;
	}
	public BigDecimal getCaixaValorTotalVendas() {
		return caixaValorTotalVendas;
	}
	public void setCaixaValorTotalVendas(BigDecimal caixaValorTotalVendas) {
		this.caixaValorTotalVendas = caixaValorTotalVendas;
	}
	public BigDecimal getCaixaValorTotalComissoes() {
		return caixaValorTotalComissoes;
	}
	public void setCaixaValorTotalComissoes(BigDecimal caixaValorTotalComissoes) {
		this.caixaValorTotalComissoes = caixaValorTotalComissoes;
	}
	public BigDecimal getCaixaValorTotalAcertos() {
		return caixaValorTotalAcertos;
	}
	public void setCaixaValorTotalAcertos(BigDecimal caixaValorTotalAcertos) {
		this.caixaValorTotalAcertos = caixaValorTotalAcertos;
	}
	public BigDecimal getCaixaValorTotalTrocos() {
		return caixaValorTotalTrocos;
	}
	public void setCaixaValorTotalTrocos(BigDecimal caixaValorTotalTrocos) {
		this.caixaValorTotalTrocos = caixaValorTotalTrocos;
	}
	public BigDecimal getCaixaValorLiquidoCaixa() {
		return caixaValorLiquidoCaixa;
	}
	public void setCaixaValorLiquidoCaixa(BigDecimal caixaValorLiquidoCaixa) {
		this.caixaValorLiquidoCaixa = caixaValorLiquidoCaixa;
	}
	public String getCaixaEmpresa() {
		return caixaEmpresa;
	}
	public void setCaixaEmpresa(String caixaEmpresa) {
		this.caixaEmpresa = caixaEmpresa;
	}
	public Integer getCaixaOperador() {
		return caixaOperador;
	}
	public void setCaixaOperador(Integer caixaOperador) {
		this.caixaOperador = caixaOperador;
	}
	public Integer getCaixaNumeroVeiculo() {
		return caixaNumeroVeiculo;
	}
	public void setCaixaNumeroVeiculo(Integer caixaNumeroVeiculo) {
		this.caixaNumeroVeiculo = caixaNumeroVeiculo;
	}
	public Integer getCaixaNumeroCatraca() {
		return caixaNumeroCatraca;
	}
	public void setCaixaNumeroCatraca(Integer caixaNumeroCatraca) {
		this.caixaNumeroCatraca = caixaNumeroCatraca;
	}
	public String getHoraHorario() {
		return horaHorario;
	}
	public void setHoraHorario(String horaHorario) {
		this.horaHorario = horaHorario;
	}
	public Character getSentidoHorario() {
		return sentidoHorario;
	}
	public void setSentidoHorario(Character sentidoHorario) {
		this.sentidoHorario = sentidoHorario;
	}
	public Integer getDestinoHorario() {
		return destinoHorario;
	}
	public void setDestinoHorario(Integer destinoHorario) {
		this.destinoHorario = destinoHorario;
	}
	public Integer getOrigemHorario() {
		return origemHorario;
	}
	public void setOrigemHorario(Integer origemHorario) {
		this.origemHorario = origemHorario;
	}
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	public Integer getCodigoLinha() {
		return codigoLinha;
	}
	public void setCodigoLinha(Integer codigoLinha) {
		this.codigoLinha = codigoLinha;
	}
	public Integer getCaixaId() {
		return caixaId;
	}
	public void setCaixaId(Integer caixaId) {
		this.caixaId = caixaId;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caixaDataAbertura == null) ? 0 : caixaDataAbertura.hashCode());
		result = prime * result + ((caixaDataFechamento == null) ? 0 : caixaDataFechamento.hashCode());
		result = prime * result + ((caixaEmpresa == null) ? 0 : caixaEmpresa.hashCode());
		result = prime * result + ((caixaHoraAbertura == null) ? 0 : caixaHoraAbertura.hashCode());
		result = prime * result + ((caixaHoraFechamento == null) ? 0 : caixaHoraFechamento.hashCode());
		result = prime * result + ((caixaId == null) ? 0 : caixaId.hashCode());
		result = prime * result + ((caixaNumeroCatraca == null) ? 0 : caixaNumeroCatraca.hashCode());
		result = prime * result + ((caixaNumeroVeiculo == null) ? 0 : caixaNumeroVeiculo.hashCode());
		result = prime * result + ((caixaOperador == null) ? 0 : caixaOperador.hashCode());
		result = prime * result + ((caixaValorLiquidoCaixa == null) ? 0 : caixaValorLiquidoCaixa.hashCode());
		result = prime * result + ((caixaValorTotalAcertos == null) ? 0 : caixaValorTotalAcertos.hashCode());
		result = prime * result + ((caixaValorTotalComissoes == null) ? 0 : caixaValorTotalComissoes.hashCode());
		result = prime * result + ((caixaValorTotalTrocos == null) ? 0 : caixaValorTotalTrocos.hashCode());
		result = prime * result + ((caixaValorTotalVendas == null) ? 0 : caixaValorTotalVendas.hashCode());
		result = prime * result + ((codigoEmpresa == null) ? 0 : codigoEmpresa.hashCode());
		result = prime * result + ((codigoHorario == null) ? 0 : codigoHorario.hashCode());
		result = prime * result + ((codigoLinha == null) ? 0 : codigoLinha.hashCode());
		result = prime * result + ((codigoVeiculo == null) ? 0 : codigoVeiculo.hashCode());
		result = prime * result + ((dataFechamentoViagem == null) ? 0 : dataFechamentoViagem.hashCode());
		result = prime * result + ((dataViagem == null) ? 0 : dataViagem.hashCode());
		result = prime * result + ((destinoHorario == null) ? 0 : destinoHorario.hashCode());
		result = prime * result + ((horaFechamentoViagem == null) ? 0 : horaFechamentoViagem.hashCode());
		result = prime * result + ((horaHorario == null) ? 0 : horaHorario.hashCode());
		result = prime * result + ((horaViagem == null) ? 0 : horaViagem.hashCode());
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		result = prime * result + ((idViagem == null) ? 0 : idViagem.hashCode());
		result = prime * result + ((origemHorario == null) ? 0 : origemHorario.hashCode());
		result = prime * result + ((sentidoHorario == null) ? 0 : sentidoHorario.hashCode());
		result = prime * result + ((situacaoViagem == null) ? 0 : situacaoViagem.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ViagemFechadaBean other = (ViagemFechadaBean) obj;
		if (caixaDataAbertura == null) {
			if (other.caixaDataAbertura != null)
				return false;
		} else if (!caixaDataAbertura.equals(other.caixaDataAbertura))
			return false;
		if (caixaDataFechamento == null) {
			if (other.caixaDataFechamento != null)
				return false;
		} else if (!caixaDataFechamento.equals(other.caixaDataFechamento))
			return false;
		if (caixaEmpresa == null) {
			if (other.caixaEmpresa != null)
				return false;
		} else if (!caixaEmpresa.equals(other.caixaEmpresa))
			return false;
		if (caixaHoraAbertura == null) {
			if (other.caixaHoraAbertura != null)
				return false;
		} else if (!caixaHoraAbertura.equals(other.caixaHoraAbertura))
			return false;
		if (caixaHoraFechamento == null) {
			if (other.caixaHoraFechamento != null)
				return false;
		} else if (!caixaHoraFechamento.equals(other.caixaHoraFechamento))
			return false;
		if (caixaId == null) {
			if (other.caixaId != null)
				return false;
		} else if (!caixaId.equals(other.caixaId))
			return false;
		if (caixaNumeroCatraca == null) {
			if (other.caixaNumeroCatraca != null)
				return false;
		} else if (!caixaNumeroCatraca.equals(other.caixaNumeroCatraca))
			return false;
		if (caixaNumeroVeiculo == null) {
			if (other.caixaNumeroVeiculo != null)
				return false;
		} else if (!caixaNumeroVeiculo.equals(other.caixaNumeroVeiculo))
			return false;
		if (caixaOperador == null) {
			if (other.caixaOperador != null)
				return false;
		} else if (!caixaOperador.equals(other.caixaOperador))
			return false;
		if (caixaValorLiquidoCaixa == null) {
			if (other.caixaValorLiquidoCaixa != null)
				return false;
		} else if (!caixaValorLiquidoCaixa.equals(other.caixaValorLiquidoCaixa))
			return false;
		if (caixaValorTotalAcertos == null) {
			if (other.caixaValorTotalAcertos != null)
				return false;
		} else if (!caixaValorTotalAcertos.equals(other.caixaValorTotalAcertos))
			return false;
		if (caixaValorTotalComissoes == null) {
			if (other.caixaValorTotalComissoes != null)
				return false;
		} else if (!caixaValorTotalComissoes.equals(other.caixaValorTotalComissoes))
			return false;
		if (caixaValorTotalTrocos == null) {
			if (other.caixaValorTotalTrocos != null)
				return false;
		} else if (!caixaValorTotalTrocos.equals(other.caixaValorTotalTrocos))
			return false;
		if (caixaValorTotalVendas == null) {
			if (other.caixaValorTotalVendas != null)
				return false;
		} else if (!caixaValorTotalVendas.equals(other.caixaValorTotalVendas))
			return false;
		if (codigoEmpresa == null) {
			if (other.codigoEmpresa != null)
				return false;
		} else if (!codigoEmpresa.equals(other.codigoEmpresa))
			return false;
		if (codigoHorario == null) {
			if (other.codigoHorario != null)
				return false;
		} else if (!codigoHorario.equals(other.codigoHorario))
			return false;
		if (codigoLinha == null) {
			if (other.codigoLinha != null)
				return false;
		} else if (!codigoLinha.equals(other.codigoLinha))
			return false;
		if (codigoVeiculo == null) {
			if (other.codigoVeiculo != null)
				return false;
		} else if (!codigoVeiculo.equals(other.codigoVeiculo))
			return false;
		if (dataFechamentoViagem == null) {
			if (other.dataFechamentoViagem != null)
				return false;
		} else if (!dataFechamentoViagem.equals(other.dataFechamentoViagem))
			return false;
		if (dataViagem == null) {
			if (other.dataViagem != null)
				return false;
		} else if (!dataViagem.equals(other.dataViagem))
			return false;
		if (destinoHorario == null) {
			if (other.destinoHorario != null)
				return false;
		} else if (!destinoHorario.equals(other.destinoHorario))
			return false;
		if (horaFechamentoViagem == null) {
			if (other.horaFechamentoViagem != null)
				return false;
		} else if (!horaFechamentoViagem.equals(other.horaFechamentoViagem))
			return false;
		if (horaHorario == null) {
			if (other.horaHorario != null)
				return false;
		} else if (!horaHorario.equals(other.horaHorario))
			return false;
		if (horaViagem == null) {
			if (other.horaViagem != null)
				return false;
		} else if (!horaViagem.equals(other.horaViagem))
			return false;
		if (idDispositivo == null) {
			if (other.idDispositivo != null)
				return false;
		} else if (!idDispositivo.equals(other.idDispositivo))
			return false;
		if (idViagem == null) {
			if (other.idViagem != null)
				return false;
		} else if (!idViagem.equals(other.idViagem))
			return false;
		if (origemHorario == null) {
			if (other.origemHorario != null)
				return false;
		} else if (!origemHorario.equals(other.origemHorario))
			return false;
		if (sentidoHorario == null) {
			if (other.sentidoHorario != null)
				return false;
		} else if (!sentidoHorario.equals(other.sentidoHorario))
			return false;
		if (situacaoViagem == null) {
			if (other.situacaoViagem != null)
				return false;
		} else if (!situacaoViagem.equals(other.situacaoViagem))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ViagemFechadaBean [idViagem=" + idViagem + ", codigoVeiculo=" + codigoVeiculo + ", dataViagem="
				+ dataViagem + ", horaViagem=" + horaViagem + ", situacaoViagem=" + situacaoViagem
				+ ", dataFechamentoViagem=" + dataFechamentoViagem + ", horaFechamentoViagem=" + horaFechamentoViagem
				+ ", codigoHorario=" + codigoHorario + ", caixaDataAbertura=" + caixaDataAbertura
				+ ", caixaHoraAbertura=" + caixaHoraAbertura + ", caixaDataFechamento=" + caixaDataFechamento
				+ ", caixaHoraFechamento=" + caixaHoraFechamento + ", caixaValorTotalVendas=" + caixaValorTotalVendas
				+ ", caixaValorTotalComissoes=" + caixaValorTotalComissoes + ", caixaValorTotalAcertos="
				+ caixaValorTotalAcertos + ", caixaValorTotalTrocos=" + caixaValorTotalTrocos
				+ ", caixaValorLiquidoCaixa=" + caixaValorLiquidoCaixa + ", caixaEmpresa=" + caixaEmpresa
				+ ", caixaOperador=" + caixaOperador + ", caixaNumeroVeiculo=" + caixaNumeroVeiculo
				+ ", caixaNumeroCatraca=" + caixaNumeroCatraca + ", horaHorario=" + horaHorario + ", sentidoHorario="
				+ sentidoHorario + ", destinoHorario=" + destinoHorario + ", origemHorario=" + origemHorario
				+ ", codigoEmpresa=" + codigoEmpresa + ", codigoLinha=" + codigoLinha + ", caixaId=" + caixaId
				+ ", idDispositivo=" + idDispositivo + "]";
	}
    
}
