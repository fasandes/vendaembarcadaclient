package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the classe database table.
 * 
 */
@Entity
@NamedQuery(name="Classe.findAll", query="SELECT c FROM Classe c")
public class Classe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="codigo_classe")
	private String codigoClasse;

	private String descricao;

//	//bi-directional many-to-one association to Servico
//	@OneToMany(mappedBy="classe")
//	private List<Servico> servicos;

	public Classe() {
	}

	public String getCodigoClasse() {
		return this.codigoClasse;
	}

	public void setCodigoClasse(String codigoClasse) {
		this.codigoClasse = codigoClasse;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

//	public List<Servico> getServicos() {
//		return this.servicos;
//	}
//
//	public void setServicos(List<Servico> servicos) {
//		this.servicos = servicos;
//	}

//	public Servico addServico(Servico servico) {
//		getServicos().add(servico);
//		servico.setClasse(this);
//
//		return servico;
//	}
//
//	public Servico removeServico(Servico servico) {
//		getServicos().remove(servico);
//		servico.setClasse(null);
//
//		return servico;
//	}

}