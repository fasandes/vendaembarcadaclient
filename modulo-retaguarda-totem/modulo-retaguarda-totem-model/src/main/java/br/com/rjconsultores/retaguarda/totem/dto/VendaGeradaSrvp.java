package br.com.rjconsultores.retaguarda.totem.dto;

import java.util.Date;

public class VendaGeradaSrvp {
	
	private Integer idVenda;
	private String idDispositivo;
	private Date dataImportacao;
	public Integer getIdVenda() {
		return idVenda;
	}
	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	public Date getDataImportacao() {
		return dataImportacao;
	}
	public void setDataImportacao(Date dataImportacao) {
		this.dataImportacao = dataImportacao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataImportacao == null) ? 0 : dataImportacao.hashCode());
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		result = prime * result + ((idVenda == null) ? 0 : idVenda.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaGeradaSrvp other = (VendaGeradaSrvp) obj;
		if (dataImportacao == null) {
			if (other.dataImportacao != null)
				return false;
		} else if (!dataImportacao.equals(other.dataImportacao))
			return false;
		if (idDispositivo == null) {
			if (other.idDispositivo != null)
				return false;
		} else if (!idDispositivo.equals(other.idDispositivo))
			return false;
		if (idVenda == null) {
			if (other.idVenda != null)
				return false;
		} else if (!idVenda.equals(other.idVenda))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VendaGeradaSrvp [idVenda=" + idVenda + ", idDispositivo=" + idDispositivo + ", dataImportacao="
				+ dataImportacao + "]";
	}
	
}
