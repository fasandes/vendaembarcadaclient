package br.com.rjconsultores.retaguarda.totem.bean;

import java.util.Date;

import com.rjconsultores.srvp.comm.enums.TipoDesconto;
import com.rjconsultores.srvp.comm.enums.TipoPessoa;
import com.rjconsultores.srvp.comm.enums.TipoVenda;
import com.rjconsultores.srvp.comm.returns.transporte.FormaPagamento;

/**
 * @author Fabricio
 *
 */
public class VendaSrvpBean {

	private String usuario;
	private String servico;
	private Integer agencia;
	private String codCliente;
	private String nome;
	private String tipoDocumento;
	private String numeroDocumento;
	private String localEmbarque;
	private Date dataNascimento;
	private String nacionalidade;
	private String paisResidencia;
	private String ocupacao;
	private String sexo;
	private String estadoCivil;
	private String email;
	private String tipoDocumento2;
	private String numeroDocumento2;
	private String codigoMotorista;
	private boolean possuiSeguroOpcional;
	private boolean possuiSeguro;
	private boolean possuiTaxa;
	private boolean precoIdaVolta;
	private boolean precoNominal;
	private Integer quantidadeIda;
	private Integer quantidadeVolta;
	private TipoDesconto tipoDesconto;
	private Double percentualDesconto;
	private Double preco;
	private Double precoResolucao3450;
	private Integer quantidadeParcelas;
	private Double juros;
	private TipoVenda tipoVenda;
	private String informacaoPagamento;
	private boolean pagoSitef;
	private Integer codigoOrigem;
	private Integer codigoDestino;

	
	private String codigo;
	private Double porcentagem;
	private Double valor;
	private Double percentualAumentoDesconto;
	private String entidade;
	private TipoPessoa pessoa;
	private Date data;

	private boolean remarcacao;
	private boolean impressaoPosterior;
	private boolean reservaPoltrona;
	private String vencimentoReserva;

	public VendaSrvpBean() {
		super();
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getServico() {
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public Integer getAgencia() {
		return agencia;
	}

	public void setAgencia(Integer agencia) {
		this.agencia = agencia;
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getLocalEmbarque() {
		return localEmbarque;
	}

	public void setLocalEmbarque(String localEmbarque) {
		this.localEmbarque = localEmbarque;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getPaisResidencia() {
		return paisResidencia;
	}

	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	public String getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipoDocumento2() {
		return tipoDocumento2;
	}

	public void setTipoDocumento2(String tipoDocumento2) {
		this.tipoDocumento2 = tipoDocumento2;
	}

	public String getNumeroDocumento2() {
		return numeroDocumento2;
	}

	public void setNumeroDocumento2(String numeroDocumento2) {
		this.numeroDocumento2 = numeroDocumento2;
	}

	public String getCodigoMotorista() {
		return codigoMotorista;
	}

	public void setCodigoMotorista(String codigoMotorista) {
		this.codigoMotorista = codigoMotorista;
	}

	public boolean isPossuiSeguroOpcional() {
		return possuiSeguroOpcional;
	}

	public void setPossuiSeguroOpcional(boolean possuiSeguroOpcional) {
		this.possuiSeguroOpcional = possuiSeguroOpcional;
	}

	public boolean isPossuiSeguro() {
		return possuiSeguro;
	}

	public void setPossuiSeguro(boolean possuiSeguro) {
		this.possuiSeguro = possuiSeguro;
	}

	public boolean isPossuiTaxa() {
		return possuiTaxa;
	}

	public void setPossuiTaxa(boolean possuiTaxa) {
		this.possuiTaxa = possuiTaxa;
	}

	public boolean isPrecoIdaVolta() {
		return precoIdaVolta;
	}

	public void setPrecoIdaVolta(boolean precoIdaVolta) {
		this.precoIdaVolta = precoIdaVolta;
	}

	public boolean isPrecoNominal() {
		return precoNominal;
	}

	public void setPrecoNominal(boolean precoNominal) {
		this.precoNominal = precoNominal;
	}

	public Integer getQuantidadeIda() {
		return quantidadeIda;
	}

	public void setQuantidadeIda(Integer quantidadeIda) {
		this.quantidadeIda = quantidadeIda;
	}

	public Integer getQuantidadeVolta() {
		return quantidadeVolta;
	}

	public void setQuantidadeVolta(Integer quantidadeVolta) {
		this.quantidadeVolta = quantidadeVolta;
	}

	public TipoDesconto getTipoDesconto() {
		return tipoDesconto;
	}

	public void setTipoDesconto(TipoDesconto tipoDesconto) {
		this.tipoDesconto = tipoDesconto;
	}

	public Double getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(Double percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public Double getPrecoResolucao3450() {
		return precoResolucao3450;
	}

	public void setPrecoResolucao3450(Double precoResolucao3450) {
		this.precoResolucao3450 = precoResolucao3450;
	}

	public Integer getQuantidadeParcelas() {
		return quantidadeParcelas;
	}

	public void setQuantidadeParcelas(Integer quantidadeParcelas) {
		this.quantidadeParcelas = quantidadeParcelas;
	}

	public Double getJuros() {
		return juros;
	}

	public void setJuros(Double juros) {
		this.juros = juros;
	}

	public TipoVenda getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(TipoVenda tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public String getInformacaoPagamento() {
		return informacaoPagamento;
	}

	public void setInformacaoPagamento(String informacaoPagamento) {
		this.informacaoPagamento = informacaoPagamento;
	}

	public boolean isPagoSitef() {
		return pagoSitef;
	}

	public void setPagoSitef(boolean pagoSitef) {
		this.pagoSitef = pagoSitef;
	}

	public String getCodigoFormaPagamento() {
		return codigo;
	}

	public void setCodigoFormaPagamento(String codigo) {
		this.codigo = codigo;
	}

	public Double getPorcentagemFormaPagamento() {
		return porcentagem;
	}

	public void setPorcentagem(Double porcentagem) {
		this.porcentagem = porcentagem;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getPercentualAumentoDesconto() {
		return percentualAumentoDesconto;
	}

	public void setPercentualAumentoDesconto(Double percentualAumentoDesconto) {
		this.percentualAumentoDesconto = percentualAumentoDesconto;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public TipoPessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(TipoPessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public boolean isRemarcacao() {
		return remarcacao;
	}

	public void setRemarcacao(boolean remarcacao) {
		this.remarcacao = remarcacao;
	}

	public boolean isImpressaoPosterior() {
		return impressaoPosterior;
	}

	public void setImpressaoPosterior(boolean impressaoPosterior) {
		this.impressaoPosterior = impressaoPosterior;
	}

	public boolean isReservaPoltrona() {
		return reservaPoltrona;
	}

	public void setReservaPoltrona(boolean reservaPoltrona) {
		this.reservaPoltrona = reservaPoltrona;
	}

	public String getVencimentoReserva() {
		return vencimentoReserva;
	}

	public void setVencimentoReserva(String vencimentoReserva) {
		this.vencimentoReserva = vencimentoReserva;
	}

	public Integer getCodigoOrigem() {
		return codigoOrigem;
	}

	public void setCodigoOrigem(Integer codigoOrigem) {
		this.codigoOrigem = codigoOrigem;
	}

	public Integer getCodigoDestino() {
		return codigoDestino;
	}

	public void setCodigoDestino(Integer codigoDestino) {
		this.codigoDestino = codigoDestino;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Double getPorcentagem() {
		return porcentagem;
	}
	
	

	
}
