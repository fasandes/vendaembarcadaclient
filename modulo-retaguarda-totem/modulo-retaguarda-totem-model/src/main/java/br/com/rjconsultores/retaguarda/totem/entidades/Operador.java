package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the operador database table.
 * 
 */
@Entity
@NamedQuery(name="Operador.findAll", query="SELECT o FROM Operador o")
public class Operador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="operador_id")
	private Integer operadorId;

	@Column(name="data_alteracao")
	private Timestamp dataAlteracao;

	@Column(name="operador_agencia")
	private String operadorAgencia;

	@Column(name="operador_ativo")
	private Boolean operadorAtivo;

	@Column(name="operador_login")
	private String operadorLogin;

	@Column(name="operador_senha")
	private String operadorSenha;

	@Column(name="operador_tipo")
	private Integer operadorTipo;
	
	@ManyToMany(mappedBy="listaOperadores")
	private List<Servico> listaServicos;


	public Operador() {
	}

	public int getOperadorId() {
		return this.operadorId;
	}

	public void setOperadorId(int operadorId) {
		this.operadorId = operadorId;
	}

	public Timestamp getDataAlteracao() {
		return this.dataAlteracao;
	}

	public void setDataAlteracao(Timestamp dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	public String getOperadorAgencia() {
		return this.operadorAgencia;
	}

	public void setOperadorAgencia(String operadorAgencia) {
		this.operadorAgencia = operadorAgencia;
	}

	public Boolean getOperadorAtivo() {
		return this.operadorAtivo;
	}

	public void setOperadorAtivo(Boolean operadorAtivo) {
		this.operadorAtivo = operadorAtivo;
	}

	public String getOperadorLogin() {
		return this.operadorLogin;
	}

	public void setOperadorLogin(String operadorLogin) {
		this.operadorLogin = operadorLogin;
	}

	public String getOperadorSenha() {
		return this.operadorSenha;
	}

	public void setOperadorSenha(String operadorSenha) {
		this.operadorSenha = operadorSenha;
	}

	public Integer getOperadorTipo() {
		return this.operadorTipo;
	}

	public void setOperadorTipo(Integer operadorTipo) {
		this.operadorTipo = operadorTipo;
	}

}