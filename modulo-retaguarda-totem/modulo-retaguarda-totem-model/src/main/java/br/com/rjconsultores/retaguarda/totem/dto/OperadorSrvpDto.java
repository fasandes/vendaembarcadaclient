package br.com.rjconsultores.retaguarda.totem.dto;

public class OperadorSrvpDto {
	private Integer matricula;
    private String nome;
    private String senha;
    private Boolean ativo;
    private Integer tipoOperador;
	private String novaSenha;
	private String confirmaNovaSenha;
	private String idDispositivo;
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Integer getTipoOperador() {
		return tipoOperador;
	}
	public void setTipoOperador(Integer tipoOperador) {
		this.tipoOperador = tipoOperador;
	}
	public String getNovaSenha() {
		return novaSenha;
	}
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	public String getConfirmaNovaSenha() {
		return confirmaNovaSenha;
	}
	public void setConfirmaNovaSenha(String confirmaNovaSenha) {
		this.confirmaNovaSenha = confirmaNovaSenha;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result + ((confirmaNovaSenha == null) ? 0 : confirmaNovaSenha.hashCode());
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((novaSenha == null) ? 0 : novaSenha.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((tipoOperador == null) ? 0 : tipoOperador.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperadorSrvpDto other = (OperadorSrvpDto) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (confirmaNovaSenha == null) {
			if (other.confirmaNovaSenha != null)
				return false;
		} else if (!confirmaNovaSenha.equals(other.confirmaNovaSenha))
			return false;
		if (idDispositivo == null) {
			if (other.idDispositivo != null)
				return false;
		} else if (!idDispositivo.equals(other.idDispositivo))
			return false;
		if (matricula == null) {
			if (other.matricula != null)
				return false;
		} else if (!matricula.equals(other.matricula))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (novaSenha == null) {
			if (other.novaSenha != null)
				return false;
		} else if (!novaSenha.equals(other.novaSenha))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (tipoOperador == null) {
			if (other.tipoOperador != null)
				return false;
		} else if (!tipoOperador.equals(other.tipoOperador))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "OperadorSrvpDto [matricula=" + matricula + ", nome=" + nome + ", senha=" + senha + ", ativo=" + ativo
				+ ", tipoOperador=" + tipoOperador + ", novaSenha=" + novaSenha + ", confirmaNovaSenha="
				+ confirmaNovaSenha + ", idDispositivo=" + idDispositivo + "]";
	}
	
}
