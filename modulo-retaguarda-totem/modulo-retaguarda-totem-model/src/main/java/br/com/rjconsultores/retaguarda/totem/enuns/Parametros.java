package br.com.rjconsultores.retaguarda.totem.enuns;

public enum Parametros {
	LIB_SRVP("lib_srvp"), IP_SRVP("ip_srvp"), PRECO_SRVP("preco_srvp"), AGENCIA_SRVP("agencia_srvp"), USUARIO_SRVP(
			"usuario_srvp"), SENHA_SRVP("senha_srvp"),TABELA_VIGENTE("tabela_vigente");

	private String chave;

	Parametros(String chave) {
		this.chave = chave;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

}
