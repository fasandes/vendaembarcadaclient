package br.com.rjconsultores.retaguarda.totem.entidades;

public enum PadroesParametroEmbarcado {
	OBRIGATORIO(3), OPCIONAL_DESMARCADO(2), OPCIONAL_MARCADO(1), REMOVIDO(0);

	private Integer valor;

	PadroesParametroEmbarcado(Integer valor) {
		this.valor = valor;
	}

	public Integer getValor() {
		return valor;
	}

}
