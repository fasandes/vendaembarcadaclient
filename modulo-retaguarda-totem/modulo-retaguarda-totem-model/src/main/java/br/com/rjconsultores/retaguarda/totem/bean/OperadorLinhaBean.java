package br.com.rjconsultores.retaguarda.totem.bean;

import java.util.Date;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;

public class OperadorLinhaBean {
	
	private Operador operador;
    private Integer codigoHorario;
    private String horaHorario;
    private Character sentidoHorario;
    private Localidade destinoHorario;
    private Localidade origemHorario;
    private Empresa codigoEmpresa;
    private Linha codigoLinha;
    private Date dataCriacao;
	public Operador getOperador() {
		return operador;
	}
	public void setOperador(Operador operador) {
		this.operador = operador;
	}
	public Integer getCodigoHorario() {
		return codigoHorario;
	}
	public void setCodigoHorario(Integer codigoHorario) {
		this.codigoHorario = codigoHorario;
	}
	public String getHoraHorario() {
		return horaHorario;
	}
	public void setHoraHorario(String horaHorario) {
		this.horaHorario = horaHorario;
	}
	public Character getSentidoHorario() {
		return sentidoHorario;
	}
	public void setSentidoHorario(Character sentidoHorario) {
		this.sentidoHorario = sentidoHorario;
	}
	public Localidade getDestinoHorario() {
		return destinoHorario;
	}
	public void setDestinoHorario(Localidade destinoHorario) {
		this.destinoHorario = destinoHorario;
	}
	public Localidade getOrigemHorario() {
		return origemHorario;
	}
	public void setOrigemHorario(Localidade origemHorario) {
		this.origemHorario = origemHorario;
	}
	public Empresa getCodigoEmpresa() {
		return codigoEmpresa;
	}
	public void setCodigoEmpresa(Empresa codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	public Linha getCodigoLinha() {
		return codigoLinha;
	}
	public void setCodigoLinha(Linha codigoLinha) {
		this.codigoLinha = codigoLinha;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoEmpresa == null) ? 0 : codigoEmpresa.hashCode());
		result = prime * result + ((codigoHorario == null) ? 0 : codigoHorario.hashCode());
		result = prime * result + ((codigoLinha == null) ? 0 : codigoLinha.hashCode());
		result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result + ((destinoHorario == null) ? 0 : destinoHorario.hashCode());
		result = prime * result + ((horaHorario == null) ? 0 : horaHorario.hashCode());
		result = prime * result + ((operador == null) ? 0 : operador.hashCode());
		result = prime * result + ((origemHorario == null) ? 0 : origemHorario.hashCode());
		result = prime * result + ((sentidoHorario == null) ? 0 : sentidoHorario.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperadorLinhaBean other = (OperadorLinhaBean) obj;
		if (codigoEmpresa == null) {
			if (other.codigoEmpresa != null)
				return false;
		} else if (!codigoEmpresa.equals(other.codigoEmpresa))
			return false;
		if (codigoHorario == null) {
			if (other.codigoHorario != null)
				return false;
		} else if (!codigoHorario.equals(other.codigoHorario))
			return false;
		if (codigoLinha == null) {
			if (other.codigoLinha != null)
				return false;
		} else if (!codigoLinha.equals(other.codigoLinha))
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (destinoHorario == null) {
			if (other.destinoHorario != null)
				return false;
		} else if (!destinoHorario.equals(other.destinoHorario))
			return false;
		if (horaHorario == null) {
			if (other.horaHorario != null)
				return false;
		} else if (!horaHorario.equals(other.horaHorario))
			return false;
		if (operador == null) {
			if (other.operador != null)
				return false;
		} else if (!operador.equals(other.operador))
			return false;
		if (origemHorario == null) {
			if (other.origemHorario != null)
				return false;
		} else if (!origemHorario.equals(other.origemHorario))
			return false;
		if (sentidoHorario == null) {
			if (other.sentidoHorario != null)
				return false;
		} else if (!sentidoHorario.equals(other.sentidoHorario))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperadorLinhaBean [operador=");
		builder.append(operador);
		builder.append(", codigoHorario=");
		builder.append(codigoHorario);
		builder.append(", horaHorario=");
		builder.append(horaHorario);
		builder.append(", sentidoHorario=");
		builder.append(sentidoHorario);
		builder.append(", destinoHorario=");
		builder.append(destinoHorario);
		builder.append(", origemHorario=");
		builder.append(origemHorario);
		builder.append(", codigoEmpresa=");
		builder.append(codigoEmpresa);
		builder.append(", codigoLinha=");
		builder.append(codigoLinha);
		builder.append(", dataCriacao=");
		builder.append(dataCriacao);
		builder.append("]");
		return builder.toString();
	}
    
}
