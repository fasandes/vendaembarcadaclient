package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the servico database table.
 * 
 */
@Entity
@NamedQuery(name = "Servico.findAll", query = "SELECT s FROM Servico s")
public class Servico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer codigo;

	@Column(name = "horario_saida")
	private String horarioSaida;

	@ManyToOne
	@JoinColumn(name = "classe_codigo")
	private Classe classe;

	@ManyToOne
	@JoinColumn(name = "empresa_codigo")
	private Empresa empresa;

	@ManyToOne
	@JoinColumn(name = "linha_codigo")
	private Linha linha;

	@ManyToOne
	@JoinColumn(name = "origem_id")
	private Localidade origem;

	@ManyToOne
	@JoinColumn(name = "destino_id")
	private Localidade destino;

	@ManyToMany
	@JoinTable(name = "operador_has_servico", joinColumns = {
			@JoinColumn(name = "servico_codigo") }, inverseJoinColumns = { @JoinColumn(name = "operador_id") })
	private List<Operador> listaOperadores;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getHorarioSaida() {
		return horarioSaida;
	}

	public void setHorarioSaida(String horarioSaida) {
		this.horarioSaida = horarioSaida;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Linha getLinha() {
		return linha;
	}

	public void setLinha(Linha linha) {
		this.linha = linha;
	}

	public Localidade getOrigem() {
		return origem;
	}

	public void setOrigem(Localidade origem) {
		this.origem = origem;
	}

	public Localidade getDestino() {
		return destino;
	}

	public void setDestino(Localidade destino) {
		this.destino = destino;
	}

}