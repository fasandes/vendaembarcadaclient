package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the linha database table.
 * 
 */
@Entity
@NamedQuery(name = "Linha.findAll", query = "SELECT l FROM Linha l")
public class Linha implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo_linha")
	private Integer codigoLinha;

	@ManyToOne
	@JoinColumn(name = "empresa_codigo")
	private Empresa empresa;

	private String descricao;

	private String prefixo;

	public Linha() {
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPrefixo() {
		return this.prefixo;
	}

	public void setPrefixo(String prefixo) {
		this.prefixo = prefixo;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public void setCodigoLinha(Integer codigoLinha) {
		this.codigoLinha = codigoLinha;
	}

	public Integer getCodigoLinha() {
		return codigoLinha;
	}

}