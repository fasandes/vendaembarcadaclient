package br.com.rjconsultores.retaguarda.totem.dto;

import java.math.BigDecimal;

import br.com.rjconsultores.retaguarda.totem.bean.VendaFechadaBean;

public class VendaFechadaDto {

	private VendaFechadaBean vendaFechadaBean;
	private ParametrosGeracaoVenda parametrosGeracaoVenda;
	private String enderecoWsRetornoVenda;
	private String codigoTipoPagamento;
	private BigDecimal percentualDescontoTipoPagamento;

	public VendaFechadaDto() {
		super();
	}

	public VendaFechadaDto(VendaFechadaBean vendaFechadaBean, ParametrosGeracaoVenda parametrosGeracaoVenda,
			String enderecoWsRetornoVenda, String codigoTipoPagamento, BigDecimal percentualDescontoTipoPagamento) {
		super();
		this.vendaFechadaBean = vendaFechadaBean;
		this.parametrosGeracaoVenda = parametrosGeracaoVenda;
		this.enderecoWsRetornoVenda = enderecoWsRetornoVenda;
		this.codigoTipoPagamento = codigoTipoPagamento;
		this.percentualDescontoTipoPagamento = percentualDescontoTipoPagamento;
	}
	

	public VendaFechadaBean getVendaFechadaBean() {
		return vendaFechadaBean;
	}

	public void setVendaFechadaBean(VendaFechadaBean vendaFechadaBean) {
		this.vendaFechadaBean = vendaFechadaBean;
	}

	public ParametrosGeracaoVenda getParametrosGeracaoVenda() {
		return parametrosGeracaoVenda;
	}

	public void setParametrosGeracaoVenda(ParametrosGeracaoVenda parametrosGeracaoVenda) {
		this.parametrosGeracaoVenda = parametrosGeracaoVenda;
	}

	public String getEnderecoWsRetornoVenda() {
		return enderecoWsRetornoVenda;
	}

	public void setEnderecoWsRetornoVenda(String enderecoWsRetornoVenda) {
		this.enderecoWsRetornoVenda = enderecoWsRetornoVenda;
	}

	public String getCodigoTipoPagamento() {
		return codigoTipoPagamento;
	}

	public void setCodigoTipoPagamento(String codigoTipoPagamento) {
		this.codigoTipoPagamento = codigoTipoPagamento;
	}

	public BigDecimal getPercentualDescontoTipoPagamento() {
		return percentualDescontoTipoPagamento;
	}

	public void setPercentualDescontoTipoPagamento(BigDecimal percentualDescontoTipoPagamento) {
		this.percentualDescontoTipoPagamento = percentualDescontoTipoPagamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoTipoPagamento == null) ? 0 : codigoTipoPagamento.hashCode());
		result = prime * result + ((enderecoWsRetornoVenda == null) ? 0 : enderecoWsRetornoVenda.hashCode());
		result = prime * result + ((parametrosGeracaoVenda == null) ? 0 : parametrosGeracaoVenda.hashCode());
		result = prime * result
				+ ((percentualDescontoTipoPagamento == null) ? 0 : percentualDescontoTipoPagamento.hashCode());
		result = prime * result + ((vendaFechadaBean == null) ? 0 : vendaFechadaBean.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaFechadaDto other = (VendaFechadaDto) obj;
		if (codigoTipoPagamento == null) {
			if (other.codigoTipoPagamento != null)
				return false;
		} else if (!codigoTipoPagamento.equals(other.codigoTipoPagamento))
			return false;
		if (enderecoWsRetornoVenda == null) {
			if (other.enderecoWsRetornoVenda != null)
				return false;
		} else if (!enderecoWsRetornoVenda.equals(other.enderecoWsRetornoVenda))
			return false;
		if (parametrosGeracaoVenda == null) {
			if (other.parametrosGeracaoVenda != null)
				return false;
		} else if (!parametrosGeracaoVenda.equals(other.parametrosGeracaoVenda))
			return false;
		if (percentualDescontoTipoPagamento == null) {
			if (other.percentualDescontoTipoPagamento != null)
				return false;
		} else if (!percentualDescontoTipoPagamento.equals(other.percentualDescontoTipoPagamento))
			return false;
		if (vendaFechadaBean == null) {
			if (other.vendaFechadaBean != null)
				return false;
		} else if (!vendaFechadaBean.equals(other.vendaFechadaBean))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VendaFechadaDto [vendaFechadaBean=" + vendaFechadaBean + ", parametrosGeracaoVenda="
				+ parametrosGeracaoVenda + ", enderecoWsRetornoVenda=" + enderecoWsRetornoVenda
				+ ", codigoTipoPagamento=" + codigoTipoPagamento + ", percentualDescontoTipoPagamento="
				+ percentualDescontoTipoPagamento + "]";
	}

}
