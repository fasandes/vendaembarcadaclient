package br.com.rjconsultores.retaguarda.totem.dto;

public class VendaGeradaSrvpDto {

	private VendaGeradaSrvp vendaGeradaSrvp;

	public VendaGeradaSrvp getVendaGeradaSrvp() {
		return vendaGeradaSrvp;
	}

	public void setVendaGeradaSrvp(VendaGeradaSrvp vendaGeradaSrvp) {
		this.vendaGeradaSrvp = vendaGeradaSrvp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vendaGeradaSrvp == null) ? 0 : vendaGeradaSrvp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaGeradaSrvpDto other = (VendaGeradaSrvpDto) obj;
		if (vendaGeradaSrvp == null) {
			if (other.vendaGeradaSrvp != null)
				return false;
		} else if (!vendaGeradaSrvp.equals(other.vendaGeradaSrvp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VendaGeradaSrvpDto [vendaGeradaSrvp=" + vendaGeradaSrvp + "]";
	}
	
}
