package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;

import br.com.rjconsultores.retaguarda.totem.entidades.Classe;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

public class OperacaoBean {
	
    private BigDecimal valorTarifa;
    private BigDecimal valorTaxa;
    private BigDecimal valorSeguro;
    private BigDecimal valorPedagio;
    private BigDecimal valorOutros;
    private String codigoPais;
    private BigDecimal distanciaTrecho;
    private Classe classe;
    private Empresa empresa;
    private Localidade destino;
    private Localidade origem;
    private Linha linha;
    private Boolean permiteVenda;
    
	public BigDecimal getValorTarifa() {
		return valorTarifa;
	}
	public void setValorTarifa(BigDecimal valorTarifa) {
		this.valorTarifa = valorTarifa;
	}
	public BigDecimal getValorTaxa() {
		return valorTaxa;
	}
	public void setValorTaxa(BigDecimal valorTaxa) {
		this.valorTaxa = valorTaxa;
	}
	public BigDecimal getValorSeguro() {
		return valorSeguro;
	}
	public void setValorSeguro(BigDecimal valorSeguro) {
		this.valorSeguro = valorSeguro;
	}
	public BigDecimal getValorPedagio() {
		return valorPedagio;
	}
	public void setValorPedagio(BigDecimal valorPedagio) {
		this.valorPedagio = valorPedagio;
	}
	public BigDecimal getValorOutros() {
		return valorOutros;
	}
	public void setValorOutros(BigDecimal valorOutros) {
		this.valorOutros = valorOutros;
	}
	public String getCodigoPais() {
		return codigoPais;
	}
	public void setCodigoPais(String codigoPais) {
		this.codigoPais = codigoPais;
	}
	public BigDecimal getDistanciaTrecho() {
		return distanciaTrecho;
	}
	public void setDistanciaTrecho(BigDecimal distanciaTrecho) {
		this.distanciaTrecho = distanciaTrecho;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Localidade getDestino() {
		return destino;
	}
	public void setDestino(Localidade destino) {
		this.destino = destino;
	}
	public Localidade getOrigem() {
		return origem;
	}
	public void setOrigem(Localidade origem) {
		this.origem = origem;
	}
	public Linha getLinha() {
		return linha;
	}
	public void setLinha(Linha linha) {
		this.linha = linha;
	}
	
	public Boolean getPermiteVenda() {
		return permiteVenda;
	}
	public void setPermiteVenda(Boolean permiteVenda) {
		this.permiteVenda = permiteVenda;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classe == null) ? 0 : classe.hashCode());
		result = prime * result + ((codigoPais == null) ? 0 : codigoPais.hashCode());
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + ((distanciaTrecho == null) ? 0 : distanciaTrecho.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((linha == null) ? 0 : linha.hashCode());
		result = prime * result + ((origem == null) ? 0 : origem.hashCode());
		result = prime * result + ((permiteVenda == null) ? 0 : permiteVenda.hashCode());
		result = prime * result + ((valorOutros == null) ? 0 : valorOutros.hashCode());
		result = prime * result + ((valorPedagio == null) ? 0 : valorPedagio.hashCode());
		result = prime * result + ((valorSeguro == null) ? 0 : valorSeguro.hashCode());
		result = prime * result + ((valorTarifa == null) ? 0 : valorTarifa.hashCode());
		result = prime * result + ((valorTaxa == null) ? 0 : valorTaxa.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperacaoBean other = (OperacaoBean) obj;
		if (classe == null) {
			if (other.classe != null)
				return false;
		} else if (!classe.equals(other.classe))
			return false;
		if (codigoPais == null) {
			if (other.codigoPais != null)
				return false;
		} else if (!codigoPais.equals(other.codigoPais))
			return false;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (distanciaTrecho == null) {
			if (other.distanciaTrecho != null)
				return false;
		} else if (!distanciaTrecho.equals(other.distanciaTrecho))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (linha == null) {
			if (other.linha != null)
				return false;
		} else if (!linha.equals(other.linha))
			return false;
		if (origem == null) {
			if (other.origem != null)
				return false;
		} else if (!origem.equals(other.origem))
			return false;
		if (permiteVenda == null) {
			if (other.permiteVenda != null)
				return false;
		} else if (!permiteVenda.equals(other.permiteVenda))
			return false;
		if (valorOutros == null) {
			if (other.valorOutros != null)
				return false;
		} else if (!valorOutros.equals(other.valorOutros))
			return false;
		if (valorPedagio == null) {
			if (other.valorPedagio != null)
				return false;
		} else if (!valorPedagio.equals(other.valorPedagio))
			return false;
		if (valorSeguro == null) {
			if (other.valorSeguro != null)
				return false;
		} else if (!valorSeguro.equals(other.valorSeguro))
			return false;
		if (valorTarifa == null) {
			if (other.valorTarifa != null)
				return false;
		} else if (!valorTarifa.equals(other.valorTarifa))
			return false;
		if (valorTaxa == null) {
			if (other.valorTaxa != null)
				return false;
		} else if (!valorTaxa.equals(other.valorTaxa))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "OperacaoBean [valorTarifa=" + valorTarifa + ", valorTaxa=" + valorTaxa + ", valorSeguro=" + valorSeguro
				+ ", valorPedagio=" + valorPedagio + ", valorOutros=" + valorOutros + ", codigoPais=" + codigoPais
				+ ", distanciaTrecho=" + distanciaTrecho + ", classe=" + classe + ", empresa=" + empresa + ", destino="
				+ destino + ", origem=" + origem + ", linha=" + linha + ", permiteVenda=" + permiteVenda + "]";
	}
	
}
