package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the preco database table.
 * 
 */
@Entity
@NamedQuery(name="Preco.findAll", query="SELECT p FROM Preco p")
public class Preco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="valor_outros")
	private BigDecimal valorOutros;

	@Column(name="valor_pedagio")
	private BigDecimal valorPedagio;

	@Column(name="valor_seguro")
	private BigDecimal valorSeguro;

	@Column(name="valor_tarifa")
	private BigDecimal valorTarifa;

	@Column(name="valor_taxa")
	private BigDecimal valorTaxa;

	//bi-directional many-to-one association to Trecho
	@ManyToOne
	private Trecho trecho;

	public Preco() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValorOutros() {
		return this.valorOutros;
	}

	public void setValorOutros(BigDecimal valorOutros) {
		this.valorOutros = valorOutros;
	}

	public BigDecimal getValorPedagio() {
		return this.valorPedagio;
	}

	public void setValorPedagio(BigDecimal valorPedagio) {
		this.valorPedagio = valorPedagio;
	}

	public BigDecimal getValorSeguro() {
		return this.valorSeguro;
	}

	public void setValorSeguro(BigDecimal valorSeguro) {
		this.valorSeguro = valorSeguro;
	}

	public BigDecimal getValorTarifa() {
		return this.valorTarifa;
	}

	public void setValorTarifa(BigDecimal valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	public BigDecimal getValorTaxa() {
		return this.valorTaxa;
	}

	public void setValorTaxa(BigDecimal valorTaxa) {
		this.valorTaxa = valorTaxa;
	}

	public Trecho getTrecho() {
		return this.trecho;
	}

	public void setTrecho(Trecho trecho) {
		this.trecho = trecho;
	}

}