package br.com.rjconsultores.retaguarda.totem.bean;

public class SuperLinhaSequenciaBean {

	private Integer sequencia;
	private HorarioBean horario;
	private SuperLinhaBean superLinha;

	public SuperLinhaSequenciaBean() {
		super();
	}

	public Integer getSequencia() {
		return sequencia;
	}

	public void setSequencia(Integer sequencia) {
		this.sequencia = sequencia;
	}

	public HorarioBean getHorario() {
		return horario;
	}

	public void setHorario(HorarioBean horario) {
		this.horario = horario;
	}

	public SuperLinhaBean getSuperLinha() {
		return superLinha;
	}

	public void setSuperLinha(SuperLinhaBean superLinha) {
		this.superLinha = superLinha;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime * result + ((sequencia == null) ? 0 : sequencia.hashCode());
		result = prime * result + ((superLinha == null) ? 0 : superLinha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SuperLinhaSequenciaBean other = (SuperLinhaSequenciaBean) obj;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (sequencia == null) {
			if (other.sequencia != null)
				return false;
		} else if (!sequencia.equals(other.sequencia))
			return false;
		if (superLinha == null) {
			if (other.superLinha != null)
				return false;
		} else if (!superLinha.equals(other.superLinha))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SuperLinhaSequenciaBean [sequencia=" + sequencia + ", horario=" + horario + ", superLinha=" + superLinha
				+ "]";
	}

}
