package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;
import java.util.Date;

public class CaixaFechadoBean {

	private Integer caixaId;
	private Integer codigoSessao;
	private Date dataAbertura;
	private Date horaAbertura;
	private Date dataFechamento;
	private Date horaFechamento;
	private BigDecimal valorTotalVendas;
	private BigDecimal valorTotalComissoes;
	private BigDecimal valorTotalAcertos;
	private BigDecimal valorTotalTrocos;
	private BigDecimal valorLiquidoCaixa;
	private String empresa;
	private Integer operador;
	private Integer numeroVeiculo;
	private Integer numeroCatraca;
	private String idDispositivo;
	public Integer getCaixaId() {
		return caixaId;
	}
	public void setCaixaId(Integer caixaId) {
		this.caixaId = caixaId;
	}
	public Integer getCodigoSessao() {
		return codigoSessao;
	}
	public void setCodigoSessao(Integer codigoSessao) {
		this.codigoSessao = codigoSessao;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getHoraAbertura() {
		return horaAbertura;
	}
	public void setHoraAbertura(Date horaAbertura) {
		this.horaAbertura = horaAbertura;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Date getHoraFechamento() {
		return horaFechamento;
	}
	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}
	public BigDecimal getValorTotalVendas() {
		return valorTotalVendas;
	}
	public void setValorTotalVendas(BigDecimal valorTotalVendas) {
		this.valorTotalVendas = valorTotalVendas;
	}
	public BigDecimal getValorTotalComissoes() {
		return valorTotalComissoes;
	}
	public void setValorTotalComissoes(BigDecimal valorTotalComissoes) {
		this.valorTotalComissoes = valorTotalComissoes;
	}
	public BigDecimal getValorTotalAcertos() {
		return valorTotalAcertos;
	}
	public void setValorTotalAcertos(BigDecimal valorTotalAcertos) {
		this.valorTotalAcertos = valorTotalAcertos;
	}
	public BigDecimal getValorTotalTrocos() {
		return valorTotalTrocos;
	}
	public void setValorTotalTrocos(BigDecimal valorTotalTrocos) {
		this.valorTotalTrocos = valorTotalTrocos;
	}
	public BigDecimal getValorLiquidoCaixa() {
		return valorLiquidoCaixa;
	}
	public void setValorLiquidoCaixa(BigDecimal valorLiquidoCaixa) {
		this.valorLiquidoCaixa = valorLiquidoCaixa;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public Integer getOperador() {
		return operador;
	}
	public void setOperador(Integer operador) {
		this.operador = operador;
	}
	public Integer getNumeroVeiculo() {
		return numeroVeiculo;
	}
	public void setNumeroVeiculo(Integer numeroVeiculo) {
		this.numeroVeiculo = numeroVeiculo;
	}
	public Integer getNumeroCatraca() {
		return numeroCatraca;
	}
	public void setNumeroCatraca(Integer numeroCatraca) {
		this.numeroCatraca = numeroCatraca;
	}
	public String getIdDispositivo() {
		return idDispositivo;
	}
	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caixaId == null) ? 0 : caixaId.hashCode());
		result = prime * result + ((codigoSessao == null) ? 0 : codigoSessao.hashCode());
		result = prime * result + ((dataAbertura == null) ? 0 : dataAbertura.hashCode());
		result = prime * result + ((dataFechamento == null) ? 0 : dataFechamento.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((horaAbertura == null) ? 0 : horaAbertura.hashCode());
		result = prime * result + ((horaFechamento == null) ? 0 : horaFechamento.hashCode());
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		result = prime * result + ((numeroCatraca == null) ? 0 : numeroCatraca.hashCode());
		result = prime * result + ((numeroVeiculo == null) ? 0 : numeroVeiculo.hashCode());
		result = prime * result + ((operador == null) ? 0 : operador.hashCode());
		result = prime * result + ((valorLiquidoCaixa == null) ? 0 : valorLiquidoCaixa.hashCode());
		result = prime * result + ((valorTotalAcertos == null) ? 0 : valorTotalAcertos.hashCode());
		result = prime * result + ((valorTotalComissoes == null) ? 0 : valorTotalComissoes.hashCode());
		result = prime * result + ((valorTotalTrocos == null) ? 0 : valorTotalTrocos.hashCode());
		result = prime * result + ((valorTotalVendas == null) ? 0 : valorTotalVendas.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CaixaFechadoBean other = (CaixaFechadoBean) obj;
		if (caixaId == null) {
			if (other.caixaId != null)
				return false;
		} else if (!caixaId.equals(other.caixaId))
			return false;
		if (codigoSessao == null) {
			if (other.codigoSessao != null)
				return false;
		} else if (!codigoSessao.equals(other.codigoSessao))
			return false;
		if (dataAbertura == null) {
			if (other.dataAbertura != null)
				return false;
		} else if (!dataAbertura.equals(other.dataAbertura))
			return false;
		if (dataFechamento == null) {
			if (other.dataFechamento != null)
				return false;
		} else if (!dataFechamento.equals(other.dataFechamento))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (horaAbertura == null) {
			if (other.horaAbertura != null)
				return false;
		} else if (!horaAbertura.equals(other.horaAbertura))
			return false;
		if (horaFechamento == null) {
			if (other.horaFechamento != null)
				return false;
		} else if (!horaFechamento.equals(other.horaFechamento))
			return false;
		if (idDispositivo == null) {
			if (other.idDispositivo != null)
				return false;
		} else if (!idDispositivo.equals(other.idDispositivo))
			return false;
		if (numeroCatraca == null) {
			if (other.numeroCatraca != null)
				return false;
		} else if (!numeroCatraca.equals(other.numeroCatraca))
			return false;
		if (numeroVeiculo == null) {
			if (other.numeroVeiculo != null)
				return false;
		} else if (!numeroVeiculo.equals(other.numeroVeiculo))
			return false;
		if (operador == null) {
			if (other.operador != null)
				return false;
		} else if (!operador.equals(other.operador))
			return false;
		if (valorLiquidoCaixa == null) {
			if (other.valorLiquidoCaixa != null)
				return false;
		} else if (!valorLiquidoCaixa.equals(other.valorLiquidoCaixa))
			return false;
		if (valorTotalAcertos == null) {
			if (other.valorTotalAcertos != null)
				return false;
		} else if (!valorTotalAcertos.equals(other.valorTotalAcertos))
			return false;
		if (valorTotalComissoes == null) {
			if (other.valorTotalComissoes != null)
				return false;
		} else if (!valorTotalComissoes.equals(other.valorTotalComissoes))
			return false;
		if (valorTotalTrocos == null) {
			if (other.valorTotalTrocos != null)
				return false;
		} else if (!valorTotalTrocos.equals(other.valorTotalTrocos))
			return false;
		if (valorTotalVendas == null) {
			if (other.valorTotalVendas != null)
				return false;
		} else if (!valorTotalVendas.equals(other.valorTotalVendas))
			return false;
		return true;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CaixaFechadoBean [caixaId=");
		builder.append(caixaId);
		builder.append(", codigoSessao=");
		builder.append(codigoSessao);
		builder.append(", dataAbertura=");
		builder.append(dataAbertura);
		builder.append(", horaAbertura=");
		builder.append(horaAbertura);
		builder.append(", dataFechamento=");
		builder.append(dataFechamento);
		builder.append(", horaFechamento=");
		builder.append(horaFechamento);
		builder.append(", valorTotalVendas=");
		builder.append(valorTotalVendas);
		builder.append(", valorTotalComissoes=");
		builder.append(valorTotalComissoes);
		builder.append(", valorTotalAcertos=");
		builder.append(valorTotalAcertos);
		builder.append(", valorTotalTrocos=");
		builder.append(valorTotalTrocos);
		builder.append(", valorLiquidoCaixa=");
		builder.append(valorLiquidoCaixa);
		builder.append(", empresa=");
		builder.append(empresa);
		builder.append(", operador=");
		builder.append(operador);
		builder.append(", numeroVeiculo=");
		builder.append(numeroVeiculo);
		builder.append(", numeroCatraca=");
		builder.append(numeroCatraca);
		builder.append(", idDispositivo=");
		builder.append(idDispositivo);
		builder.append("]");
		return builder.toString();
	}

}
