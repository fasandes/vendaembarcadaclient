package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the tipo_pagamento database table.
 * 
 */
@Entity
@Table(name = "tipo_pagamento")
@NamedQuery(name = "TipoPagamento.findAll", query = "SELECT t FROM TipoPagamento t")
public class TipoPagamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "codigo_tipo_pagamento")
	private String codigoTipoPagamento;

	private Boolean ativo;

	private String descricao;

	@Column(name = "exige_documento")
	private Boolean exigeDocumento;

	private BigDecimal fator;

	public TipoPagamento() {
	}

	public String getCodigoTipoPagamento() {
		return codigoTipoPagamento;
	}

	public void setCodigoTipoPagamento(String codigoTipoPagamento) {
		this.codigoTipoPagamento = codigoTipoPagamento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getExigeDocumento() {
		return exigeDocumento;
	}

	public void setExigeDocumento(Boolean exigeDocumento) {
		this.exigeDocumento = exigeDocumento;
	}

	public BigDecimal getFator() {
		return fator;
	}

	public void setFator(BigDecimal fator) {
		this.fator = fator;
	}

}