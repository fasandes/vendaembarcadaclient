package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;
import java.util.Date;

public class VendaFechadaBean {

	private Integer vendaId;
	private Integer numeroBilhete;
	private Date dataAberturaViagem;
	private Date dataAberturaVenda;
	private Date dataAberturaCaixa;
	private Date horaAberturaVenda;
	private Date horaAberturaCaixa;
	private Integer codigoSessao;
	private String codigoVenda;
	private BigDecimal valorTotalBilhete;
	private String tipoVenda;
	private String codigoVeiculo;
	private Date dataFechamento;
	private Date horaFechamento;
	private BigDecimal valorTaxaEmbarque;
	private BigDecimal valorSeguro;
	private Boolean vendaCancelada;
	private BigDecimal valorPedagio;
	private BigDecimal valorOutros;
	private String documento;
	private Integer pda;
	private String classe;
	private String empresa;
	private Integer destino;
	private Integer origem;
	private Integer linha;
	private Integer codigoHorario;
	private Integer viagemId;
	private Integer operador;
	private String formaPagamento;
	private String viagemCodigoVeiculo;
	private Date viagemDataViagem;
	private String viagemHoraViagem;
	private String viagemSituacaoViagem;
	private Date viagemDataFechamentoViagem;
	private Date viagemHoraFechamentoViagem;
	private Integer viagemCodigoHorario;
	private char viagemSentidoViagem;
	private String idDispositivo;
	private String operadorEmail;

	public Integer getVendaId() {
		return vendaId;
	}

	public void setVendaId(Integer vendaId) {
		this.vendaId = vendaId;
	}

	public Integer getNumeroBilhete() {
		return numeroBilhete;
	}

	public void setNumeroBilhete(Integer numeroBilhete) {
		this.numeroBilhete = numeroBilhete;
	}

	public Date getDataAberturaViagem() {
		return dataAberturaViagem;
	}

	public void setDataAberturaViagem(Date dataAberturaViagem) {
		this.dataAberturaViagem = dataAberturaViagem;
	}

	public Date getDataAberturaVenda() {
		return dataAberturaVenda;
	}

	public void setDataAberturaVenda(Date dataAberturaVenda) {
		this.dataAberturaVenda = dataAberturaVenda;
	}

	public Date getDataAberturaCaixa() {
		return dataAberturaCaixa;
	}

	public void setDataAberturaCaixa(Date dataAberturaCaixa) {
		this.dataAberturaCaixa = dataAberturaCaixa;
	}

	public Date getHoraAberturaVenda() {
		return horaAberturaVenda;
	}

	public void setHoraAberturaVenda(Date horaAberturaVenda) {
		this.horaAberturaVenda = horaAberturaVenda;
	}

	public Date getHoraAberturaCaixa() {
		return horaAberturaCaixa;
	}

	public void setHoraAberturaCaixa(Date horaAberturaCaixa) {
		this.horaAberturaCaixa = horaAberturaCaixa;
	}

	public Integer getCodigoSessao() {
		return codigoSessao;
	}

	public void setCodigoSessao(Integer codigoSessao) {
		this.codigoSessao = codigoSessao;
	}

	public String getCodigoVenda() {
		return codigoVenda;
	}

	public void setCodigoVenda(String codigoVenda) {
		this.codigoVenda = codigoVenda;
	}

	public BigDecimal getValorTotalBilhete() {
		return valorTotalBilhete;
	}

	public void setValorTotalBilhete(BigDecimal valorTotalBilhete) {
		this.valorTotalBilhete = valorTotalBilhete;
	}

	public String getTipoVenda() {
		return tipoVenda;
	}

	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public String getCodigoVeiculo() {
		return codigoVeiculo;
	}

	public void setCodigoVeiculo(String codigoVeiculo) {
		this.codigoVeiculo = codigoVeiculo;
	}

	public Date getDataFechamento() {
		return dataFechamento;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	public Date getHoraFechamento() {
		return horaFechamento;
	}

	public void setHoraFechamento(Date horaFechamento) {
		this.horaFechamento = horaFechamento;
	}

	public BigDecimal getValorTaxaEmbarque() {
		return valorTaxaEmbarque;
	}

	public void setValorTaxaEmbarque(BigDecimal valorTaxaEmbarque) {
		this.valorTaxaEmbarque = valorTaxaEmbarque;
	}

	public BigDecimal getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(BigDecimal valorSeguro) {
		this.valorSeguro = valorSeguro;
	}

	public Boolean getVendaCancelada() {
		return vendaCancelada;
	}

	public void setVendaCancelada(Boolean vendaCancelada) {
		this.vendaCancelada = vendaCancelada;
	}

	public BigDecimal getValorPedagio() {
		return valorPedagio;
	}

	public void setValorPedagio(BigDecimal valorPedagio) {
		this.valorPedagio = valorPedagio;
	}

	public BigDecimal getValorOutros() {
		return valorOutros;
	}

	public void setValorOutros(BigDecimal valorOutros) {
		this.valorOutros = valorOutros;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getPda() {
		return pda;
	}

	public void setPda(Integer pda) {
		this.pda = pda;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Integer getDestino() {
		return destino;
	}

	public void setDestino(Integer destino) {
		this.destino = destino;
	}

	public Integer getOrigem() {
		return origem;
	}

	public void setOrigem(Integer origem) {
		this.origem = origem;
	}

	public Integer getLinha() {
		return linha;
	}

	public void setLinha(Integer linha) {
		this.linha = linha;
	}

	public Integer getCodigoHorario() {
		return codigoHorario;
	}

	public void setCodigoHorario(Integer codigoHorario) {
		this.codigoHorario = codigoHorario;
	}

	public Integer getViagemId() {
		return viagemId;
	}

	public void setViagemId(Integer viagemId) {
		this.viagemId = viagemId;
	}

	public Integer getOperador() {
		return operador;
	}

	public void setOperador(Integer operador) {
		this.operador = operador;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public String getViagemCodigoVeiculo() {
		return viagemCodigoVeiculo;
	}

	public void setViagemCodigoVeiculo(String viagemCodigoVeiculo) {
		this.viagemCodigoVeiculo = viagemCodigoVeiculo;
	}

	public Date getViagemDataViagem() {
		return viagemDataViagem;
	}

	public void setViagemDataViagem(Date viagemDataViagem) {
		this.viagemDataViagem = viagemDataViagem;
	}

	public String getViagemHoraViagem() {
		return viagemHoraViagem;
	}

	public void setViagemHoraViagem(String viagemHoraViagem) {
		this.viagemHoraViagem = viagemHoraViagem;
	}

	public String getViagemSituacaoViagem() {
		return viagemSituacaoViagem;
	}

	public void setViagemSituacaoViagem(String viagemSituacaoViagem) {
		this.viagemSituacaoViagem = viagemSituacaoViagem;
	}

	public Date getViagemDataFechamentoViagem() {
		return viagemDataFechamentoViagem;
	}

	public void setViagemDataFechamentoViagem(Date viagemDataFechamentoViagem) {
		this.viagemDataFechamentoViagem = viagemDataFechamentoViagem;
	}

	public Date getViagemHoraFechamentoViagem() {
		return viagemHoraFechamentoViagem;
	}

	public void setViagemHoraFechamentoViagem(Date viagemHoraFechamentoViagem) {
		this.viagemHoraFechamentoViagem = viagemHoraFechamentoViagem;
	}

	public Integer getViagemCodigoHorario() {
		return viagemCodigoHorario;
	}

	public void setViagemCodigoHorario(Integer viagemCodigoHorario) {
		this.viagemCodigoHorario = viagemCodigoHorario;
	}

	public char getViagemSentidoViagem() {
		return viagemSentidoViagem;
	}

	public void setViagemSentidoViagem(char viagemSentidoViagem) {
		this.viagemSentidoViagem = viagemSentidoViagem;
	}

	public String getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(String idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getOperadorEmail() {
		return operadorEmail;
	}

	public void setOperadorEmail(String operadorEmail) {
		this.operadorEmail = operadorEmail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classe == null) ? 0 : classe.hashCode());
		result = prime * result + ((codigoHorario == null) ? 0 : codigoHorario.hashCode());
		result = prime * result + ((codigoSessao == null) ? 0 : codigoSessao.hashCode());
		result = prime * result + ((codigoVeiculo == null) ? 0 : codigoVeiculo.hashCode());
		result = prime * result + ((codigoVenda == null) ? 0 : codigoVenda.hashCode());
		result = prime * result + ((dataAberturaCaixa == null) ? 0 : dataAberturaCaixa.hashCode());
		result = prime * result + ((dataAberturaVenda == null) ? 0 : dataAberturaVenda.hashCode());
		result = prime * result + ((dataAberturaViagem == null) ? 0 : dataAberturaViagem.hashCode());
		result = prime * result + ((dataFechamento == null) ? 0 : dataFechamento.hashCode());
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + ((documento == null) ? 0 : documento.hashCode());
		result = prime * result + ((empresa == null) ? 0 : empresa.hashCode());
		result = prime * result + ((formaPagamento == null) ? 0 : formaPagamento.hashCode());
		result = prime * result + ((horaAberturaCaixa == null) ? 0 : horaAberturaCaixa.hashCode());
		result = prime * result + ((horaAberturaVenda == null) ? 0 : horaAberturaVenda.hashCode());
		result = prime * result + ((horaFechamento == null) ? 0 : horaFechamento.hashCode());
		result = prime * result + ((idDispositivo == null) ? 0 : idDispositivo.hashCode());
		result = prime * result + ((linha == null) ? 0 : linha.hashCode());
		result = prime * result + ((numeroBilhete == null) ? 0 : numeroBilhete.hashCode());
		result = prime * result + ((operador == null) ? 0 : operador.hashCode());
		result = prime * result + ((operadorEmail == null) ? 0 : operadorEmail.hashCode());
		result = prime * result + ((origem == null) ? 0 : origem.hashCode());
		result = prime * result + ((pda == null) ? 0 : pda.hashCode());
		result = prime * result + ((tipoVenda == null) ? 0 : tipoVenda.hashCode());
		result = prime * result + ((valorOutros == null) ? 0 : valorOutros.hashCode());
		result = prime * result + ((valorPedagio == null) ? 0 : valorPedagio.hashCode());
		result = prime * result + ((valorSeguro == null) ? 0 : valorSeguro.hashCode());
		result = prime * result + ((valorTaxaEmbarque == null) ? 0 : valorTaxaEmbarque.hashCode());
		result = prime * result + ((valorTotalBilhete == null) ? 0 : valorTotalBilhete.hashCode());
		result = prime * result + ((vendaCancelada == null) ? 0 : vendaCancelada.hashCode());
		result = prime * result + ((vendaId == null) ? 0 : vendaId.hashCode());
		result = prime * result + ((viagemCodigoHorario == null) ? 0 : viagemCodigoHorario.hashCode());
		result = prime * result + ((viagemCodigoVeiculo == null) ? 0 : viagemCodigoVeiculo.hashCode());
		result = prime * result + ((viagemDataFechamentoViagem == null) ? 0 : viagemDataFechamentoViagem.hashCode());
		result = prime * result + ((viagemDataViagem == null) ? 0 : viagemDataViagem.hashCode());
		result = prime * result + ((viagemHoraFechamentoViagem == null) ? 0 : viagemHoraFechamentoViagem.hashCode());
		result = prime * result + ((viagemHoraViagem == null) ? 0 : viagemHoraViagem.hashCode());
		result = prime * result + ((viagemId == null) ? 0 : viagemId.hashCode());
		result = prime * result + viagemSentidoViagem;
		result = prime * result + ((viagemSituacaoViagem == null) ? 0 : viagemSituacaoViagem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaFechadaBean other = (VendaFechadaBean) obj;
		if (classe == null) {
			if (other.classe != null)
				return false;
		} else if (!classe.equals(other.classe))
			return false;
		if (codigoHorario == null) {
			if (other.codigoHorario != null)
				return false;
		} else if (!codigoHorario.equals(other.codigoHorario))
			return false;
		if (codigoSessao == null) {
			if (other.codigoSessao != null)
				return false;
		} else if (!codigoSessao.equals(other.codigoSessao))
			return false;
		if (codigoVeiculo == null) {
			if (other.codigoVeiculo != null)
				return false;
		} else if (!codigoVeiculo.equals(other.codigoVeiculo))
			return false;
		if (codigoVenda == null) {
			if (other.codigoVenda != null)
				return false;
		} else if (!codigoVenda.equals(other.codigoVenda))
			return false;
		if (dataAberturaCaixa == null) {
			if (other.dataAberturaCaixa != null)
				return false;
		} else if (!dataAberturaCaixa.equals(other.dataAberturaCaixa))
			return false;
		if (dataAberturaVenda == null) {
			if (other.dataAberturaVenda != null)
				return false;
		} else if (!dataAberturaVenda.equals(other.dataAberturaVenda))
			return false;
		if (dataAberturaViagem == null) {
			if (other.dataAberturaViagem != null)
				return false;
		} else if (!dataAberturaViagem.equals(other.dataAberturaViagem))
			return false;
		if (dataFechamento == null) {
			if (other.dataFechamento != null)
				return false;
		} else if (!dataFechamento.equals(other.dataFechamento))
			return false;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (documento == null) {
			if (other.documento != null)
				return false;
		} else if (!documento.equals(other.documento))
			return false;
		if (empresa == null) {
			if (other.empresa != null)
				return false;
		} else if (!empresa.equals(other.empresa))
			return false;
		if (formaPagamento == null) {
			if (other.formaPagamento != null)
				return false;
		} else if (!formaPagamento.equals(other.formaPagamento))
			return false;
		if (horaAberturaCaixa == null) {
			if (other.horaAberturaCaixa != null)
				return false;
		} else if (!horaAberturaCaixa.equals(other.horaAberturaCaixa))
			return false;
		if (horaAberturaVenda == null) {
			if (other.horaAberturaVenda != null)
				return false;
		} else if (!horaAberturaVenda.equals(other.horaAberturaVenda))
			return false;
		if (horaFechamento == null) {
			if (other.horaFechamento != null)
				return false;
		} else if (!horaFechamento.equals(other.horaFechamento))
			return false;
		if (idDispositivo == null) {
			if (other.idDispositivo != null)
				return false;
		} else if (!idDispositivo.equals(other.idDispositivo))
			return false;
		if (linha == null) {
			if (other.linha != null)
				return false;
		} else if (!linha.equals(other.linha))
			return false;
		if (numeroBilhete == null) {
			if (other.numeroBilhete != null)
				return false;
		} else if (!numeroBilhete.equals(other.numeroBilhete))
			return false;
		if (operador == null) {
			if (other.operador != null)
				return false;
		} else if (!operador.equals(other.operador))
			return false;
		if (operadorEmail == null) {
			if (other.operadorEmail != null)
				return false;
		} else if (!operadorEmail.equals(other.operadorEmail))
			return false;
		if (origem == null) {
			if (other.origem != null)
				return false;
		} else if (!origem.equals(other.origem))
			return false;
		if (pda == null) {
			if (other.pda != null)
				return false;
		} else if (!pda.equals(other.pda))
			return false;
		if (tipoVenda == null) {
			if (other.tipoVenda != null)
				return false;
		} else if (!tipoVenda.equals(other.tipoVenda))
			return false;
		if (valorOutros == null) {
			if (other.valorOutros != null)
				return false;
		} else if (!valorOutros.equals(other.valorOutros))
			return false;
		if (valorPedagio == null) {
			if (other.valorPedagio != null)
				return false;
		} else if (!valorPedagio.equals(other.valorPedagio))
			return false;
		if (valorSeguro == null) {
			if (other.valorSeguro != null)
				return false;
		} else if (!valorSeguro.equals(other.valorSeguro))
			return false;
		if (valorTaxaEmbarque == null) {
			if (other.valorTaxaEmbarque != null)
				return false;
		} else if (!valorTaxaEmbarque.equals(other.valorTaxaEmbarque))
			return false;
		if (valorTotalBilhete == null) {
			if (other.valorTotalBilhete != null)
				return false;
		} else if (!valorTotalBilhete.equals(other.valorTotalBilhete))
			return false;
		if (vendaCancelada == null) {
			if (other.vendaCancelada != null)
				return false;
		} else if (!vendaCancelada.equals(other.vendaCancelada))
			return false;
		if (vendaId == null) {
			if (other.vendaId != null)
				return false;
		} else if (!vendaId.equals(other.vendaId))
			return false;
		if (viagemCodigoHorario == null) {
			if (other.viagemCodigoHorario != null)
				return false;
		} else if (!viagemCodigoHorario.equals(other.viagemCodigoHorario))
			return false;
		if (viagemCodigoVeiculo == null) {
			if (other.viagemCodigoVeiculo != null)
				return false;
		} else if (!viagemCodigoVeiculo.equals(other.viagemCodigoVeiculo))
			return false;
		if (viagemDataFechamentoViagem == null) {
			if (other.viagemDataFechamentoViagem != null)
				return false;
		} else if (!viagemDataFechamentoViagem.equals(other.viagemDataFechamentoViagem))
			return false;
		if (viagemDataViagem == null) {
			if (other.viagemDataViagem != null)
				return false;
		} else if (!viagemDataViagem.equals(other.viagemDataViagem))
			return false;
		if (viagemHoraFechamentoViagem == null) {
			if (other.viagemHoraFechamentoViagem != null)
				return false;
		} else if (!viagemHoraFechamentoViagem.equals(other.viagemHoraFechamentoViagem))
			return false;
		if (viagemHoraViagem == null) {
			if (other.viagemHoraViagem != null)
				return false;
		} else if (!viagemHoraViagem.equals(other.viagemHoraViagem))
			return false;
		if (viagemId == null) {
			if (other.viagemId != null)
				return false;
		} else if (!viagemId.equals(other.viagemId))
			return false;
		if (viagemSentidoViagem != other.viagemSentidoViagem)
			return false;
		if (viagemSituacaoViagem == null) {
			if (other.viagemSituacaoViagem != null)
				return false;
		} else if (!viagemSituacaoViagem.equals(other.viagemSituacaoViagem))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VendaFechadaBean [vendaId=" + vendaId + ", numeroBilhete=" + numeroBilhete + ", dataAberturaViagem="
				+ dataAberturaViagem + ", dataAberturaVenda=" + dataAberturaVenda + ", dataAberturaCaixa="
				+ dataAberturaCaixa + ", horaAberturaVenda=" + horaAberturaVenda + ", horaAberturaCaixa="
				+ horaAberturaCaixa + ", codigoSessao=" + codigoSessao + ", codigoVenda=" + codigoVenda
				+ ", valorTotalBilhete=" + valorTotalBilhete + ", tipoVenda=" + tipoVenda + ", codigoVeiculo="
				+ codigoVeiculo + ", dataFechamento=" + dataFechamento + ", horaFechamento=" + horaFechamento
				+ ", valorTaxaEmbarque=" + valorTaxaEmbarque + ", valorSeguro=" + valorSeguro + ", vendaCancelada="
				+ vendaCancelada + ", valorPedagio=" + valorPedagio + ", valorOutros=" + valorOutros + ", documento="
				+ documento + ", pda=" + pda + ", classe=" + classe + ", empresa=" + empresa + ", destino=" + destino
				+ ", origem=" + origem + ", linha=" + linha + ", codigoHorario=" + codigoHorario + ", viagemId="
				+ viagemId + ", operador=" + operador + ", formaPagamento=" + formaPagamento + ", viagemCodigoVeiculo="
				+ viagemCodigoVeiculo + ", viagemDataViagem=" + viagemDataViagem + ", viagemHoraViagem="
				+ viagemHoraViagem + ", viagemSituacaoViagem=" + viagemSituacaoViagem + ", viagemDataFechamentoViagem="
				+ viagemDataFechamentoViagem + ", viagemHoraFechamentoViagem=" + viagemHoraFechamentoViagem
				+ ", viagemCodigoHorario=" + viagemCodigoHorario + ", viagemSentidoViagem=" + viagemSentidoViagem
				+ ", idDispositivo=" + idDispositivo + ", operadorEmail=" + operadorEmail + "]";
	}
}
