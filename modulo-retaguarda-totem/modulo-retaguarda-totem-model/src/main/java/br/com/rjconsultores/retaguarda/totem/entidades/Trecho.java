package br.com.rjconsultores.retaguarda.totem.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

/**
 * The persistent class for the trecho database table.
 * 
 */
@Entity
@NamedQuery(name = "Trecho.findAll", query = "SELECT t FROM Trecho t")
public class Trecho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "sequencia_da_localidade")
	private Integer sequenciaDaLocalidade;

	@ManyToOne
	private Servico servico;

	@ManyToOne
	@JoinColumn(name = "origem_id")
	private Localidade origem;

	@ManyToOne
	@JoinColumn(name = "destino_id")
	private Localidade destino;

	public Trecho() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequenciaDaLocalidade() {
		return sequenciaDaLocalidade;
	}

	public void setSequenciaDaLocalidade(Integer sequenciaDaLocalidade) {
		this.sequenciaDaLocalidade = sequenciaDaLocalidade;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Localidade getOrigem() {
		return origem;
	}

	public void setOrigem(Localidade origem) {
		this.origem = origem;
	}

	public Localidade getDestino() {
		return destino;
	}

	public void setDestino(Localidade destino) {
		this.destino = destino;
	}

}