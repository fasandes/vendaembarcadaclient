package br.com.rjconsultores.retaguarda.totem.bean;

import java.math.BigDecimal;

import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

public class DetalheHorarioOperadorBean {

	private Integer sequenciaDaLocalidade;
	private Integer horario;
	private Localidade localidade;
	private BigDecimal latitude;
	private BigDecimal longitude;

	public Integer getSequenciaDaLocalidade() {
		return sequenciaDaLocalidade;
	}

	public void setSequenciaDaLocalidade(Integer sequenciaDaLocalidade) {
		this.sequenciaDaLocalidade = sequenciaDaLocalidade;
	}

	public Integer getHorario() {
		return horario;
	}

	public void setHorario(Integer horario) {
		this.horario = horario;
	}

	public Localidade getLocalidade() {
		return localidade;
	}

	public void setLocalidade(Localidade localidade) {
		this.localidade = localidade;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((horario == null) ? 0 : horario.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((localidade == null) ? 0 : localidade.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((sequenciaDaLocalidade == null) ? 0 : sequenciaDaLocalidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalheHorarioOperadorBean other = (DetalheHorarioOperadorBean) obj;
		if (horario == null) {
			if (other.horario != null)
				return false;
		} else if (!horario.equals(other.horario))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (localidade == null) {
			if (other.localidade != null)
				return false;
		} else if (!localidade.equals(other.localidade))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (sequenciaDaLocalidade == null) {
			if (other.sequenciaDaLocalidade != null)
				return false;
		} else if (!sequenciaDaLocalidade.equals(other.sequenciaDaLocalidade))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetalheHorarioOperacaoBean [sequenciaDaLocalidade=" + sequenciaDaLocalidade + ", horario=" + horario
				+ ", localidade=" + localidade + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

}
