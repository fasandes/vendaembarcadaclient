package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import br.com.rjconsultores.retaguarda.totem.dao.TipoPagamentoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;

@Named("tipoPagamentoDAO")
public class TipoPagamentoHibernateDAO extends GenericHibernateDAO<TipoPagamento, String> implements TipoPagamentoDAO {

	public TipoPagamentoHibernateDAO() {
		super(TipoPagamento.class);
	}

	@Override
	public List<TipoPagamento> buscarTodosAtivos() {
		List<TipoPagamento> lsTipoPagamento = new ArrayList<>();
		Criteria criteria = getSessao().createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("ativo", true));
		lsTipoPagamento = (List<TipoPagamento>) criteria.list();
		return lsTipoPagamento;
	}

	@Override
	public void deletarTodos() {
		try {
			String sql = "DELETE FROM tipo_pagamento";
			SQLQuery query = getSessao().createSQLQuery(sql);
			query.executeUpdate();
			// getSessao().delete(op);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<TipoPagamento> buscarVinculoTipoPagamento() {
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT t FROM TipoPagamento t ");
			sql.append("WHERE t.ativo = :ativo AND t.codigoTipoPagamento NOT IN ");
			sql.append("(SELECT v.codigoTipoPagamento from VinculoTipoPagamento v) ");

			Query query = getSessao().createQuery(sql.toString());
			query.setInteger("ativo", 1);

			if (!query.list().isEmpty()) {
				return (List<TipoPagamento>) query.list();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// @Inject
	// public ClasseHibernateDAO(
	// @Qualifier("sessionFactory") SessionFactory factory) {
	// setSessionFactory(factory);
	// }

}
