package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.PrecoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Preco;

@Named(value = "precoService")
public class PrecoService {

	@Inject
	private PrecoDAO precoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(PrecoService.class);

	public Preco salvar(final Preco p) {
		return precoDAO.salvar(p);
	}

	public Preco buscarPorId(final Integer id) {
		return precoDAO.buscarPorId(id);
	}

	public Preco atualizar(final Preco preco) {
		return precoDAO.atualizar(preco);
	}

	public void deletar(final Preco preco) {
		precoDAO.apagar(preco);
	}

	public List<Preco> buscarTodos() {
		return precoDAO.buscarTodos();
	}

	public void savarOuAtualizar(final Preco preco) {
		precoDAO.savarOuAtualizar(preco);
	}
	

}
