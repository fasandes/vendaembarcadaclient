package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.TipoPagamentoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;

@Named(value = "tipoPagamentoService")
public class TipoPagamentoService {

	@Inject
	private TipoPagamentoDAO tipoPagamentoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(TipoPagamentoService.class);

	public TipoPagamento salvar(final TipoPagamento tipoPagamento) {
		return tipoPagamentoDAO.salvar(tipoPagamento);
	}

	public TipoPagamento buscarPorId(final String id) {
		return tipoPagamentoDAO.buscarPorId(id);
	}

	public TipoPagamento atualizar(final TipoPagamento tipoPagamento) {
		return tipoPagamentoDAO.atualizar(tipoPagamento);
	}

	public void deletar(final TipoPagamento tipoPagamento) {
		tipoPagamentoDAO.apagar(tipoPagamento);
	}

	public List<TipoPagamento> buscarTodos() {
		return tipoPagamentoDAO.buscarTodos();
	}

	public List<TipoPagamento> buscarTodosAtivos() {
		return tipoPagamentoDAO.buscarTodosAtivos();
	}

	public void deletarTodos() {
		tipoPagamentoDAO.deletarTodos();

	}

	public List<TipoPagamento> buscarVinculoTipoPagamento() {
		return tipoPagamentoDAO.buscarVinculoTipoPagamento();
	}

}
