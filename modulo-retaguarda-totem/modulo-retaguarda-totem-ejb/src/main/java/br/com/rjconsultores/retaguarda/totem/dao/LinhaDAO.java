package br.com.rjconsultores.retaguarda.totem.dao;

import java.util.List;

import br.com.rjconsultores.retaguarda.totem.entidades.Linha;

public interface LinhaDAO extends GenericDAO<Linha, Integer> {

	List<Linha> buscarLinhas(List<Integer> lsCodigosLinhas);

	List<Integer> buscarCodigoLinhaMesmaDescricao(String descricaoLinha);

}
