package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.ParametroEmbarcado;

public interface ParametroEmbarcadoDAO extends GenericDAO<ParametroEmbarcado, Integer> {

}
