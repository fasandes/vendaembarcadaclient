package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import br.com.rjconsultores.retaguarda.totem.dao.FormaPagamentoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.FormaPagamento;

@Named("formaPagamentoDAO")
public class FormaPagamentoHibernateDAO extends GenericHibernateDAO<FormaPagamento, String>
		implements FormaPagamentoDAO {

	public FormaPagamentoHibernateDAO() {
		super(FormaPagamento.class);
	}

	@Override
	public List<FormaPagamento> buscarTodosAtivos() {
		List<FormaPagamento> lsFormaPagamento = new ArrayList<>();
		Criteria criteria = getSessao().createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("ativo", true));
		lsFormaPagamento = (List<FormaPagamento>) criteria.list();
		return lsFormaPagamento;
	}

	@Override
	public void deletarTodos() {
		try {
			String sql = "DELETE FROM forma_pagamento";
			SQLQuery query = getSessao().createSQLQuery(sql);
			query.executeUpdate();
			// getSessao().delete(op);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<FormaPagamento> buscarVinculoFormaPagamento() {
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT f FROM FormaPagamento f ");
			sql.append("WHERE f.ativo = :ativo AND f.codigoFormaPagamento NOT IN ");
			sql.append("(SELECT v.codigoFormaPagamento from VinculoTipoPagamento v) ");

			Query query = getSessao().createQuery(sql.toString());
			query.setInteger("ativo", 1);

			if (!query.list().isEmpty()) {
				return (List<FormaPagamento>) query.list();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
