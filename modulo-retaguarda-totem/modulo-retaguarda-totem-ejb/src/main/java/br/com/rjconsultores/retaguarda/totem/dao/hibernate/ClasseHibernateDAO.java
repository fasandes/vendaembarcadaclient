package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.ClasseDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Classe;

@Named("classeDAO")
public class ClasseHibernateDAO extends GenericHibernateDAO<Classe, String> implements ClasseDAO {

	public ClasseHibernateDAO() {
		super(Classe.class);
	}

	// @Inject
	// public ClasseHibernateDAO(
	// @Qualifier("sessionFactory") SessionFactory factory) {
	// setSessionFactory(factory);
	// }

}
