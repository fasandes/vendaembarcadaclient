package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.EstadoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Estado;

@Named(value = "estadoService")
public class EstadoService {

	@Inject
	private EstadoDAO estadoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EstadoService.class);

	public Estado salvar(final Estado c) {
		return estadoDAO.salvar(c);
	}

	public Estado buscarPorId(final Integer id) {
		return estadoDAO.buscarPorId(id);
	}

	public Estado atualizar(final Estado estado) {
		return estadoDAO.atualizar(estado);
	}

	public void deletar(final Estado estado) {
		estadoDAO.apagar(estado);
	}

	public List<Estado> buscarTodos() {
		return estadoDAO.buscarTodos();
	}
	

}
