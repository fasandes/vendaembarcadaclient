package br.com.rjconsultores.retaguarda.totem.dao;

import java.util.List;

import br.com.rjconsultores.retaguarda.totem.entidades.FormaPagamento;

public interface FormaPagamentoDAO extends GenericDAO<FormaPagamento, String> {

	List<FormaPagamento> buscarTodosAtivos();

	void deletarTodos();

	List<FormaPagamento> buscarVinculoFormaPagamento();

}
