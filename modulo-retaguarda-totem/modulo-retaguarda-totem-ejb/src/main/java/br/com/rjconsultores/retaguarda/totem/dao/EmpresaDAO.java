package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;

public interface EmpresaDAO extends GenericDAO<Empresa, String> {

}
