package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.UsuarioDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;

@Named(value = "usuarioService")
public class UsuarioService {

	@Inject
	private UsuarioDAO usuarioDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(UsuarioService.class);

	public Usuario salvar(Usuario c) {
		return usuarioDAO.salvar(c);
	}

	public Usuario buscarPorId(final Integer id) {
		return usuarioDAO.buscarPorId(id);
	}

	public Usuario atualizar(final Usuario usuario) {
		return usuarioDAO.atualizar(usuario);
	}

	public void deletar(final Usuario usuario) {
		usuarioDAO.apagar(usuario);
	}

	public List<Usuario> buscarTodos() {
		return usuarioDAO.buscarTodos();
	}
	
	public Usuario buscarPorUsername(String username) {
		return usuarioDAO.buscarPorUsername(username);
	}
	

}
