package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;

public interface UsuarioDAO extends GenericDAO<Usuario, Integer> {


	public Usuario buscarPorUsername(String username);
}
