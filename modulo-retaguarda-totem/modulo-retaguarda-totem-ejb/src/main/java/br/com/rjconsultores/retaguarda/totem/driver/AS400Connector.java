/**
 * @author vinicio@rjconsultores.com.br
 *         17:38:18 13/02/2015 2015
 */
package br.com.rjconsultores.retaguarda.totem.driver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

/**
 * @author vinicio@rjconsultores.com.br 17:38:18 13/02/2015 2015
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE })
public @interface AS400Connector {

}
