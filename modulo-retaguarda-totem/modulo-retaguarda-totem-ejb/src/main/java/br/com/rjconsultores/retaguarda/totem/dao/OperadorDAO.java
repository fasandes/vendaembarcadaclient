package br.com.rjconsultores.retaguarda.totem.dao;

import java.util.List;

import br.com.rjconsultores.retaguarda.totem.entidades.Operador;

public interface OperadorDAO extends GenericDAO<Operador, Integer> {

	List<Operador> buscarTodosAtivos();

	boolean buscarOperador(String nome, String email, String agencia);

}
