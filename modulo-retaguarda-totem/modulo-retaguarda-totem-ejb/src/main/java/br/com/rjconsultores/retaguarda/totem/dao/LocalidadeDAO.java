/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.dao;

import java.util.List;

import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

public interface LocalidadeDAO extends GenericDAO<Localidade, Integer> {

	public List<Localidade> buscarPorIds(List<Integer> lsIds);

	public void deletarTodasLocalidades();

}
