package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.FormaPagamentoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.FormaPagamento;

@Named(value = "formaPagamentoService")
public class FormaPagamentoService {

	@Inject
	private FormaPagamentoDAO formaPagamentoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(FormaPagamentoService.class);

	public FormaPagamento salvar(final FormaPagamento formaPagamento) {
		return formaPagamentoDAO.salvar(formaPagamento);
	}

	public FormaPagamento buscarPorId(final String id) {
		return formaPagamentoDAO.buscarPorId(id);
	}

	public FormaPagamento atualizar(final FormaPagamento empresaSRVP) {
		return formaPagamentoDAO.atualizar(empresaSRVP);
	}

	public void deletar(final FormaPagamento formaPagamento) {
		formaPagamentoDAO.apagar(formaPagamento);
	}

	public List<FormaPagamento> buscarTodos() {
		return formaPagamentoDAO.buscarTodos();
	}

	public List<FormaPagamento> buscarTodosAtivos() {
		return formaPagamentoDAO.buscarTodosAtivos();
	}

	public void deletarTodos() {
		formaPagamentoDAO.deletarTodos();

	}

	public List<FormaPagamento> buscarVinculoFormaPagamento() {
		return formaPagamentoDAO.buscarVinculoFormaPagamento();
	}

}
