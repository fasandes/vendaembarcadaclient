package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.EstadoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Estado;

@Named("estadoDAO")
public class EstadoHibernateDAO extends GenericHibernateDAO<Estado, Integer> implements EstadoDAO {

	public EstadoHibernateDAO() {
		super(Estado.class);
	}


}
