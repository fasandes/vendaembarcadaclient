package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.ServicoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Servico;

@Named("servicoEmbarcadoDAO")
public class ServicoHibernateDAO extends GenericHibernateDAO<Servico, Integer> implements ServicoDAO {

	public ServicoHibernateDAO() {
		super(Servico.class);
	}

}
