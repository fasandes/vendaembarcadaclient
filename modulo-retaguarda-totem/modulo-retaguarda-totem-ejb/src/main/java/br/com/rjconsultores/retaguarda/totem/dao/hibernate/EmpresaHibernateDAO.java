package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.EmpresaDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;

@Named("empresaDAO")
public class EmpresaHibernateDAO extends GenericHibernateDAO<Empresa, String> implements EmpresaDAO {

	public EmpresaHibernateDAO() {
		super(Empresa.class);
	}

}
