package br.com.rjconsultores.retaguarda.totem.dao;

import java.util.List;

import br.com.rjconsultores.retaguarda.totem.entidades.TipoPagamento;

public interface TipoPagamentoDAO extends GenericDAO<TipoPagamento, String> {

	List<TipoPagamento> buscarTodosAtivos();

	void deletarTodos();

	List<TipoPagamento> buscarVinculoTipoPagamento();

}
