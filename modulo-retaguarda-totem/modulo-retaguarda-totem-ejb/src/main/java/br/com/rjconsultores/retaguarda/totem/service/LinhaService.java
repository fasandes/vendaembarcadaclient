package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.LinhaDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;

@Named(value = "serviceLinha")
public class LinhaService {

	@Inject
	private LinhaDAO linhaDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(LinhaService.class);

	public Linha salvar(final Linha c) {
		return linhaDAO.salvar(c);
	}

	public Linha buscarPorId(final int id) {
		return linhaDAO.buscarPorId(id);
	}

	public Linha atualizar(final Linha linha) {
		return linhaDAO.atualizar(linha);
	}
	
	public void deletar(final Linha linha) {
		linhaDAO.apagar(linha);
	}
	
	public List<Linha> buscarTodos() {
		return linhaDAO.buscarTodos();
	}

	public List<Linha> buscarLinhas(List<Integer> lsCodigosLinhas) {
		return linhaDAO.buscarLinhas(lsCodigosLinhas);
	}

	public List<Integer> buscarCodigoLinhaMesmaDescricao(String descricaoLinha) {
		return linhaDAO.buscarCodigoLinhaMesmaDescricao(descricaoLinha);
	}	
	

}
