package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Classe;

public interface ClasseDAO extends GenericDAO<Classe, String> {

}
