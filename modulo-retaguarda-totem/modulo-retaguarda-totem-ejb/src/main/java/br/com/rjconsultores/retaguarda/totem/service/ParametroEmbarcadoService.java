package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.ParametroEmbarcadoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.ParametroEmbarcado;

@Named(value = "parametroEmbarcadoService")
public class ParametroEmbarcadoService {

	@Inject
	private ParametroEmbarcadoDAO parametroEmbarcadoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ParametroEmbarcadoService.class);

	public ParametroEmbarcado buscarPorId(final Integer id) {
		return parametroEmbarcadoDAO.buscarPorId(id);
	}

	public ParametroEmbarcado atualizar(final ParametroEmbarcado parametroEmbarcado) {
		return parametroEmbarcadoDAO.atualizar(parametroEmbarcado);
	}

	public void deletar(final ParametroEmbarcado parametroEmbarcado) {
		parametroEmbarcadoDAO.apagar(parametroEmbarcado);
	}

	public List<ParametroEmbarcado> buscarTodos() {
		return parametroEmbarcadoDAO.buscarTodos();
	}

	public void savarOuAtualizar(final ParametroEmbarcado parametroEmbarcado) {
		parametroEmbarcadoDAO.savarOuAtualizar(parametroEmbarcado);
	}

}
