/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import java.util.List;

import javax.inject.Named;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;

import br.com.rjconsultores.retaguarda.totem.dao.LocalidadeDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

@Named("localidadeDAO")
public class LocalidadeHibernateDAO extends GenericHibernateDAO<Localidade, Integer> implements LocalidadeDAO {

	public LocalidadeHibernateDAO() {
		super(Localidade.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Localidade> buscarPorIds(List<Integer> lsIds) {
		Criteria criteria = getSessao().createCriteria(getPersistentClass());

		criteria.add(Restrictions.in("localidadeId", lsIds));

		return (List<Localidade>) criteria.list();
	}

	@Override
	public void deletarTodasLocalidades() {
		try {
			String sql = "DELETE FROM localidade";
			SQLQuery query = getSessao().createSQLQuery(sql);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return;

	}

}
