package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.ServicoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Servico;

@Named(value = "servicoService")
public class ServicoService {

	@Inject
	private ServicoDAO servicoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ServicoService.class);

	public Servico salvar(final Servico p) {
		return servicoDAO.salvar(p);
	}

	public Servico buscarPorId(final Integer id) {
		return servicoDAO.buscarPorId(id);
	}

	public Servico atualizar(final Servico servico) {
		return servicoDAO.atualizar(servico);
	}

	public void deletar(final Servico servico) {
		servicoDAO.apagar(servico);
	}

	public List<Servico> buscarTodos() {
		return servicoDAO.buscarTodos();
	}

	public void savarOuAtualizar(final Servico servico) {
		servicoDAO.savarOuAtualizar(servico);
	}

}
