/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.LocalidadeDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;

@Named(value = "localidadeService")
public class LocalidadeService {

	@Inject
	private LocalidadeDAO localidadeDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(LocalidadeService.class);

	public Localidade buscarPorId(final Integer i) {
		return localidadeDAO.buscarPorId(i);
	}

	public List<Localidade> buscarTodas() {
		return localidadeDAO.buscarTodos();
	}

	public List<Localidade> buscarPorIds(final List<Integer> lsIds) {
		return localidadeDAO.buscarPorIds(lsIds);
	}

	public Localidade atualizar(Localidade localidade) {
		return localidadeDAO.atualizar(localidade);

	}

	public void salvar(Localidade localidade) {
		localidadeDAO.salvar(localidade);

	}

	public void deletarTodasLocalidades() {
		localidadeDAO.deletarTodasLocalidades();

	}

}
