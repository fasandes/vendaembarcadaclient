package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.PrecoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Preco;

@Named("precoEmbarcadoDAO")
public class PrecoHibernateDAO extends GenericHibernateDAO<Preco, Integer> implements PrecoDAO {

	public PrecoHibernateDAO() {
		super(Preco.class);
	}

}
