package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.rjconsultores.retaguarda.totem.dao.OperadorDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;

@Named("operadorDAO")
public class OperadorHibernateDAO extends GenericHibernateDAO<Operador, Integer> implements OperadorDAO {

	public OperadorHibernateDAO() {
		super(Operador.class);
	}

	@Override
	public List<Operador> buscarTodosAtivos() {
		List<Operador> lsOperador = new ArrayList<>();
		Criteria criteria = getSessao().createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("operadorAtivo", true));
		criteria.add(Restrictions.isNotNull("operadorSenha"));
		lsOperador = (List<Operador>) criteria.list();
		return lsOperador;
	}

	@Override
	public boolean buscarOperador(String nome, String email, String agencia) {
		Criteria criteria = getSessao().createCriteria(getPersistentClass());
		criteria.add(Restrictions.eq("operadorName", nome));
		criteria.add(Restrictions.eq("operadorEmail", email));
		criteria.add(Restrictions.eq("operadorAgencia", agencia));

		if (!criteria.list().isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

}
