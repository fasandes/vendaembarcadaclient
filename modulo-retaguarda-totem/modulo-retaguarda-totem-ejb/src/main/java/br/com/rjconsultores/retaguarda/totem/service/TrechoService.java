package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.TrechoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Trecho;

@Named(value = "trechoService")
public class TrechoService {

	@Inject
	private TrechoDAO trechoDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(TrechoService.class);

	public Trecho salvar(final Trecho p) {
		return trechoDAO.salvar(p);
	}

	public Trecho buscarPorId(final Integer id) {
		return trechoDAO.buscarPorId(id);
	}

	public Trecho atualizar(final Trecho trecho) {
		return trechoDAO.atualizar(trecho);
	}

	public void deletar(final Trecho trecho) {
		trechoDAO.apagar(trecho);
	}

	public List<Trecho> buscarTodos() {
		return trechoDAO.buscarTodos();
	}

	public void savarOuAtualizar(final Trecho trecho) {
		trechoDAO.savarOuAtualizar(trecho);
	}
	
	public Trecho buscarTrecho(Integer origem, Integer destino, Integer linha) {
		return trechoDAO.buscaTrecho(origem, destino, linha);
	}

}
