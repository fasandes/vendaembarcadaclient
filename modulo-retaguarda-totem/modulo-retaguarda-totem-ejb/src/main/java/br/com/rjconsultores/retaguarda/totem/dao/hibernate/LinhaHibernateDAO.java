package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.rjconsultores.retaguarda.totem.dao.LinhaDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;

@Named("linhaDAO")
public class LinhaHibernateDAO extends GenericHibernateDAO<Linha, Integer> implements LinhaDAO {

	public LinhaHibernateDAO() {
		super(Linha.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Linha> buscarLinhas(List<Integer> lsCodigosLinhas) {
		List<Linha> lsLinha = new ArrayList<>();
		Criteria criteria = getSessao().createCriteria(getPersistentClass());
		criteria.add(Restrictions.in("codigoLinha", lsCodigosLinhas));
		lsLinha = (List<Linha>) criteria.list();
		return lsLinha;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> buscarCodigoLinhaMesmaDescricao(String descricaoLinha) {
		try {
			List<Integer> lsCodigoLinha = new ArrayList<Integer>();
			Criteria criteria = getSessao().createCriteria(getPersistentClass());
			criteria.add(Restrictions.like("descricao", "%" + descricaoLinha + "%"));

			if (!criteria.list().isEmpty()) {
				for (Linha linha : (List<Linha>) criteria.list()) {
					lsCodigoLinha.add(linha.getCodigoLinha());
				}
			}
			return lsCodigoLinha;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
