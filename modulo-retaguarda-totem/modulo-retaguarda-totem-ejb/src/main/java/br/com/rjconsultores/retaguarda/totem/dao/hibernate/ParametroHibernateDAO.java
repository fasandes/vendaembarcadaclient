package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.ParametroEmbarcadoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.ParametroEmbarcado;

@Named("parametroEmbarcadoDAO")
public class ParametroHibernateDAO extends GenericHibernateDAO<ParametroEmbarcado, Integer> implements ParametroEmbarcadoDAO {

	public ParametroHibernateDAO() {
		super(ParametroEmbarcado.class);
	}

}
