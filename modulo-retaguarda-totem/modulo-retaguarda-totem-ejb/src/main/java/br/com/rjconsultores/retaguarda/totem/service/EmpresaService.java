package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.EmpresaDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Empresa;

@Named(value = "empresaService")
public class EmpresaService {

	@Inject
	private EmpresaDAO empresaDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EmpresaService.class);

	public Empresa salvar(final Empresa c) {
		return empresaDAO.salvar(c);
	}

	// public Empresa buscarPorId(final char id) {
	// return empresaDAO.buscarPorId(id);
	// }

	public Empresa buscarPorCodigo(final String id) {
		return empresaDAO.buscarPorCodigo(id);
	}

	public Empresa atualizar(final Empresa empresaSRVP) {
		return empresaDAO.atualizar(empresaSRVP);
	}

	public void deletar(final Empresa empresaSRVP) {
		empresaDAO.apagar(empresaSRVP);
	}

	public List<Empresa> buscarTodos() {
		return empresaDAO.buscarTodos();
	}

}
