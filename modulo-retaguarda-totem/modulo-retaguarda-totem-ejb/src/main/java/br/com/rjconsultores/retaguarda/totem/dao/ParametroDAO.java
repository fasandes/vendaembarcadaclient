package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;

public interface ParametroDAO extends GenericDAO<Parametro, String> {

}
