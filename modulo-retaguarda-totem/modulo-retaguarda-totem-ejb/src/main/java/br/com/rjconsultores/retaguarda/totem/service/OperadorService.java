package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.OperadorDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Operador;

@Named(value = "operadorService")
public class OperadorService {

	@Inject
	private OperadorDAO operadorDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(OperadorService.class);

	public Operador salvar(Operador c) {
		return operadorDAO.salvar(c);
	}

	public Operador buscarPorId(final Integer id) {
		return operadorDAO.buscarPorId(id);
	}

	public Operador atualizar(final Operador operador) {
		return operadorDAO.atualizar(operador);
	}

	public void deletar(final Operador operador) {
		operadorDAO.apagar(operador);
	}

	public List<Operador> buscarTodos() {
		return operadorDAO.buscarTodos();
	}

	public List<Operador> buscarTodosAtivos() {
		return operadorDAO.buscarTodosAtivos();
	}

	public boolean buscarOperador(String nome, String email, String agencia) {
		return operadorDAO.buscarOperador(nome, email, agencia);
	}

}
