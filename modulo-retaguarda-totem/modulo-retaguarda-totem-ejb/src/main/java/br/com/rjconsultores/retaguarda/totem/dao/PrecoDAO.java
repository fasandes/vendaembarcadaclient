package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Preco;

public interface PrecoDAO extends GenericDAO<Preco, Integer> {

}
