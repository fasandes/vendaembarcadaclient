package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Estado;

public interface EstadoDAO extends GenericDAO<Estado, Integer> {

}
