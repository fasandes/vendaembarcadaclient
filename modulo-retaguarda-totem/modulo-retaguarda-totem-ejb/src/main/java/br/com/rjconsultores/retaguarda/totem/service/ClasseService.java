package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.ClasseDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Classe;

@Named(value = "classeService")
public class ClasseService {

	@Inject
	private ClasseDAO classeDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ClasseService.class);

	public Classe salvar(final Classe c) {
		return classeDAO.salvar(c);
	}

	public Classe buscarPorId(final String id) {
		return classeDAO.buscarPorId(id);
	}

	public Classe atualizar(final Classe classe) {
		return classeDAO.atualizar(classe);
	}

	public void deletar(final Classe classe) {
		classeDAO.apagar(classe);
	}

	public List<Classe> buscarTodos() {
		return classeDAO.buscarTodos();
	}

}
