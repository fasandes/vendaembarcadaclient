/**
 * @author vinicio@rjconsultores.com.br
 *         17:33:01 13/02/2015 2015
 */
package br.com.rjconsultores.retaguarda.totem.driver;

import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;

import com.rjconsultores.srvp.comm.AS400Comm;
import com.rjconsultores.srvp.comm.exceptions.AS400Exception;
import com.rjconsultores.srvp.comm.exceptions.AS400TimeOutException;

/**
 * @author vinicio@rjconsultores.com.br 17:33:01 13/02/2015 2015
 */
public class AS400DriverFactory {

	@Inject
	private Instance<Object> instance;

	@Produces
	@Singleton
	@AS400Connector
	public AS400Comm createAS400Driver() {
		return AS400Comm.getInstance();
	}

	public void disposesAS400Driver(@Disposes @AS400Connector AS400Comm as400Comm) throws AS400TimeOutException {
		this.desconectar(as400Comm);
	}

	/**
	 * @author vinicio@rjconsultores.com.br
	 * @param as400Comm
	 *            08:41:58 25/02/2015 2015
	 * @throws AS400TimeOutException
	 */
	private void desconectar(AS400Comm as400Comm) throws AS400TimeOutException {
		try {
			if (as400Comm.isLogado()) {
				as400Comm.deslogar();
			}

			if (as400Comm.isConectado()) {
				as400Comm.desconectar();
			}
		} catch (final AS400Exception e) {
			e.printStackTrace();
		}
	}

}
