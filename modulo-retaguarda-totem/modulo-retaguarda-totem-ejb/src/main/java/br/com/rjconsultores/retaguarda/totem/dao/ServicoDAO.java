package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Servico;

public interface ServicoDAO extends GenericDAO<Servico, Integer> {

}
