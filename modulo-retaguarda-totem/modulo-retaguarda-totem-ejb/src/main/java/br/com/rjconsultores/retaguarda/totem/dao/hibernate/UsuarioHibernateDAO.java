package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.rjconsultores.retaguarda.totem.dao.UsuarioDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario;
import br.com.rjconsultores.retaguarda.totem.entidades.Usuario_;

@Named("usuarioDAO")
public class UsuarioHibernateDAO extends GenericHibernateDAO<Usuario, Integer> implements UsuarioDAO {

	public UsuarioHibernateDAO() {
		super(Usuario.class);
	}
	
	public Usuario buscarPorUsername(String username) {

		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Usuario> criteria = builder.createQuery(this.getPersistentClass());
		Root<Usuario> rootEntry = criteria.from(this.getPersistentClass());
		criteria.select(rootEntry);
		criteria.where(builder.equal(rootEntry.get(Usuario_.usuario), username));

		try {
			return this.getEntityManager().createQuery(criteria).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

}
