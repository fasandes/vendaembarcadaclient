package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import br.com.rjconsultores.retaguarda.totem.dao.TrechoDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha;
import br.com.rjconsultores.retaguarda.totem.entidades.Linha_;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade;
import br.com.rjconsultores.retaguarda.totem.entidades.Localidade_;
import br.com.rjconsultores.retaguarda.totem.entidades.Servico;
import br.com.rjconsultores.retaguarda.totem.entidades.Servico_;
import br.com.rjconsultores.retaguarda.totem.entidades.Trecho;
import br.com.rjconsultores.retaguarda.totem.entidades.Trecho_;

@Named("trechoEmbarcadoDAO")
public class TrechoHibernateDAO extends GenericHibernateDAO<Trecho, Integer> implements TrechoDAO {

	public TrechoHibernateDAO() {
		super(Trecho.class);
	}

	@Override
	public Trecho buscaTrecho(Integer origem, Integer destino, Integer linha) {
		
		CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Trecho> criteria = builder.createQuery(this.getPersistentClass());
		
		Root<Trecho> rootEntry = criteria.from(this.getPersistentClass());
		Join<Trecho,Servico> servicoJoin = rootEntry.join(Trecho_.servico);
		Join<Trecho,Localidade> origemJoin = rootEntry.join(Trecho_.origem);
		Join<Trecho,Localidade> destinoJoin = rootEntry.join(Trecho_.destino);
		Join<Servico,Linha> linhaJoin = servicoJoin.join(Servico_.linha);
		
		criteria.select(rootEntry);
		criteria.where(builder.equal(linhaJoin.get(Linha_.codigoLinha), linha),builder.equal(origemJoin.get(Localidade_.localidadeId), origem),builder.equal(destinoJoin.get(Localidade_.localidadeId),destino));

		try {
			return this.getEntityManager().createQuery(criteria).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

	}

}
