package br.com.rjconsultores.retaguarda.totem.service;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import br.com.rjconsultores.retaguarda.totem.dao.ParametroDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;

@Named(value = "parametroService")
public class ParametroService {

	@Inject
	private ParametroDAO parametroDAO;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(ParametroService.class);

	public Parametro salvar(final Parametro p) {
		return parametroDAO.salvar(p);
	}

	public Parametro buscarPorId(final String id) {
		return parametroDAO.buscarPorId(id);
	}

	public Parametro atualizar(final Parametro parametros) {
		return parametroDAO.atualizar(parametros);
	}

	public void deletar(final Parametro parametros) {
		parametroDAO.apagar(parametros);
	}

	public List<Parametro> buscarTodos() {
		return parametroDAO.buscarTodos();
	}

	public void savarOuAtualizar(final Parametro parametro) {
		parametroDAO.savarOuAtualizar(parametro);
	}

}
