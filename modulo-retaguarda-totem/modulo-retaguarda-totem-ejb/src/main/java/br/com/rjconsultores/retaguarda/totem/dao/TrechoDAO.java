package br.com.rjconsultores.retaguarda.totem.dao;

import br.com.rjconsultores.retaguarda.totem.entidades.Trecho;

public interface TrechoDAO extends GenericDAO<Trecho, Integer> {
	
	public Trecho buscaTrecho(Integer origem, Integer destino, Integer linha);

}
