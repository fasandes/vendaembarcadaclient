package br.com.rjconsultores.retaguarda.totem.dao.hibernate;

import javax.inject.Named;

import br.com.rjconsultores.retaguarda.totem.dao.ParametroDAO;
import br.com.rjconsultores.retaguarda.totem.entidades.Parametro;

@Named("parametroDAO")
public class ParametroEmbarcadoHibernateDAO extends GenericHibernateDAO<Parametro, String> implements ParametroDAO {

	public ParametroEmbarcadoHibernateDAO() {
		super(Parametro.class);
	}

}
